
// Generated from Xanathar.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "XanatharParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by XanatharParser.
 */
class  XanatharListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterPrep(XanatharParser::PrepContext *ctx) = 0;
  virtual void exitPrep(XanatharParser::PrepContext *ctx) = 0;

  virtual void enterString_inner(XanatharParser::String_innerContext *ctx) = 0;
  virtual void exitString_inner(XanatharParser::String_innerContext *ctx) = 0;

  virtual void enterEscaped_string(XanatharParser::Escaped_stringContext *ctx) = 0;
  virtual void exitEscaped_string(XanatharParser::Escaped_stringContext *ctx) = 0;

  virtual void enterLetter(XanatharParser::LetterContext *ctx) = 0;
  virtual void exitLetter(XanatharParser::LetterContext *ctx) = 0;

  virtual void enterCname(XanatharParser::CnameContext *ctx) = 0;
  virtual void exitCname(XanatharParser::CnameContext *ctx) = 0;

  virtual void enterWord(XanatharParser::WordContext *ctx) = 0;
  virtual void exitWord(XanatharParser::WordContext *ctx) = 0;

  virtual void enterStr(XanatharParser::StrContext *ctx) = 0;
  virtual void exitStr(XanatharParser::StrContext *ctx) = 0;

  virtual void enterCast(XanatharParser::CastContext *ctx) = 0;
  virtual void exitCast(XanatharParser::CastContext *ctx) = 0;

  virtual void enterUse_statement(XanatharParser::Use_statementContext *ctx) = 0;
  virtual void exitUse_statement(XanatharParser::Use_statementContext *ctx) = 0;

  virtual void enterDeclare_statement(XanatharParser::Declare_statementContext *ctx) = 0;
  virtual void exitDeclare_statement(XanatharParser::Declare_statementContext *ctx) = 0;

  virtual void enterVariable_phrase(XanatharParser::Variable_phraseContext *ctx) = 0;
  virtual void exitVariable_phrase(XanatharParser::Variable_phraseContext *ctx) = 0;

  virtual void enterRaw_var(XanatharParser::Raw_varContext *ctx) = 0;
  virtual void exitRaw_var(XanatharParser::Raw_varContext *ctx) = 0;

  virtual void enterAs_phrase(XanatharParser::As_phraseContext *ctx) = 0;
  virtual void exitAs_phrase(XanatharParser::As_phraseContext *ctx) = 0;

  virtual void enterTypespec(XanatharParser::TypespecContext *ctx) = 0;
  virtual void exitTypespec(XanatharParser::TypespecContext *ctx) = 0;

  virtual void enterVariable_setting(XanatharParser::Variable_settingContext *ctx) = 0;
  virtual void exitVariable_setting(XanatharParser::Variable_settingContext *ctx) = 0;

  virtual void enterLet(XanatharParser::LetContext *ctx) = 0;
  virtual void exitLet(XanatharParser::LetContext *ctx) = 0;

  virtual void enterClass_(XanatharParser::Class_Context *ctx) = 0;
  virtual void exitClass_(XanatharParser::Class_Context *ctx) = 0;

  virtual void enterClass__specs(XanatharParser::Class__specsContext *ctx) = 0;
  virtual void exitClass__specs(XanatharParser::Class__specsContext *ctx) = 0;

  virtual void enterExtends_(XanatharParser::Extends_Context *ctx) = 0;
  virtual void exitExtends_(XanatharParser::Extends_Context *ctx) = 0;

  virtual void enterInterface_(XanatharParser::Interface_Context *ctx) = 0;
  virtual void exitInterface_(XanatharParser::Interface_Context *ctx) = 0;

  virtual void enterInterface_fn(XanatharParser::Interface_fnContext *ctx) = 0;
  virtual void exitInterface_fn(XanatharParser::Interface_fnContext *ctx) = 0;

  virtual void enterDeclared_var(XanatharParser::Declared_varContext *ctx) = 0;
  virtual void exitDeclared_var(XanatharParser::Declared_varContext *ctx) = 0;

  virtual void enterTypes(XanatharParser::TypesContext *ctx) = 0;
  virtual void exitTypes(XanatharParser::TypesContext *ctx) = 0;

  virtual void enterInheritance(XanatharParser::InheritanceContext *ctx) = 0;
  virtual void exitInheritance(XanatharParser::InheritanceContext *ctx) = 0;

  virtual void enterFncall(XanatharParser::FncallContext *ctx) = 0;
  virtual void exitFncall(XanatharParser::FncallContext *ctx) = 0;

  virtual void enterGeneric_call(XanatharParser::Generic_callContext *ctx) = 0;
  virtual void exitGeneric_call(XanatharParser::Generic_callContext *ctx) = 0;

  virtual void enterGp(XanatharParser::GpContext *ctx) = 0;
  virtual void exitGp(XanatharParser::GpContext *ctx) = 0;

  virtual void enterFn(XanatharParser::FnContext *ctx) = 0;
  virtual void exitFn(XanatharParser::FnContext *ctx) = 0;

  virtual void enterHook(XanatharParser::HookContext *ctx) = 0;
  virtual void exitHook(XanatharParser::HookContext *ctx) = 0;

  virtual void enterTemplate_(XanatharParser::Template_Context *ctx) = 0;
  virtual void exitTemplate_(XanatharParser::Template_Context *ctx) = 0;

  virtual void enterClass_es(XanatharParser::Class_esContext *ctx) = 0;
  virtual void exitClass_es(XanatharParser::Class_esContext *ctx) = 0;

  virtual void enterClass__(XanatharParser::Class__Context *ctx) = 0;
  virtual void exitClass__(XanatharParser::Class__Context *ctx) = 0;

  virtual void enterVirtual_fn(XanatharParser::Virtual_fnContext *ctx) = 0;
  virtual void exitVirtual_fn(XanatharParser::Virtual_fnContext *ctx) = 0;

  virtual void enterOverride(XanatharParser::OverrideContext *ctx) = 0;
  virtual void exitOverride(XanatharParser::OverrideContext *ctx) = 0;

  virtual void enterOverride_name(XanatharParser::Override_nameContext *ctx) = 0;
  virtual void exitOverride_name(XanatharParser::Override_nameContext *ctx) = 0;

  virtual void enterOverride_param(XanatharParser::Override_paramContext *ctx) = 0;
  virtual void exitOverride_param(XanatharParser::Override_paramContext *ctx) = 0;

  virtual void enterLambda(XanatharParser::LambdaContext *ctx) = 0;
  virtual void exitLambda(XanatharParser::LambdaContext *ctx) = 0;

  virtual void enterParams(XanatharParser::ParamsContext *ctx) = 0;
  virtual void exitParams(XanatharParser::ParamsContext *ctx) = 0;

  virtual void enterIf_(XanatharParser::If_Context *ctx) = 0;
  virtual void exitIf_(XanatharParser::If_Context *ctx) = 0;

  virtual void enterElse_(XanatharParser::Else_Context *ctx) = 0;
  virtual void exitElse_(XanatharParser::Else_Context *ctx) = 0;

  virtual void enterFor_(XanatharParser::For_Context *ctx) = 0;
  virtual void exitFor_(XanatharParser::For_Context *ctx) = 0;

  virtual void enterWhile_(XanatharParser::While_Context *ctx) = 0;
  virtual void exitWhile_(XanatharParser::While_Context *ctx) = 0;

  virtual void enterRet(XanatharParser::RetContext *ctx) = 0;
  virtual void exitRet(XanatharParser::RetContext *ctx) = 0;

  virtual void enterMatch_(XanatharParser::Match_Context *ctx) = 0;
  virtual void exitMatch_(XanatharParser::Match_Context *ctx) = 0;

  virtual void enterMatch_arm(XanatharParser::Match_armContext *ctx) = 0;
  virtual void exitMatch_arm(XanatharParser::Match_armContext *ctx) = 0;

  virtual void enterDefault_arm(XanatharParser::Default_armContext *ctx) = 0;
  virtual void exitDefault_arm(XanatharParser::Default_armContext *ctx) = 0;

  virtual void enterRef(XanatharParser::RefContext *ctx) = 0;
  virtual void exitRef(XanatharParser::RefContext *ctx) = 0;

  virtual void enterPtr(XanatharParser::PtrContext *ctx) = 0;
  virtual void exitPtr(XanatharParser::PtrContext *ctx) = 0;

  virtual void enterDeref(XanatharParser::DerefContext *ctx) = 0;
  virtual void exitDeref(XanatharParser::DerefContext *ctx) = 0;

  virtual void enterNumber(XanatharParser::NumberContext *ctx) = 0;
  virtual void exitNumber(XanatharParser::NumberContext *ctx) = 0;

  virtual void enterFloat_(XanatharParser::Float_Context *ctx) = 0;
  virtual void exitFloat_(XanatharParser::Float_Context *ctx) = 0;

  virtual void enterBreak_(XanatharParser::Break_Context *ctx) = 0;
  virtual void exitBreak_(XanatharParser::Break_Context *ctx) = 0;

  virtual void enterSkip(XanatharParser::SkipContext *ctx) = 0;
  virtual void exitSkip(XanatharParser::SkipContext *ctx) = 0;

  virtual void enterNew_(XanatharParser::New_Context *ctx) = 0;
  virtual void exitNew_(XanatharParser::New_Context *ctx) = 0;

  virtual void enterCall_sealed_class_(XanatharParser::Call_sealed_class_Context *ctx) = 0;
  virtual void exitCall_sealed_class_(XanatharParser::Call_sealed_class_Context *ctx) = 0;

  virtual void enterLock(XanatharParser::LockContext *ctx) = 0;
  virtual void exitLock(XanatharParser::LockContext *ctx) = 0;

  virtual void enterUnlock(XanatharParser::UnlockContext *ctx) = 0;
  virtual void exitUnlock(XanatharParser::UnlockContext *ctx) = 0;

  virtual void enterInc(XanatharParser::IncContext *ctx) = 0;
  virtual void exitInc(XanatharParser::IncContext *ctx) = 0;

  virtual void enterDec(XanatharParser::DecContext *ctx) = 0;
  virtual void exitDec(XanatharParser::DecContext *ctx) = 0;

  virtual void enterReg(XanatharParser::RegContext *ctx) = 0;
  virtual void exitReg(XanatharParser::RegContext *ctx) = 0;

  virtual void enterReg_set(XanatharParser::Reg_setContext *ctx) = 0;
  virtual void exitReg_set(XanatharParser::Reg_setContext *ctx) = 0;

  virtual void enterBinaryNot(XanatharParser::BinaryNotContext *ctx) = 0;
  virtual void exitBinaryNot(XanatharParser::BinaryNotContext *ctx) = 0;

  virtual void enterLambdaCreation(XanatharParser::LambdaCreationContext *ctx) = 0;
  virtual void exitLambdaCreation(XanatharParser::LambdaCreationContext *ctx) = 0;

  virtual void enterLessOrEqual(XanatharParser::LessOrEqualContext *ctx) = 0;
  virtual void exitLessOrEqual(XanatharParser::LessOrEqualContext *ctx) = 0;

  virtual void enterMultiplication(XanatharParser::MultiplicationContext *ctx) = 0;
  virtual void exitMultiplication(XanatharParser::MultiplicationContext *ctx) = 0;

  virtual void enterBinaryAnd(XanatharParser::BinaryAndContext *ctx) = 0;
  virtual void exitBinaryAnd(XanatharParser::BinaryAndContext *ctx) = 0;

  virtual void enterBareword(XanatharParser::BarewordContext *ctx) = 0;
  virtual void exitBareword(XanatharParser::BarewordContext *ctx) = 0;

  virtual void enterCallSealedClass(XanatharParser::CallSealedClassContext *ctx) = 0;
  virtual void exitCallSealedClass(XanatharParser::CallSealedClassContext *ctx) = 0;

  virtual void enterBinaryXor(XanatharParser::BinaryXorContext *ctx) = 0;
  virtual void exitBinaryXor(XanatharParser::BinaryXorContext *ctx) = 0;

  virtual void enterGetSizeOf(XanatharParser::GetSizeOfContext *ctx) = 0;
  virtual void exitGetSizeOf(XanatharParser::GetSizeOfContext *ctx) = 0;

  virtual void enterPointer(XanatharParser::PointerContext *ctx) = 0;
  virtual void exitPointer(XanatharParser::PointerContext *ctx) = 0;

  virtual void enterIs(XanatharParser::IsContext *ctx) = 0;
  virtual void exitIs(XanatharParser::IsContext *ctx) = 0;

  virtual void enterString(XanatharParser::StringContext *ctx) = 0;
  virtual void exitString(XanatharParser::StringContext *ctx) = 0;

  virtual void enterArrayIndex(XanatharParser::ArrayIndexContext *ctx) = 0;
  virtual void exitArrayIndex(XanatharParser::ArrayIndexContext *ctx) = 0;

  virtual void enterNewCall(XanatharParser::NewCallContext *ctx) = 0;
  virtual void exitNewCall(XanatharParser::NewCallContext *ctx) = 0;

  virtual void enterEqual(XanatharParser::EqualContext *ctx) = 0;
  virtual void exitEqual(XanatharParser::EqualContext *ctx) = 0;

  virtual void enterIncrement(XanatharParser::IncrementContext *ctx) = 0;
  virtual void exitIncrement(XanatharParser::IncrementContext *ctx) = 0;

  virtual void enterGetRegister(XanatharParser::GetRegisterContext *ctx) = 0;
  virtual void exitGetRegister(XanatharParser::GetRegisterContext *ctx) = 0;

  virtual void enterBinaryOr(XanatharParser::BinaryOrContext *ctx) = 0;
  virtual void exitBinaryOr(XanatharParser::BinaryOrContext *ctx) = 0;

  virtual void enterDivision(XanatharParser::DivisionContext *ctx) = 0;
  virtual void exitDivision(XanatharParser::DivisionContext *ctx) = 0;

  virtual void enterFunctionCall(XanatharParser::FunctionCallContext *ctx) = 0;
  virtual void exitFunctionCall(XanatharParser::FunctionCallContext *ctx) = 0;

  virtual void enterLess(XanatharParser::LessContext *ctx) = 0;
  virtual void exitLess(XanatharParser::LessContext *ctx) = 0;

  virtual void enterParentheses(XanatharParser::ParenthesesContext *ctx) = 0;
  virtual void exitParentheses(XanatharParser::ParenthesesContext *ctx) = 0;

  virtual void enterDereference(XanatharParser::DereferenceContext *ctx) = 0;
  virtual void exitDereference(XanatharParser::DereferenceContext *ctx) = 0;

  virtual void enterGenericCall(XanatharParser::GenericCallContext *ctx) = 0;
  virtual void exitGenericCall(XanatharParser::GenericCallContext *ctx) = 0;

  virtual void enterGetTypeOf(XanatharParser::GetTypeOfContext *ctx) = 0;
  virtual void exitGetTypeOf(XanatharParser::GetTypeOfContext *ctx) = 0;

  virtual void enterAddition(XanatharParser::AdditionContext *ctx) = 0;
  virtual void exitAddition(XanatharParser::AdditionContext *ctx) = 0;

  virtual void enterNotEqual(XanatharParser::NotEqualContext *ctx) = 0;
  virtual void exitNotEqual(XanatharParser::NotEqualContext *ctx) = 0;

  virtual void enterDecrement(XanatharParser::DecrementContext *ctx) = 0;
  virtual void exitDecrement(XanatharParser::DecrementContext *ctx) = 0;

  virtual void enterReference(XanatharParser::ReferenceContext *ctx) = 0;
  virtual void exitReference(XanatharParser::ReferenceContext *ctx) = 0;

  virtual void enterModulo(XanatharParser::ModuloContext *ctx) = 0;
  virtual void exitModulo(XanatharParser::ModuloContext *ctx) = 0;

  virtual void enterRawVariable(XanatharParser::RawVariableContext *ctx) = 0;
  virtual void exitRawVariable(XanatharParser::RawVariableContext *ctx) = 0;

  virtual void enterVariablePhrase(XanatharParser::VariablePhraseContext *ctx) = 0;
  virtual void exitVariablePhrase(XanatharParser::VariablePhraseContext *ctx) = 0;

  virtual void enterGreaterOrEqual(XanatharParser::GreaterOrEqualContext *ctx) = 0;
  virtual void exitGreaterOrEqual(XanatharParser::GreaterOrEqualContext *ctx) = 0;

  virtual void enterRange(XanatharParser::RangeContext *ctx) = 0;
  virtual void exitRange(XanatharParser::RangeContext *ctx) = 0;

  virtual void enterNumberConst(XanatharParser::NumberConstContext *ctx) = 0;
  virtual void exitNumberConst(XanatharParser::NumberConstContext *ctx) = 0;

  virtual void enterFloatConst(XanatharParser::FloatConstContext *ctx) = 0;
  virtual void exitFloatConst(XanatharParser::FloatConstContext *ctx) = 0;

  virtual void enterRotateRight(XanatharParser::RotateRightContext *ctx) = 0;
  virtual void exitRotateRight(XanatharParser::RotateRightContext *ctx) = 0;

  virtual void enterCasting(XanatharParser::CastingContext *ctx) = 0;
  virtual void exitCasting(XanatharParser::CastingContext *ctx) = 0;

  virtual void enterSubtraction(XanatharParser::SubtractionContext *ctx) = 0;
  virtual void exitSubtraction(XanatharParser::SubtractionContext *ctx) = 0;

  virtual void enterGreater(XanatharParser::GreaterContext *ctx) = 0;
  virtual void exitGreater(XanatharParser::GreaterContext *ctx) = 0;

  virtual void enterSpec(XanatharParser::SpecContext *ctx) = 0;
  virtual void exitSpec(XanatharParser::SpecContext *ctx) = 0;

  virtual void enterRotateLeft(XanatharParser::RotateLeftContext *ctx) = 0;
  virtual void exitRotateLeft(XanatharParser::RotateLeftContext *ctx) = 0;

  virtual void enterPhrase(XanatharParser::PhraseContext *ctx) = 0;
  virtual void exitPhrase(XanatharParser::PhraseContext *ctx) = 0;

  virtual void enterStart(XanatharParser::StartContext *ctx) = 0;
  virtual void exitStart(XanatharParser::StartContext *ctx) = 0;

  virtual void enterAsm_(XanatharParser::Asm_Context *ctx) = 0;
  virtual void exitAsm_(XanatharParser::Asm_Context *ctx) = 0;


};

