
// Generated from Xanathar.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "XanatharParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by XanatharParser.
 */
class  XanatharVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by XanatharParser.
   */
    virtual antlrcpp::Any visitPrep(XanatharParser::PrepContext *context) = 0;

    virtual antlrcpp::Any visitString_inner(XanatharParser::String_innerContext *context) = 0;

    virtual antlrcpp::Any visitEscaped_string(XanatharParser::Escaped_stringContext *context) = 0;

    virtual antlrcpp::Any visitLetter(XanatharParser::LetterContext *context) = 0;

    virtual antlrcpp::Any visitCname(XanatharParser::CnameContext *context) = 0;

    virtual antlrcpp::Any visitWord(XanatharParser::WordContext *context) = 0;

    virtual antlrcpp::Any visitStr(XanatharParser::StrContext *context) = 0;

    virtual antlrcpp::Any visitCast(XanatharParser::CastContext *context) = 0;

    virtual antlrcpp::Any visitUse_statement(XanatharParser::Use_statementContext *context) = 0;

    virtual antlrcpp::Any visitDeclare_statement(XanatharParser::Declare_statementContext *context) = 0;

    virtual antlrcpp::Any visitVariable_phrase(XanatharParser::Variable_phraseContext *context) = 0;

    virtual antlrcpp::Any visitRaw_var(XanatharParser::Raw_varContext *context) = 0;

    virtual antlrcpp::Any visitAs_phrase(XanatharParser::As_phraseContext *context) = 0;

    virtual antlrcpp::Any visitTypespec(XanatharParser::TypespecContext *context) = 0;

    virtual antlrcpp::Any visitVariable_setting(XanatharParser::Variable_settingContext *context) = 0;

    virtual antlrcpp::Any visitLet(XanatharParser::LetContext *context) = 0;

    virtual antlrcpp::Any visitClass_(XanatharParser::Class_Context *context) = 0;

    virtual antlrcpp::Any visitClass__specs(XanatharParser::Class__specsContext *context) = 0;

    virtual antlrcpp::Any visitExtends_(XanatharParser::Extends_Context *context) = 0;

    virtual antlrcpp::Any visitInterface_(XanatharParser::Interface_Context *context) = 0;

    virtual antlrcpp::Any visitInterface_fn(XanatharParser::Interface_fnContext *context) = 0;

    virtual antlrcpp::Any visitDeclared_var(XanatharParser::Declared_varContext *context) = 0;

    virtual antlrcpp::Any visitTypes(XanatharParser::TypesContext *context) = 0;

    virtual antlrcpp::Any visitInheritance(XanatharParser::InheritanceContext *context) = 0;

    virtual antlrcpp::Any visitFncall(XanatharParser::FncallContext *context) = 0;

    virtual antlrcpp::Any visitGeneric_call(XanatharParser::Generic_callContext *context) = 0;

    virtual antlrcpp::Any visitGp(XanatharParser::GpContext *context) = 0;

    virtual antlrcpp::Any visitFn(XanatharParser::FnContext *context) = 0;

    virtual antlrcpp::Any visitHook(XanatharParser::HookContext *context) = 0;

    virtual antlrcpp::Any visitTemplate_(XanatharParser::Template_Context *context) = 0;

    virtual antlrcpp::Any visitClass_es(XanatharParser::Class_esContext *context) = 0;

    virtual antlrcpp::Any visitClass__(XanatharParser::Class__Context *context) = 0;

    virtual antlrcpp::Any visitVirtual_fn(XanatharParser::Virtual_fnContext *context) = 0;

    virtual antlrcpp::Any visitOverride(XanatharParser::OverrideContext *context) = 0;

    virtual antlrcpp::Any visitOverride_name(XanatharParser::Override_nameContext *context) = 0;

    virtual antlrcpp::Any visitOverride_param(XanatharParser::Override_paramContext *context) = 0;

    virtual antlrcpp::Any visitLambda(XanatharParser::LambdaContext *context) = 0;

    virtual antlrcpp::Any visitParams(XanatharParser::ParamsContext *context) = 0;

    virtual antlrcpp::Any visitIf_(XanatharParser::If_Context *context) = 0;

    virtual antlrcpp::Any visitElse_(XanatharParser::Else_Context *context) = 0;

    virtual antlrcpp::Any visitFor_(XanatharParser::For_Context *context) = 0;

    virtual antlrcpp::Any visitWhile_(XanatharParser::While_Context *context) = 0;

    virtual antlrcpp::Any visitRet(XanatharParser::RetContext *context) = 0;

    virtual antlrcpp::Any visitMatch_(XanatharParser::Match_Context *context) = 0;

    virtual antlrcpp::Any visitMatch_arm(XanatharParser::Match_armContext *context) = 0;

    virtual antlrcpp::Any visitDefault_arm(XanatharParser::Default_armContext *context) = 0;

    virtual antlrcpp::Any visitRef(XanatharParser::RefContext *context) = 0;

    virtual antlrcpp::Any visitPtr(XanatharParser::PtrContext *context) = 0;

    virtual antlrcpp::Any visitDeref(XanatharParser::DerefContext *context) = 0;

    virtual antlrcpp::Any visitNumber(XanatharParser::NumberContext *context) = 0;

    virtual antlrcpp::Any visitFloat_(XanatharParser::Float_Context *context) = 0;

    virtual antlrcpp::Any visitBreak_(XanatharParser::Break_Context *context) = 0;

    virtual antlrcpp::Any visitSkip(XanatharParser::SkipContext *context) = 0;

    virtual antlrcpp::Any visitNew_(XanatharParser::New_Context *context) = 0;

    virtual antlrcpp::Any visitCall_sealed_class_(XanatharParser::Call_sealed_class_Context *context) = 0;

    virtual antlrcpp::Any visitLock(XanatharParser::LockContext *context) = 0;

    virtual antlrcpp::Any visitUnlock(XanatharParser::UnlockContext *context) = 0;

    virtual antlrcpp::Any visitInc(XanatharParser::IncContext *context) = 0;

    virtual antlrcpp::Any visitDec(XanatharParser::DecContext *context) = 0;

    virtual antlrcpp::Any visitReg(XanatharParser::RegContext *context) = 0;

    virtual antlrcpp::Any visitReg_set(XanatharParser::Reg_setContext *context) = 0;

    virtual antlrcpp::Any visitBinaryNot(XanatharParser::BinaryNotContext *context) = 0;

    virtual antlrcpp::Any visitLambdaCreation(XanatharParser::LambdaCreationContext *context) = 0;

    virtual antlrcpp::Any visitLessOrEqual(XanatharParser::LessOrEqualContext *context) = 0;

    virtual antlrcpp::Any visitMultiplication(XanatharParser::MultiplicationContext *context) = 0;

    virtual antlrcpp::Any visitBinaryAnd(XanatharParser::BinaryAndContext *context) = 0;

    virtual antlrcpp::Any visitBareword(XanatharParser::BarewordContext *context) = 0;

    virtual antlrcpp::Any visitCallSealedClass(XanatharParser::CallSealedClassContext *context) = 0;

    virtual antlrcpp::Any visitBinaryXor(XanatharParser::BinaryXorContext *context) = 0;

    virtual antlrcpp::Any visitGetSizeOf(XanatharParser::GetSizeOfContext *context) = 0;

    virtual antlrcpp::Any visitPointer(XanatharParser::PointerContext *context) = 0;

    virtual antlrcpp::Any visitIs(XanatharParser::IsContext *context) = 0;

    virtual antlrcpp::Any visitString(XanatharParser::StringContext *context) = 0;

    virtual antlrcpp::Any visitArrayIndex(XanatharParser::ArrayIndexContext *context) = 0;

    virtual antlrcpp::Any visitNewCall(XanatharParser::NewCallContext *context) = 0;

    virtual antlrcpp::Any visitEqual(XanatharParser::EqualContext *context) = 0;

    virtual antlrcpp::Any visitIncrement(XanatharParser::IncrementContext *context) = 0;

    virtual antlrcpp::Any visitGetRegister(XanatharParser::GetRegisterContext *context) = 0;

    virtual antlrcpp::Any visitBinaryOr(XanatharParser::BinaryOrContext *context) = 0;

    virtual antlrcpp::Any visitDivision(XanatharParser::DivisionContext *context) = 0;

    virtual antlrcpp::Any visitFunctionCall(XanatharParser::FunctionCallContext *context) = 0;

    virtual antlrcpp::Any visitLess(XanatharParser::LessContext *context) = 0;

    virtual antlrcpp::Any visitParentheses(XanatharParser::ParenthesesContext *context) = 0;

    virtual antlrcpp::Any visitDereference(XanatharParser::DereferenceContext *context) = 0;

    virtual antlrcpp::Any visitGenericCall(XanatharParser::GenericCallContext *context) = 0;

    virtual antlrcpp::Any visitGetTypeOf(XanatharParser::GetTypeOfContext *context) = 0;

    virtual antlrcpp::Any visitAddition(XanatharParser::AdditionContext *context) = 0;

    virtual antlrcpp::Any visitNotEqual(XanatharParser::NotEqualContext *context) = 0;

    virtual antlrcpp::Any visitDecrement(XanatharParser::DecrementContext *context) = 0;

    virtual antlrcpp::Any visitReference(XanatharParser::ReferenceContext *context) = 0;

    virtual antlrcpp::Any visitModulo(XanatharParser::ModuloContext *context) = 0;

    virtual antlrcpp::Any visitRawVariable(XanatharParser::RawVariableContext *context) = 0;

    virtual antlrcpp::Any visitVariablePhrase(XanatharParser::VariablePhraseContext *context) = 0;

    virtual antlrcpp::Any visitGreaterOrEqual(XanatharParser::GreaterOrEqualContext *context) = 0;

    virtual antlrcpp::Any visitRange(XanatharParser::RangeContext *context) = 0;

    virtual antlrcpp::Any visitNumberConst(XanatharParser::NumberConstContext *context) = 0;

    virtual antlrcpp::Any visitFloatConst(XanatharParser::FloatConstContext *context) = 0;

    virtual antlrcpp::Any visitRotateRight(XanatharParser::RotateRightContext *context) = 0;

    virtual antlrcpp::Any visitCasting(XanatharParser::CastingContext *context) = 0;

    virtual antlrcpp::Any visitSubtraction(XanatharParser::SubtractionContext *context) = 0;

    virtual antlrcpp::Any visitGreater(XanatharParser::GreaterContext *context) = 0;

    virtual antlrcpp::Any visitSpec(XanatharParser::SpecContext *context) = 0;

    virtual antlrcpp::Any visitRotateLeft(XanatharParser::RotateLeftContext *context) = 0;

    virtual antlrcpp::Any visitPhrase(XanatharParser::PhraseContext *context) = 0;

    virtual antlrcpp::Any visitStart(XanatharParser::StartContext *context) = 0;

    virtual antlrcpp::Any visitAsm_(XanatharParser::Asm_Context *context) = 0;


};

