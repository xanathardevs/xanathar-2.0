
// Generated from Xanathar.g4 by ANTLR 4.7.1


#include "XanatharListener.h"
#include "XanatharVisitor.h"

#include "XanatharParser.h"


using namespace antlrcpp;
using namespace antlr4;

XanatharParser::XanatharParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

XanatharParser::~XanatharParser() {
  delete _interpreter;
}

std::string XanatharParser::getGrammarFileName() const {
  return "Xanathar.g4";
}

const std::vector<std::string>& XanatharParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& XanatharParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- PrepContext ------------------------------------------------------------------

XanatharParser::PrepContext::PrepContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t XanatharParser::PrepContext::getRuleIndex() const {
  return XanatharParser::RulePrep;
}

void XanatharParser::PrepContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPrep(this);
}

void XanatharParser::PrepContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPrep(this);
}


antlrcpp::Any XanatharParser::PrepContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitPrep(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::PrepContext* XanatharParser::prep() {
  PrepContext *_localctx = _tracker.createInstance<PrepContext>(_ctx, getState());
  enterRule(_localctx, 0, XanatharParser::RulePrep);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(130);
    match(XanatharParser::T__0);
    setState(132); 
    _errHandler->sync(this);
    alt = 1 + 1;
    do {
      switch (alt) {
        case 1 + 1: {
              setState(131);
              matchWildcard();
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(134); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 0, _ctx);
    } while (alt != 1 && alt != atn::ATN::INVALID_ALT_NUMBER);
    setState(136);
    match(XanatharParser::T__0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- String_innerContext ------------------------------------------------------------------

XanatharParser::String_innerContext::String_innerContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t XanatharParser::String_innerContext::getRuleIndex() const {
  return XanatharParser::RuleString_inner;
}

void XanatharParser::String_innerContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterString_inner(this);
}

void XanatharParser::String_innerContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitString_inner(this);
}


antlrcpp::Any XanatharParser::String_innerContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitString_inner(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::String_innerContext* XanatharParser::string_inner() {
  String_innerContext *_localctx = _tracker.createInstance<String_innerContext>(_ctx, getState());
  enterRule(_localctx, 2, XanatharParser::RuleString_inner);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(140);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(138);
      _la = _input->LA(1);
      if (_la == 0 || _la == Token::EOF || (_la == XanatharParser::T__1)) {
      _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(139);
      match(XanatharParser::T__2);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Escaped_stringContext ------------------------------------------------------------------

XanatharParser::Escaped_stringContext::Escaped_stringContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::String_innerContext *> XanatharParser::Escaped_stringContext::string_inner() {
  return getRuleContexts<XanatharParser::String_innerContext>();
}

XanatharParser::String_innerContext* XanatharParser::Escaped_stringContext::string_inner(size_t i) {
  return getRuleContext<XanatharParser::String_innerContext>(i);
}


size_t XanatharParser::Escaped_stringContext::getRuleIndex() const {
  return XanatharParser::RuleEscaped_string;
}

void XanatharParser::Escaped_stringContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterEscaped_string(this);
}

void XanatharParser::Escaped_stringContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitEscaped_string(this);
}


antlrcpp::Any XanatharParser::Escaped_stringContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitEscaped_string(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Escaped_stringContext* XanatharParser::escaped_string() {
  Escaped_stringContext *_localctx = _tracker.createInstance<Escaped_stringContext>(_ctx, getState());
  enterRule(_localctx, 4, XanatharParser::RuleEscaped_string);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(142);
    match(XanatharParser::T__1);
    setState(146);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__2)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__5)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__9)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__12)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__15)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__18)
      | (1ULL << XanatharParser::T__19)
      | (1ULL << XanatharParser::T__20)
      | (1ULL << XanatharParser::T__21)
      | (1ULL << XanatharParser::T__22)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__28)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__31)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__34)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__37)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__39)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__43)
      | (1ULL << XanatharParser::T__44)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__47)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__51)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__54)
      | (1ULL << XanatharParser::T__55)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__57)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59)
      | (1ULL << XanatharParser::T__60)
      | (1ULL << XanatharParser::T__61)
      | (1ULL << XanatharParser::T__62))) != 0) || ((((_la - 64) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 64)) & ((1ULL << (XanatharParser::T__63 - 64))
      | (1ULL << (XanatharParser::T__64 - 64))
      | (1ULL << (XanatharParser::T__65 - 64))
      | (1ULL << (XanatharParser::T__66 - 64))
      | (1ULL << (XanatharParser::T__67 - 64))
      | (1ULL << (XanatharParser::T__68 - 64))
      | (1ULL << (XanatharParser::T__69 - 64))
      | (1ULL << (XanatharParser::T__70 - 64))
      | (1ULL << (XanatharParser::T__71 - 64))
      | (1ULL << (XanatharParser::T__72 - 64))
      | (1ULL << (XanatharParser::T__73 - 64))
      | (1ULL << (XanatharParser::T__74 - 64))
      | (1ULL << (XanatharParser::WS - 64))
      | (1ULL << (XanatharParser::COMMENT - 64))
      | (1ULL << (XanatharParser::DIGIT - 64))
      | (1ULL << (XanatharParser::LCASELETT - 64))
      | (1ULL << (XanatharParser::UCASELET - 64))
      | (1ULL << (XanatharParser::OP - 64)))) != 0)) {
      setState(143);
      string_inner();
      setState(148);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(149);
    match(XanatharParser::T__1);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LetterContext ------------------------------------------------------------------

XanatharParser::LetterContext::LetterContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* XanatharParser::LetterContext::UCASELET() {
  return getToken(XanatharParser::UCASELET, 0);
}

tree::TerminalNode* XanatharParser::LetterContext::LCASELETT() {
  return getToken(XanatharParser::LCASELETT, 0);
}


size_t XanatharParser::LetterContext::getRuleIndex() const {
  return XanatharParser::RuleLetter;
}

void XanatharParser::LetterContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLetter(this);
}

void XanatharParser::LetterContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLetter(this);
}


antlrcpp::Any XanatharParser::LetterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitLetter(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::LetterContext* XanatharParser::letter() {
  LetterContext *_localctx = _tracker.createInstance<LetterContext>(_ctx, getState());
  enterRule(_localctx, 6, XanatharParser::RuleLetter);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(151);
    _la = _input->LA(1);
    if (!(_la == XanatharParser::LCASELETT

    || _la == XanatharParser::UCASELET)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CnameContext ------------------------------------------------------------------

XanatharParser::CnameContext::CnameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::LetterContext *> XanatharParser::CnameContext::letter() {
  return getRuleContexts<XanatharParser::LetterContext>();
}

XanatharParser::LetterContext* XanatharParser::CnameContext::letter(size_t i) {
  return getRuleContext<XanatharParser::LetterContext>(i);
}

std::vector<tree::TerminalNode *> XanatharParser::CnameContext::DIGIT() {
  return getTokens(XanatharParser::DIGIT);
}

tree::TerminalNode* XanatharParser::CnameContext::DIGIT(size_t i) {
  return getToken(XanatharParser::DIGIT, i);
}


size_t XanatharParser::CnameContext::getRuleIndex() const {
  return XanatharParser::RuleCname;
}

void XanatharParser::CnameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCname(this);
}

void XanatharParser::CnameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCname(this);
}


antlrcpp::Any XanatharParser::CnameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitCname(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::CnameContext* XanatharParser::cname() {
  CnameContext *_localctx = _tracker.createInstance<CnameContext>(_ctx, getState());
  enterRule(_localctx, 8, XanatharParser::RuleCname);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(155);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__3: {
        setState(153);
        match(XanatharParser::T__3);
        break;
      }

      case XanatharParser::LCASELETT:
      case XanatharParser::UCASELET: {
        setState(154);
        letter();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(162);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(160);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case XanatharParser::T__3: {
            setState(157);
            match(XanatharParser::T__3);
            break;
          }

          case XanatharParser::LCASELETT:
          case XanatharParser::UCASELET: {
            setState(158);
            letter();
            break;
          }

          case XanatharParser::DIGIT: {
            setState(159);
            match(XanatharParser::DIGIT);
            break;
          }

        default:
          throw NoViableAltException(this);
        } 
      }
      setState(164);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WordContext ------------------------------------------------------------------

XanatharParser::WordContext::WordContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::CnameContext* XanatharParser::WordContext::cname() {
  return getRuleContext<XanatharParser::CnameContext>(0);
}


size_t XanatharParser::WordContext::getRuleIndex() const {
  return XanatharParser::RuleWord;
}

void XanatharParser::WordContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterWord(this);
}

void XanatharParser::WordContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitWord(this);
}


antlrcpp::Any XanatharParser::WordContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitWord(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::WordContext* XanatharParser::word() {
  WordContext *_localctx = _tracker.createInstance<WordContext>(_ctx, getState());
  enterRule(_localctx, 10, XanatharParser::RuleWord);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(165);
    cname();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StrContext ------------------------------------------------------------------

XanatharParser::StrContext::StrContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Escaped_stringContext* XanatharParser::StrContext::escaped_string() {
  return getRuleContext<XanatharParser::Escaped_stringContext>(0);
}


size_t XanatharParser::StrContext::getRuleIndex() const {
  return XanatharParser::RuleStr;
}

void XanatharParser::StrContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStr(this);
}

void XanatharParser::StrContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStr(this);
}


antlrcpp::Any XanatharParser::StrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitStr(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::StrContext* XanatharParser::str() {
  StrContext *_localctx = _tracker.createInstance<StrContext>(_ctx, getState());
  enterRule(_localctx, 12, XanatharParser::RuleStr);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(167);
    escaped_string();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CastContext ------------------------------------------------------------------

XanatharParser::CastContext::CastContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::CastContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::As_phraseContext* XanatharParser::CastContext::as_phrase() {
  return getRuleContext<XanatharParser::As_phraseContext>(0);
}


size_t XanatharParser::CastContext::getRuleIndex() const {
  return XanatharParser::RuleCast;
}

void XanatharParser::CastContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCast(this);
}

void XanatharParser::CastContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCast(this);
}


antlrcpp::Any XanatharParser::CastContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitCast(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::CastContext* XanatharParser::cast() {
  CastContext *_localctx = _tracker.createInstance<CastContext>(_ctx, getState());
  enterRule(_localctx, 14, XanatharParser::RuleCast);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(169);
    match(XanatharParser::T__4);
    setState(170);
    expression(0);
    setState(171);
    as_phrase();
    setState(172);
    match(XanatharParser::T__5);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Use_statementContext ------------------------------------------------------------------

XanatharParser::Use_statementContext::Use_statementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::Use_statementContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}


size_t XanatharParser::Use_statementContext::getRuleIndex() const {
  return XanatharParser::RuleUse_statement;
}

void XanatharParser::Use_statementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterUse_statement(this);
}

void XanatharParser::Use_statementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitUse_statement(this);
}


antlrcpp::Any XanatharParser::Use_statementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitUse_statement(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Use_statementContext* XanatharParser::use_statement() {
  Use_statementContext *_localctx = _tracker.createInstance<Use_statementContext>(_ctx, getState());
  enterRule(_localctx, 16, XanatharParser::RuleUse_statement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(174);
    match(XanatharParser::T__6);
    setState(175);
    word();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Declare_statementContext ------------------------------------------------------------------

XanatharParser::Declare_statementContext::Declare_statementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Variable_phraseContext* XanatharParser::Declare_statementContext::variable_phrase() {
  return getRuleContext<XanatharParser::Variable_phraseContext>(0);
}

XanatharParser::As_phraseContext* XanatharParser::Declare_statementContext::as_phrase() {
  return getRuleContext<XanatharParser::As_phraseContext>(0);
}

XanatharParser::ExpressionContext* XanatharParser::Declare_statementContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::Declare_statementContext::getRuleIndex() const {
  return XanatharParser::RuleDeclare_statement;
}

void XanatharParser::Declare_statementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDeclare_statement(this);
}

void XanatharParser::Declare_statementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDeclare_statement(this);
}


antlrcpp::Any XanatharParser::Declare_statementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDeclare_statement(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Declare_statementContext* XanatharParser::declare_statement() {
  Declare_statementContext *_localctx = _tracker.createInstance<Declare_statementContext>(_ctx, getState());
  enterRule(_localctx, 18, XanatharParser::RuleDeclare_statement);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(177);
    match(XanatharParser::T__7);
    setState(178);
    variable_phrase();
    setState(179);
    as_phrase();
    setState(184);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__8) {
      setState(180);
      match(XanatharParser::T__8);
      setState(181);
      expression(0);
      setState(182);
      match(XanatharParser::T__9);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Variable_phraseContext ------------------------------------------------------------------

XanatharParser::Variable_phraseContext::Variable_phraseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::Variable_phraseContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}


size_t XanatharParser::Variable_phraseContext::getRuleIndex() const {
  return XanatharParser::RuleVariable_phrase;
}

void XanatharParser::Variable_phraseContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariable_phrase(this);
}

void XanatharParser::Variable_phraseContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariable_phrase(this);
}


antlrcpp::Any XanatharParser::Variable_phraseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitVariable_phrase(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Variable_phraseContext* XanatharParser::variable_phrase() {
  Variable_phraseContext *_localctx = _tracker.createInstance<Variable_phraseContext>(_ctx, getState());
  enterRule(_localctx, 20, XanatharParser::RuleVariable_phrase);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(186);
    match(XanatharParser::T__10);
    setState(187);
    word();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Raw_varContext ------------------------------------------------------------------

XanatharParser::Raw_varContext::Raw_varContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::Raw_varContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}


size_t XanatharParser::Raw_varContext::getRuleIndex() const {
  return XanatharParser::RuleRaw_var;
}

void XanatharParser::Raw_varContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRaw_var(this);
}

void XanatharParser::Raw_varContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRaw_var(this);
}


antlrcpp::Any XanatharParser::Raw_varContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitRaw_var(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Raw_varContext* XanatharParser::raw_var() {
  Raw_varContext *_localctx = _tracker.createInstance<Raw_varContext>(_ctx, getState());
  enterRule(_localctx, 22, XanatharParser::RuleRaw_var);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(189);
    match(XanatharParser::T__11);
    setState(190);
    word();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- As_phraseContext ------------------------------------------------------------------

XanatharParser::As_phraseContext::As_phraseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::TypespecContext* XanatharParser::As_phraseContext::typespec() {
  return getRuleContext<XanatharParser::TypespecContext>(0);
}

XanatharParser::WordContext* XanatharParser::As_phraseContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

std::vector<XanatharParser::NumberContext *> XanatharParser::As_phraseContext::number() {
  return getRuleContexts<XanatharParser::NumberContext>();
}

XanatharParser::NumberContext* XanatharParser::As_phraseContext::number(size_t i) {
  return getRuleContext<XanatharParser::NumberContext>(i);
}


size_t XanatharParser::As_phraseContext::getRuleIndex() const {
  return XanatharParser::RuleAs_phrase;
}

void XanatharParser::As_phraseContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAs_phrase(this);
}

void XanatharParser::As_phraseContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAs_phrase(this);
}


antlrcpp::Any XanatharParser::As_phraseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitAs_phrase(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::As_phraseContext* XanatharParser::as_phrase() {
  As_phraseContext *_localctx = _tracker.createInstance<As_phraseContext>(_ctx, getState());
  enterRule(_localctx, 24, XanatharParser::RuleAs_phrase);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(192);
    match(XanatharParser::T__12);
    setState(195);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__13:
      case XanatharParser::T__14: {
        setState(193);
        typespec();
        break;
      }

      case XanatharParser::T__3:
      case XanatharParser::LCASELETT:
      case XanatharParser::UCASELET: {
        setState(194);
        word();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(203);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == XanatharParser::T__4) {
      setState(197);
      match(XanatharParser::T__4);
      setState(198);
      number();
      setState(199);
      match(XanatharParser::T__5);
      setState(205);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypespecContext ------------------------------------------------------------------

XanatharParser::TypespecContext::TypespecContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::TypespecContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}


size_t XanatharParser::TypespecContext::getRuleIndex() const {
  return XanatharParser::RuleTypespec;
}

void XanatharParser::TypespecContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTypespec(this);
}

void XanatharParser::TypespecContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTypespec(this);
}


antlrcpp::Any XanatharParser::TypespecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitTypespec(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::TypespecContext* XanatharParser::typespec() {
  TypespecContext *_localctx = _tracker.createInstance<TypespecContext>(_ctx, getState());
  enterRule(_localctx, 26, XanatharParser::RuleTypespec);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(207); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(206);
      _la = _input->LA(1);
      if (!(_la == XanatharParser::T__13

      || _la == XanatharParser::T__14)) {
      _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(209); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == XanatharParser::T__13

    || _la == XanatharParser::T__14);
    setState(211);
    word();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Variable_settingContext ------------------------------------------------------------------

XanatharParser::Variable_settingContext::Variable_settingContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Variable_phraseContext* XanatharParser::Variable_settingContext::variable_phrase() {
  return getRuleContext<XanatharParser::Variable_phraseContext>(0);
}

XanatharParser::RefContext* XanatharParser::Variable_settingContext::ref() {
  return getRuleContext<XanatharParser::RefContext>(0);
}

std::vector<XanatharParser::ExpressionContext *> XanatharParser::Variable_settingContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::Variable_settingContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::FncallContext* XanatharParser::Variable_settingContext::fncall() {
  return getRuleContext<XanatharParser::FncallContext>(0);
}


size_t XanatharParser::Variable_settingContext::getRuleIndex() const {
  return XanatharParser::RuleVariable_setting;
}

void XanatharParser::Variable_settingContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariable_setting(this);
}

void XanatharParser::Variable_settingContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariable_setting(this);
}


antlrcpp::Any XanatharParser::Variable_settingContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitVariable_setting(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Variable_settingContext* XanatharParser::variable_setting() {
  Variable_settingContext *_localctx = _tracker.createInstance<Variable_settingContext>(_ctx, getState());
  enterRule(_localctx, 28, XanatharParser::RuleVariable_setting);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(215);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx)) {
    case 1: {
      setState(213);
      variable_phrase();
      break;
    }

    case 2: {
      setState(214);
      ref();
      break;
    }

    }
    setState(221);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__8) {
      setState(217);
      match(XanatharParser::T__8);
      setState(218);
      expression(0);
      setState(219);
      match(XanatharParser::T__9);
    }
    setState(223);
    match(XanatharParser::T__15);
    setState(226);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx)) {
    case 1: {
      setState(224);
      expression(0);
      break;
    }

    case 2: {
      setState(225);
      fncall();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LetContext ------------------------------------------------------------------

XanatharParser::LetContext::LetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Variable_phraseContext* XanatharParser::LetContext::variable_phrase() {
  return getRuleContext<XanatharParser::Variable_phraseContext>(0);
}

XanatharParser::ExpressionContext* XanatharParser::LetContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::LetContext::getRuleIndex() const {
  return XanatharParser::RuleLet;
}

void XanatharParser::LetContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLet(this);
}

void XanatharParser::LetContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLet(this);
}


antlrcpp::Any XanatharParser::LetContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitLet(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::LetContext* XanatharParser::let() {
  LetContext *_localctx = _tracker.createInstance<LetContext>(_ctx, getState());
  enterRule(_localctx, 30, XanatharParser::RuleLet);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(228);
    match(XanatharParser::T__16);
    setState(229);
    variable_phrase();
    setState(230);
    match(XanatharParser::T__15);
    setState(231);
    expression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class_Context ------------------------------------------------------------------

XanatharParser::Class_Context::Class_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Class__specsContext* XanatharParser::Class_Context::class__specs() {
  return getRuleContext<XanatharParser::Class__specsContext>(0);
}

XanatharParser::WordContext* XanatharParser::Class_Context::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

XanatharParser::TypesContext* XanatharParser::Class_Context::types() {
  return getRuleContext<XanatharParser::TypesContext>(0);
}

std::vector<XanatharParser::InheritanceContext *> XanatharParser::Class_Context::inheritance() {
  return getRuleContexts<XanatharParser::InheritanceContext>();
}

XanatharParser::InheritanceContext* XanatharParser::Class_Context::inheritance(size_t i) {
  return getRuleContext<XanatharParser::InheritanceContext>(i);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::Class_Context::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::Class_Context::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::Class_Context::getRuleIndex() const {
  return XanatharParser::RuleClass_;
}

void XanatharParser::Class_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass_(this);
}

void XanatharParser::Class_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass_(this);
}


antlrcpp::Any XanatharParser::Class_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitClass_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Class_Context* XanatharParser::class_() {
  Class_Context *_localctx = _tracker.createInstance<Class_Context>(_ctx, getState());
  enterRule(_localctx, 32, XanatharParser::RuleClass_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(233);
    class__specs();
    setState(234);
    match(XanatharParser::T__17);
    setState(235);
    word();
    setState(236);
    match(XanatharParser::T__18);
    setState(237);
    types();
    setState(238);
    match(XanatharParser::T__19);
    setState(239);
    inheritance();
    setState(242);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__20) {
      setState(240);
      match(XanatharParser::T__20);
      setState(241);
      inheritance();
    }
    setState(244);
    match(XanatharParser::T__21);

    setState(248);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(245);
      phrase();
      setState(250);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(251);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class__specsContext ------------------------------------------------------------------

XanatharParser::Class__specsContext::Class__specsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t XanatharParser::Class__specsContext::getRuleIndex() const {
  return XanatharParser::RuleClass__specs;
}

void XanatharParser::Class__specsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass__specs(this);
}

void XanatharParser::Class__specsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass__specs(this);
}


antlrcpp::Any XanatharParser::Class__specsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitClass__specs(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Class__specsContext* XanatharParser::class__specs() {
  Class__specsContext *_localctx = _tracker.createInstance<Class__specsContext>(_ctx, getState());
  enterRule(_localctx, 34, XanatharParser::RuleClass__specs);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(256);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == XanatharParser::T__23

    || _la == XanatharParser::T__24) {
      setState(253);
      _la = _input->LA(1);
      if (!(_la == XanatharParser::T__23

      || _la == XanatharParser::T__24)) {
      _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(258);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Extends_Context ------------------------------------------------------------------

XanatharParser::Extends_Context::Extends_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::Extends_Context::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::Extends_Context::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::Extends_Context::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::Extends_Context::getRuleIndex() const {
  return XanatharParser::RuleExtends_;
}

void XanatharParser::Extends_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExtends_(this);
}

void XanatharParser::Extends_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExtends_(this);
}


antlrcpp::Any XanatharParser::Extends_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitExtends_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Extends_Context* XanatharParser::extends_() {
  Extends_Context *_localctx = _tracker.createInstance<Extends_Context>(_ctx, getState());
  enterRule(_localctx, 36, XanatharParser::RuleExtends_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(259);
    match(XanatharParser::T__25);
    setState(260);
    word();
    setState(261);
    match(XanatharParser::T__21);

    setState(265);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(262);
      phrase();
      setState(267);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(268);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Interface_Context ------------------------------------------------------------------

XanatharParser::Interface_Context::Interface_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::Interface_Context::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

std::vector<XanatharParser::Interface_fnContext *> XanatharParser::Interface_Context::interface_fn() {
  return getRuleContexts<XanatharParser::Interface_fnContext>();
}

XanatharParser::Interface_fnContext* XanatharParser::Interface_Context::interface_fn(size_t i) {
  return getRuleContext<XanatharParser::Interface_fnContext>(i);
}


size_t XanatharParser::Interface_Context::getRuleIndex() const {
  return XanatharParser::RuleInterface_;
}

void XanatharParser::Interface_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterInterface_(this);
}

void XanatharParser::Interface_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitInterface_(this);
}


antlrcpp::Any XanatharParser::Interface_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitInterface_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Interface_Context* XanatharParser::interface_() {
  Interface_Context *_localctx = _tracker.createInstance<Interface_Context>(_ctx, getState());
  enterRule(_localctx, 38, XanatharParser::RuleInterface_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(270);
    match(XanatharParser::T__26);
    setState(271);
    word();
    setState(272);
    match(XanatharParser::T__21);
    setState(276);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == XanatharParser::T__27) {
      setState(273);
      interface_fn();
      setState(278);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(279);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Interface_fnContext ------------------------------------------------------------------

XanatharParser::Interface_fnContext::Interface_fnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::Interface_fnContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

XanatharParser::As_phraseContext* XanatharParser::Interface_fnContext::as_phrase() {
  return getRuleContext<XanatharParser::As_phraseContext>(0);
}

std::vector<XanatharParser::TypespecContext *> XanatharParser::Interface_fnContext::typespec() {
  return getRuleContexts<XanatharParser::TypespecContext>();
}

XanatharParser::TypespecContext* XanatharParser::Interface_fnContext::typespec(size_t i) {
  return getRuleContext<XanatharParser::TypespecContext>(i);
}


size_t XanatharParser::Interface_fnContext::getRuleIndex() const {
  return XanatharParser::RuleInterface_fn;
}

void XanatharParser::Interface_fnContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterInterface_fn(this);
}

void XanatharParser::Interface_fnContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitInterface_fn(this);
}


antlrcpp::Any XanatharParser::Interface_fnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitInterface_fn(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Interface_fnContext* XanatharParser::interface_fn() {
  Interface_fnContext *_localctx = _tracker.createInstance<Interface_fnContext>(_ctx, getState());
  enterRule(_localctx, 40, XanatharParser::RuleInterface_fn);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(281);
    match(XanatharParser::T__27);
    setState(282);
    word();
    setState(283);
    match(XanatharParser::T__4);
    setState(289);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 18, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(284);
        typespec();
        setState(285);
        match(XanatharParser::T__28); 
      }
      setState(291);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 18, _ctx);
    }
    setState(293);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__13

    || _la == XanatharParser::T__14) {
      setState(292);
      typespec();
    }
    setState(295);
    match(XanatharParser::T__5);
    setState(296);
    as_phrase();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Declared_varContext ------------------------------------------------------------------

XanatharParser::Declared_varContext::Declared_varContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Variable_phraseContext* XanatharParser::Declared_varContext::variable_phrase() {
  return getRuleContext<XanatharParser::Variable_phraseContext>(0);
}

XanatharParser::As_phraseContext* XanatharParser::Declared_varContext::as_phrase() {
  return getRuleContext<XanatharParser::As_phraseContext>(0);
}


size_t XanatharParser::Declared_varContext::getRuleIndex() const {
  return XanatharParser::RuleDeclared_var;
}

void XanatharParser::Declared_varContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDeclared_var(this);
}

void XanatharParser::Declared_varContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDeclared_var(this);
}


antlrcpp::Any XanatharParser::Declared_varContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDeclared_var(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Declared_varContext* XanatharParser::declared_var() {
  Declared_varContext *_localctx = _tracker.createInstance<Declared_varContext>(_ctx, getState());
  enterRule(_localctx, 42, XanatharParser::RuleDeclared_var);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(298);
    variable_phrase();
    setState(299);
    as_phrase();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypesContext ------------------------------------------------------------------

XanatharParser::TypesContext::TypesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::Declared_varContext *> XanatharParser::TypesContext::declared_var() {
  return getRuleContexts<XanatharParser::Declared_varContext>();
}

XanatharParser::Declared_varContext* XanatharParser::TypesContext::declared_var(size_t i) {
  return getRuleContext<XanatharParser::Declared_varContext>(i);
}


size_t XanatharParser::TypesContext::getRuleIndex() const {
  return XanatharParser::RuleTypes;
}

void XanatharParser::TypesContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTypes(this);
}

void XanatharParser::TypesContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTypes(this);
}


antlrcpp::Any XanatharParser::TypesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitTypes(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::TypesContext* XanatharParser::types() {
  TypesContext *_localctx = _tracker.createInstance<TypesContext>(_ctx, getState());
  enterRule(_localctx, 44, XanatharParser::RuleTypes);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(310);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__10) {
      setState(306);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 20, _ctx);
      while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
        if (alt == 1) {
          setState(301);
          declared_var();
          setState(302);
          match(XanatharParser::T__28); 
        }
        setState(308);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 20, _ctx);
      }
      setState(309);
      declared_var();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InheritanceContext ------------------------------------------------------------------

XanatharParser::InheritanceContext::InheritanceContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::WordContext *> XanatharParser::InheritanceContext::word() {
  return getRuleContexts<XanatharParser::WordContext>();
}

XanatharParser::WordContext* XanatharParser::InheritanceContext::word(size_t i) {
  return getRuleContext<XanatharParser::WordContext>(i);
}


size_t XanatharParser::InheritanceContext::getRuleIndex() const {
  return XanatharParser::RuleInheritance;
}

void XanatharParser::InheritanceContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterInheritance(this);
}

void XanatharParser::InheritanceContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitInheritance(this);
}


antlrcpp::Any XanatharParser::InheritanceContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitInheritance(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::InheritanceContext* XanatharParser::inheritance() {
  InheritanceContext *_localctx = _tracker.createInstance<InheritanceContext>(_ctx, getState());
  enterRule(_localctx, 46, XanatharParser::RuleInheritance);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(324);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__8) {
      setState(312);
      match(XanatharParser::T__8);
      setState(318);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
      while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
        if (alt == 1) {
          setState(313);
          word();
          setState(314);
          match(XanatharParser::T__28); 
        }
        setState(320);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
      }
      setState(321);
      word();
      setState(322);
      match(XanatharParser::T__9);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FncallContext ------------------------------------------------------------------

XanatharParser::FncallContext::FncallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::ExpressionContext *> XanatharParser::FncallContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::FncallContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}


size_t XanatharParser::FncallContext::getRuleIndex() const {
  return XanatharParser::RuleFncall;
}

void XanatharParser::FncallContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFncall(this);
}

void XanatharParser::FncallContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFncall(this);
}


antlrcpp::Any XanatharParser::FncallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitFncall(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::FncallContext* XanatharParser::fncall() {
  FncallContext *_localctx = _tracker.createInstance<FncallContext>(_ctx, getState());
  enterRule(_localctx, 48, XanatharParser::RuleFncall);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(326);
    expression(0);
    setState(327);
    match(XanatharParser::T__4);
    setState(333);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(328);
        expression(0);
        setState(329);
        match(XanatharParser::T__28); 
      }
      setState(335);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
    }
    setState(337);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(336);
      expression(0);
    }
    setState(339);
    match(XanatharParser::T__5);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Generic_callContext ------------------------------------------------------------------

XanatharParser::Generic_callContext::Generic_callContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::ExpressionContext *> XanatharParser::Generic_callContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::Generic_callContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::GpContext* XanatharParser::Generic_callContext::gp() {
  return getRuleContext<XanatharParser::GpContext>(0);
}


size_t XanatharParser::Generic_callContext::getRuleIndex() const {
  return XanatharParser::RuleGeneric_call;
}

void XanatharParser::Generic_callContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGeneric_call(this);
}

void XanatharParser::Generic_callContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGeneric_call(this);
}


antlrcpp::Any XanatharParser::Generic_callContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGeneric_call(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Generic_callContext* XanatharParser::generic_call() {
  Generic_callContext *_localctx = _tracker.createInstance<Generic_callContext>(_ctx, getState());
  enterRule(_localctx, 50, XanatharParser::RuleGeneric_call);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(341);
    expression(0);
    setState(342);
    match(XanatharParser::T__18);
    setState(343);
    gp();
    setState(344);
    match(XanatharParser::T__19);
    setState(345);
    match(XanatharParser::T__4);
    setState(351);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(346);
        expression(0);
        setState(347);
        match(XanatharParser::T__28); 
      }
      setState(353);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 26, _ctx);
    }
    setState(355);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(354);
      expression(0);
    }
    setState(357);
    match(XanatharParser::T__5);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- GpContext ------------------------------------------------------------------

XanatharParser::GpContext::GpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::TypespecContext *> XanatharParser::GpContext::typespec() {
  return getRuleContexts<XanatharParser::TypespecContext>();
}

XanatharParser::TypespecContext* XanatharParser::GpContext::typespec(size_t i) {
  return getRuleContext<XanatharParser::TypespecContext>(i);
}

std::vector<XanatharParser::WordContext *> XanatharParser::GpContext::word() {
  return getRuleContexts<XanatharParser::WordContext>();
}

XanatharParser::WordContext* XanatharParser::GpContext::word(size_t i) {
  return getRuleContext<XanatharParser::WordContext>(i);
}


size_t XanatharParser::GpContext::getRuleIndex() const {
  return XanatharParser::RuleGp;
}

void XanatharParser::GpContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGp(this);
}

void XanatharParser::GpContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGp(this);
}


antlrcpp::Any XanatharParser::GpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGp(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::GpContext* XanatharParser::gp() {
  GpContext *_localctx = _tracker.createInstance<GpContext>(_ctx, getState());
  enterRule(_localctx, 52, XanatharParser::RuleGp);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(367);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 29, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(361);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case XanatharParser::T__13:
          case XanatharParser::T__14: {
            setState(359);
            typespec();
            break;
          }

          case XanatharParser::T__3:
          case XanatharParser::LCASELETT:
          case XanatharParser::UCASELET: {
            setState(360);
            word();
            break;
          }

        default:
          throw NoViableAltException(this);
        }
        setState(363);
        match(XanatharParser::T__28); 
      }
      setState(369);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 29, _ctx);
    }
    setState(372);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__13:
      case XanatharParser::T__14: {
        setState(370);
        typespec();
        break;
      }

      case XanatharParser::T__3:
      case XanatharParser::LCASELETT:
      case XanatharParser::UCASELET: {
        setState(371);
        word();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FnContext ------------------------------------------------------------------

XanatharParser::FnContext::FnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::FnContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

XanatharParser::ParamsContext* XanatharParser::FnContext::params() {
  return getRuleContext<XanatharParser::ParamsContext>(0);
}

XanatharParser::As_phraseContext* XanatharParser::FnContext::as_phrase() {
  return getRuleContext<XanatharParser::As_phraseContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::FnContext::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::FnContext::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::FnContext::getRuleIndex() const {
  return XanatharParser::RuleFn;
}

void XanatharParser::FnContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFn(this);
}

void XanatharParser::FnContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFn(this);
}


antlrcpp::Any XanatharParser::FnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitFn(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::FnContext* XanatharParser::fn() {
  FnContext *_localctx = _tracker.createInstance<FnContext>(_ctx, getState());
  enterRule(_localctx, 54, XanatharParser::RuleFn);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(374);
    match(XanatharParser::T__27);
    setState(375);
    word();
    setState(376);
    match(XanatharParser::T__4);
    setState(377);
    params();
    setState(378);
    match(XanatharParser::T__5);
    setState(379);
    as_phrase();
    setState(380);
    match(XanatharParser::T__21);
    setState(384);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(381);
      phrase();
      setState(386);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(387);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- HookContext ------------------------------------------------------------------

XanatharParser::HookContext::HookContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* XanatharParser::HookContext::OP() {
  return getToken(XanatharParser::OP, 0);
}

XanatharParser::ParamsContext* XanatharParser::HookContext::params() {
  return getRuleContext<XanatharParser::ParamsContext>(0);
}

XanatharParser::As_phraseContext* XanatharParser::HookContext::as_phrase() {
  return getRuleContext<XanatharParser::As_phraseContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::HookContext::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::HookContext::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::HookContext::getRuleIndex() const {
  return XanatharParser::RuleHook;
}

void XanatharParser::HookContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterHook(this);
}

void XanatharParser::HookContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitHook(this);
}


antlrcpp::Any XanatharParser::HookContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitHook(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::HookContext* XanatharParser::hook() {
  HookContext *_localctx = _tracker.createInstance<HookContext>(_ctx, getState());
  enterRule(_localctx, 56, XanatharParser::RuleHook);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(389);
    match(XanatharParser::T__29);
    setState(390);
    match(XanatharParser::OP);
    setState(391);
    match(XanatharParser::T__4);
    setState(392);
    params();
    setState(393);
    match(XanatharParser::T__5);
    setState(395);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__12) {
      setState(394);
      as_phrase();
    }
    setState(397);
    match(XanatharParser::T__21);
    setState(401);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(398);
      phrase();
      setState(403);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(404);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Template_Context ------------------------------------------------------------------

XanatharParser::Template_Context::Template_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Class_esContext* XanatharParser::Template_Context::class_es() {
  return getRuleContext<XanatharParser::Class_esContext>(0);
}

XanatharParser::WordContext* XanatharParser::Template_Context::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

XanatharParser::ParamsContext* XanatharParser::Template_Context::params() {
  return getRuleContext<XanatharParser::ParamsContext>(0);
}

XanatharParser::As_phraseContext* XanatharParser::Template_Context::as_phrase() {
  return getRuleContext<XanatharParser::As_phraseContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::Template_Context::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::Template_Context::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::Template_Context::getRuleIndex() const {
  return XanatharParser::RuleTemplate_;
}

void XanatharParser::Template_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTemplate_(this);
}

void XanatharParser::Template_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTemplate_(this);
}


antlrcpp::Any XanatharParser::Template_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitTemplate_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Template_Context* XanatharParser::template_() {
  Template_Context *_localctx = _tracker.createInstance<Template_Context>(_ctx, getState());
  enterRule(_localctx, 58, XanatharParser::RuleTemplate_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(406);
    match(XanatharParser::T__30);
    setState(407);
    match(XanatharParser::T__18);
    setState(408);
    class_es();
    setState(409);
    match(XanatharParser::T__19);
    setState(410);
    word();
    setState(411);
    match(XanatharParser::T__4);
    setState(412);
    params();
    setState(413);
    match(XanatharParser::T__5);
    setState(414);
    as_phrase();
    setState(415);
    match(XanatharParser::T__21);
    setState(419);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(416);
      phrase();
      setState(421);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(422);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class_esContext ------------------------------------------------------------------

XanatharParser::Class_esContext::Class_esContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::Class__Context *> XanatharParser::Class_esContext::class__() {
  return getRuleContexts<XanatharParser::Class__Context>();
}

XanatharParser::Class__Context* XanatharParser::Class_esContext::class__(size_t i) {
  return getRuleContext<XanatharParser::Class__Context>(i);
}


size_t XanatharParser::Class_esContext::getRuleIndex() const {
  return XanatharParser::RuleClass_es;
}

void XanatharParser::Class_esContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass_es(this);
}

void XanatharParser::Class_esContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass_es(this);
}


antlrcpp::Any XanatharParser::Class_esContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitClass_es(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Class_esContext* XanatharParser::class_es() {
  Class_esContext *_localctx = _tracker.createInstance<Class_esContext>(_ctx, getState());
  enterRule(_localctx, 60, XanatharParser::RuleClass_es);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(429);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 35, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(424);
        class__();
        setState(425);
        match(XanatharParser::T__28); 
      }
      setState(431);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 35, _ctx);
    }
    setState(432);
    class__();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Class__Context ------------------------------------------------------------------

XanatharParser::Class__Context::Class__Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::Class__Context::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}


size_t XanatharParser::Class__Context::getRuleIndex() const {
  return XanatharParser::RuleClass__;
}

void XanatharParser::Class__Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClass__(this);
}

void XanatharParser::Class__Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClass__(this);
}


antlrcpp::Any XanatharParser::Class__Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitClass__(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Class__Context* XanatharParser::class__() {
  Class__Context *_localctx = _tracker.createInstance<Class__Context>(_ctx, getState());
  enterRule(_localctx, 62, XanatharParser::RuleClass__);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(434);
    match(XanatharParser::T__31);
    setState(435);
    word();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Virtual_fnContext ------------------------------------------------------------------

XanatharParser::Virtual_fnContext::Virtual_fnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::WordContext *> XanatharParser::Virtual_fnContext::word() {
  return getRuleContexts<XanatharParser::WordContext>();
}

XanatharParser::WordContext* XanatharParser::Virtual_fnContext::word(size_t i) {
  return getRuleContext<XanatharParser::WordContext>(i);
}

std::vector<XanatharParser::TypespecContext *> XanatharParser::Virtual_fnContext::typespec() {
  return getRuleContexts<XanatharParser::TypespecContext>();
}

XanatharParser::TypespecContext* XanatharParser::Virtual_fnContext::typespec(size_t i) {
  return getRuleContext<XanatharParser::TypespecContext>(i);
}


size_t XanatharParser::Virtual_fnContext::getRuleIndex() const {
  return XanatharParser::RuleVirtual_fn;
}

void XanatharParser::Virtual_fnContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVirtual_fn(this);
}

void XanatharParser::Virtual_fnContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVirtual_fn(this);
}


antlrcpp::Any XanatharParser::Virtual_fnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitVirtual_fn(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Virtual_fnContext* XanatharParser::virtual_fn() {
  Virtual_fnContext *_localctx = _tracker.createInstance<Virtual_fnContext>(_ctx, getState());
  enterRule(_localctx, 64, XanatharParser::RuleVirtual_fn);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(437);
    match(XanatharParser::T__32);
    setState(438);
    word();
    setState(439);
    match(XanatharParser::T__4);
    setState(445);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 36, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(440);
        typespec();
        setState(441);
        match(XanatharParser::T__28); 
      }
      setState(447);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 36, _ctx);
    }
    setState(449);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__13

    || _la == XanatharParser::T__14) {
      setState(448);
      typespec();
    }
    setState(451);
    match(XanatharParser::T__5);
    setState(452);
    match(XanatharParser::T__12);
    setState(455);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__13:
      case XanatharParser::T__14: {
        setState(453);
        typespec();
        break;
      }

      case XanatharParser::T__3:
      case XanatharParser::LCASELETT:
      case XanatharParser::UCASELET: {
        setState(454);
        word();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- OverrideContext ------------------------------------------------------------------

XanatharParser::OverrideContext::OverrideContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Override_nameContext* XanatharParser::OverrideContext::override_name() {
  return getRuleContext<XanatharParser::Override_nameContext>(0);
}

XanatharParser::Override_paramContext* XanatharParser::OverrideContext::override_param() {
  return getRuleContext<XanatharParser::Override_paramContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::OverrideContext::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::OverrideContext::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::OverrideContext::getRuleIndex() const {
  return XanatharParser::RuleOverride;
}

void XanatharParser::OverrideContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterOverride(this);
}

void XanatharParser::OverrideContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitOverride(this);
}


antlrcpp::Any XanatharParser::OverrideContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitOverride(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::OverrideContext* XanatharParser::override() {
  OverrideContext *_localctx = _tracker.createInstance<OverrideContext>(_ctx, getState());
  enterRule(_localctx, 66, XanatharParser::RuleOverride);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(457);
    match(XanatharParser::T__33);
    setState(458);
    match(XanatharParser::T__11);
    setState(459);
    override_name();
    setState(460);
    match(XanatharParser::T__18);
    setState(461);
    override_param();
    setState(462);
    match(XanatharParser::T__19);
    setState(463);
    match(XanatharParser::T__21);
    setState(467);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(464);
      phrase();
      setState(469);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(470);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Override_nameContext ------------------------------------------------------------------

XanatharParser::Override_nameContext::Override_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::WordContext *> XanatharParser::Override_nameContext::word() {
  return getRuleContexts<XanatharParser::WordContext>();
}

XanatharParser::WordContext* XanatharParser::Override_nameContext::word(size_t i) {
  return getRuleContext<XanatharParser::WordContext>(i);
}


size_t XanatharParser::Override_nameContext::getRuleIndex() const {
  return XanatharParser::RuleOverride_name;
}

void XanatharParser::Override_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterOverride_name(this);
}

void XanatharParser::Override_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitOverride_name(this);
}


antlrcpp::Any XanatharParser::Override_nameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitOverride_name(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Override_nameContext* XanatharParser::override_name() {
  Override_nameContext *_localctx = _tracker.createInstance<Override_nameContext>(_ctx, getState());
  enterRule(_localctx, 68, XanatharParser::RuleOverride_name);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(475); 
    _errHandler->sync(this);
    alt = 1;
    do {
      switch (alt) {
        case 1: {
              setState(472);
              word();
              setState(473);
              match(XanatharParser::T__34);
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(477); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 40, _ctx);
    } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
    setState(479);
    word();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Override_paramContext ------------------------------------------------------------------

XanatharParser::Override_paramContext::Override_paramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::Variable_phraseContext *> XanatharParser::Override_paramContext::variable_phrase() {
  return getRuleContexts<XanatharParser::Variable_phraseContext>();
}

XanatharParser::Variable_phraseContext* XanatharParser::Override_paramContext::variable_phrase(size_t i) {
  return getRuleContext<XanatharParser::Variable_phraseContext>(i);
}


size_t XanatharParser::Override_paramContext::getRuleIndex() const {
  return XanatharParser::RuleOverride_param;
}

void XanatharParser::Override_paramContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterOverride_param(this);
}

void XanatharParser::Override_paramContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitOverride_param(this);
}


antlrcpp::Any XanatharParser::Override_paramContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitOverride_param(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Override_paramContext* XanatharParser::override_param() {
  Override_paramContext *_localctx = _tracker.createInstance<Override_paramContext>(_ctx, getState());
  enterRule(_localctx, 70, XanatharParser::RuleOverride_param);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(486);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 41, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(481);
        variable_phrase();
        setState(482);
        match(XanatharParser::T__28); 
      }
      setState(488);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 41, _ctx);
    }
    setState(490);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__10) {
      setState(489);
      variable_phrase();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LambdaContext ------------------------------------------------------------------

XanatharParser::LambdaContext::LambdaContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ParamsContext* XanatharParser::LambdaContext::params() {
  return getRuleContext<XanatharParser::ParamsContext>(0);
}

XanatharParser::ExpressionContext* XanatharParser::LambdaContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::LambdaContext::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::LambdaContext::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::LambdaContext::getRuleIndex() const {
  return XanatharParser::RuleLambda;
}

void XanatharParser::LambdaContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLambda(this);
}

void XanatharParser::LambdaContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLambda(this);
}


antlrcpp::Any XanatharParser::LambdaContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitLambda(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::LambdaContext* XanatharParser::lambda() {
  LambdaContext *_localctx = _tracker.createInstance<LambdaContext>(_ctx, getState());
  enterRule(_localctx, 72, XanatharParser::RuleLambda);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(492);
    match(XanatharParser::T__35);
    setState(493);
    params();
    setState(494);
    match(XanatharParser::T__34);
    setState(504);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__1:
      case XanatharParser::T__3:
      case XanatharParser::T__4:
      case XanatharParser::T__8:
      case XanatharParser::T__10:
      case XanatharParser::T__11:
      case XanatharParser::T__13:
      case XanatharParser::T__14:
      case XanatharParser::T__35:
      case XanatharParser::T__45:
      case XanatharParser::T__46:
      case XanatharParser::T__56:
      case XanatharParser::T__58:
      case XanatharParser::T__59:
      case XanatharParser::T__71:
      case XanatharParser::T__72:
      case XanatharParser::DIGIT:
      case XanatharParser::LCASELETT:
      case XanatharParser::UCASELET: {
        setState(495);
        expression(0);
        break;
      }

      case XanatharParser::T__21: {
        setState(496);
        match(XanatharParser::T__21);
        setState(500);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while ((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
          | (1ULL << XanatharParser::T__1)
          | (1ULL << XanatharParser::T__3)
          | (1ULL << XanatharParser::T__4)
          | (1ULL << XanatharParser::T__6)
          | (1ULL << XanatharParser::T__7)
          | (1ULL << XanatharParser::T__8)
          | (1ULL << XanatharParser::T__10)
          | (1ULL << XanatharParser::T__11)
          | (1ULL << XanatharParser::T__13)
          | (1ULL << XanatharParser::T__14)
          | (1ULL << XanatharParser::T__16)
          | (1ULL << XanatharParser::T__17)
          | (1ULL << XanatharParser::T__23)
          | (1ULL << XanatharParser::T__24)
          | (1ULL << XanatharParser::T__25)
          | (1ULL << XanatharParser::T__26)
          | (1ULL << XanatharParser::T__27)
          | (1ULL << XanatharParser::T__29)
          | (1ULL << XanatharParser::T__30)
          | (1ULL << XanatharParser::T__32)
          | (1ULL << XanatharParser::T__33)
          | (1ULL << XanatharParser::T__35)
          | (1ULL << XanatharParser::T__36)
          | (1ULL << XanatharParser::T__38)
          | (1ULL << XanatharParser::T__40)
          | (1ULL << XanatharParser::T__41)
          | (1ULL << XanatharParser::T__42)
          | (1ULL << XanatharParser::T__45)
          | (1ULL << XanatharParser::T__46)
          | (1ULL << XanatharParser::T__48)
          | (1ULL << XanatharParser::T__49)
          | (1ULL << XanatharParser::T__50)
          | (1ULL << XanatharParser::T__52)
          | (1ULL << XanatharParser::T__53)
          | (1ULL << XanatharParser::T__56)
          | (1ULL << XanatharParser::T__58)
          | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
          ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
          | (1ULL << (XanatharParser::T__72 - 72))
          | (1ULL << (XanatharParser::T__73 - 72))
          | (1ULL << (XanatharParser::DIGIT - 72))
          | (1ULL << (XanatharParser::LCASELETT - 72))
          | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
          setState(497);
          phrase();
          setState(502);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        setState(503);
        match(XanatharParser::T__22);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParamsContext ------------------------------------------------------------------

XanatharParser::ParamsContext::ParamsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::Variable_phraseContext *> XanatharParser::ParamsContext::variable_phrase() {
  return getRuleContexts<XanatharParser::Variable_phraseContext>();
}

XanatharParser::Variable_phraseContext* XanatharParser::ParamsContext::variable_phrase(size_t i) {
  return getRuleContext<XanatharParser::Variable_phraseContext>(i);
}

std::vector<XanatharParser::As_phraseContext *> XanatharParser::ParamsContext::as_phrase() {
  return getRuleContexts<XanatharParser::As_phraseContext>();
}

XanatharParser::As_phraseContext* XanatharParser::ParamsContext::as_phrase(size_t i) {
  return getRuleContext<XanatharParser::As_phraseContext>(i);
}


size_t XanatharParser::ParamsContext::getRuleIndex() const {
  return XanatharParser::RuleParams;
}

void XanatharParser::ParamsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterParams(this);
}

void XanatharParser::ParamsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitParams(this);
}


antlrcpp::Any XanatharParser::ParamsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitParams(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::ParamsContext* XanatharParser::params() {
  ParamsContext *_localctx = _tracker.createInstance<ParamsContext>(_ctx, getState());
  enterRule(_localctx, 74, XanatharParser::RuleParams);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(512);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 45, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(506);
        variable_phrase();
        setState(507);
        as_phrase();
        setState(508);
        match(XanatharParser::T__28); 
      }
      setState(514);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 45, _ctx);
    }

    setState(515);
    variable_phrase();
    setState(516);
    as_phrase();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- If_Context ------------------------------------------------------------------

XanatharParser::If_Context::If_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::If_Context::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::If_Context::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::If_Context::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}

XanatharParser::Else_Context* XanatharParser::If_Context::else_() {
  return getRuleContext<XanatharParser::Else_Context>(0);
}


size_t XanatharParser::If_Context::getRuleIndex() const {
  return XanatharParser::RuleIf_;
}

void XanatharParser::If_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterIf_(this);
}

void XanatharParser::If_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitIf_(this);
}


antlrcpp::Any XanatharParser::If_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitIf_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::If_Context* XanatharParser::if_() {
  If_Context *_localctx = _tracker.createInstance<If_Context>(_ctx, getState());
  enterRule(_localctx, 76, XanatharParser::RuleIf_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(518);
    match(XanatharParser::T__36);
    setState(519);
    match(XanatharParser::T__4);

    setState(520);
    expression(0);
    setState(521);
    match(XanatharParser::T__5);
    setState(522);
    match(XanatharParser::T__21);
    setState(526);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(523);
      phrase();
      setState(528);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(529);
    match(XanatharParser::T__22);
    setState(531);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__37) {
      setState(530);
      else_();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Else_Context ------------------------------------------------------------------

XanatharParser::Else_Context::Else_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::Else_Context::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::Else_Context::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::Else_Context::getRuleIndex() const {
  return XanatharParser::RuleElse_;
}

void XanatharParser::Else_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterElse_(this);
}

void XanatharParser::Else_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitElse_(this);
}


antlrcpp::Any XanatharParser::Else_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitElse_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Else_Context* XanatharParser::else_() {
  Else_Context *_localctx = _tracker.createInstance<Else_Context>(_ctx, getState());
  enterRule(_localctx, 78, XanatharParser::RuleElse_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(533);
    match(XanatharParser::T__37);
    setState(534);
    match(XanatharParser::T__21);
    setState(538);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(535);
      phrase();
      setState(540);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(541);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- For_Context ------------------------------------------------------------------

XanatharParser::For_Context::For_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::For_Context::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::For_Context::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}

XanatharParser::ExpressionContext* XanatharParser::For_Context::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::For_Context::getRuleIndex() const {
  return XanatharParser::RuleFor_;
}

void XanatharParser::For_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFor_(this);
}

void XanatharParser::For_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFor_(this);
}


antlrcpp::Any XanatharParser::For_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitFor_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::For_Context* XanatharParser::for_() {
  For_Context *_localctx = _tracker.createInstance<For_Context>(_ctx, getState());
  enterRule(_localctx, 80, XanatharParser::RuleFor_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(543);
    match(XanatharParser::T__38);
    setState(544);
    match(XanatharParser::T__4);
    setState(545);
    phrase();
    setState(546);
    expression(0);
    setState(547);
    match(XanatharParser::T__39);
    setState(548);
    phrase();
    setState(549);
    match(XanatharParser::T__5);
    setState(550);
    match(XanatharParser::T__21);
    setState(554);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(551);
      phrase();
      setState(556);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(557);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- While_Context ------------------------------------------------------------------

XanatharParser::While_Context::While_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::While_Context::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::While_Context::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::While_Context::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::While_Context::getRuleIndex() const {
  return XanatharParser::RuleWhile_;
}

void XanatharParser::While_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterWhile_(this);
}

void XanatharParser::While_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitWhile_(this);
}


antlrcpp::Any XanatharParser::While_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitWhile_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::While_Context* XanatharParser::while_() {
  While_Context *_localctx = _tracker.createInstance<While_Context>(_ctx, getState());
  enterRule(_localctx, 82, XanatharParser::RuleWhile_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(559);
    match(XanatharParser::T__40);
    setState(560);
    match(XanatharParser::T__4);
    setState(561);
    expression(0);
    setState(562);
    match(XanatharParser::T__5);
    setState(563);
    match(XanatharParser::T__21);
    setState(567);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(564);
      phrase();
      setState(569);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(570);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RetContext ------------------------------------------------------------------

XanatharParser::RetContext::RetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::RetContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::RetContext::getRuleIndex() const {
  return XanatharParser::RuleRet;
}

void XanatharParser::RetContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRet(this);
}

void XanatharParser::RetContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRet(this);
}


antlrcpp::Any XanatharParser::RetContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitRet(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::RetContext* XanatharParser::ret() {
  RetContext *_localctx = _tracker.createInstance<RetContext>(_ctx, getState());
  enterRule(_localctx, 84, XanatharParser::RuleRet);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(572);
    match(XanatharParser::T__41);
    setState(574);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(573);
      expression(0);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Match_Context ------------------------------------------------------------------

XanatharParser::Match_Context::Match_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::Match_Context::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

std::vector<XanatharParser::Match_armContext *> XanatharParser::Match_Context::match_arm() {
  return getRuleContexts<XanatharParser::Match_armContext>();
}

XanatharParser::Match_armContext* XanatharParser::Match_Context::match_arm(size_t i) {
  return getRuleContext<XanatharParser::Match_armContext>(i);
}

XanatharParser::Default_armContext* XanatharParser::Match_Context::default_arm() {
  return getRuleContext<XanatharParser::Default_armContext>(0);
}


size_t XanatharParser::Match_Context::getRuleIndex() const {
  return XanatharParser::RuleMatch_;
}

void XanatharParser::Match_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterMatch_(this);
}

void XanatharParser::Match_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitMatch_(this);
}


antlrcpp::Any XanatharParser::Match_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitMatch_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Match_Context* XanatharParser::match_() {
  Match_Context *_localctx = _tracker.createInstance<Match_Context>(_ctx, getState());
  enterRule(_localctx, 86, XanatharParser::RuleMatch_);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(576);
    match(XanatharParser::T__42);
    setState(577);
    match(XanatharParser::T__4);
    setState(578);
    expression(0);
    setState(579);
    match(XanatharParser::T__5);
    setState(580);
    match(XanatharParser::T__21);
    setState(586);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 52, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(581);
        match_arm();
        setState(582);
        match(XanatharParser::T__28); 
      }
      setState(588);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 52, _ctx);
    }
    setState(591);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__1:
      case XanatharParser::T__3:
      case XanatharParser::T__4:
      case XanatharParser::T__8:
      case XanatharParser::T__10:
      case XanatharParser::T__11:
      case XanatharParser::T__13:
      case XanatharParser::T__14:
      case XanatharParser::T__35:
      case XanatharParser::T__45:
      case XanatharParser::T__46:
      case XanatharParser::T__56:
      case XanatharParser::T__58:
      case XanatharParser::T__59:
      case XanatharParser::T__71:
      case XanatharParser::T__72:
      case XanatharParser::DIGIT:
      case XanatharParser::LCASELETT:
      case XanatharParser::UCASELET: {
        setState(589);
        match_arm();
        break;
      }

      case XanatharParser::T__44: {
        setState(590);
        default_arm();
        break;
      }

      case XanatharParser::T__22:
      case XanatharParser::T__28: {
        break;
      }

    default:
      break;
    }
    setState(594);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__28) {
      setState(593);
      match(XanatharParser::T__28);
    }
    setState(596);
    match(XanatharParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Match_armContext ------------------------------------------------------------------

XanatharParser::Match_armContext::Match_armContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::Match_armContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::PhraseContext* XanatharParser::Match_armContext::phrase() {
  return getRuleContext<XanatharParser::PhraseContext>(0);
}


size_t XanatharParser::Match_armContext::getRuleIndex() const {
  return XanatharParser::RuleMatch_arm;
}

void XanatharParser::Match_armContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterMatch_arm(this);
}

void XanatharParser::Match_armContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitMatch_arm(this);
}


antlrcpp::Any XanatharParser::Match_armContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitMatch_arm(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Match_armContext* XanatharParser::match_arm() {
  Match_armContext *_localctx = _tracker.createInstance<Match_armContext>(_ctx, getState());
  enterRule(_localctx, 88, XanatharParser::RuleMatch_arm);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(598);
    expression(0);
    setState(599);
    match(XanatharParser::T__43);
    setState(600);
    phrase();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Default_armContext ------------------------------------------------------------------

XanatharParser::Default_armContext::Default_armContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::PhraseContext* XanatharParser::Default_armContext::phrase() {
  return getRuleContext<XanatharParser::PhraseContext>(0);
}


size_t XanatharParser::Default_armContext::getRuleIndex() const {
  return XanatharParser::RuleDefault_arm;
}

void XanatharParser::Default_armContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDefault_arm(this);
}

void XanatharParser::Default_armContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDefault_arm(this);
}


antlrcpp::Any XanatharParser::Default_armContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDefault_arm(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Default_armContext* XanatharParser::default_arm() {
  Default_armContext *_localctx = _tracker.createInstance<Default_armContext>(_ctx, getState());
  enterRule(_localctx, 90, XanatharParser::RuleDefault_arm);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(602);
    match(XanatharParser::T__44);
    setState(603);
    match(XanatharParser::T__43);
    setState(604);
    phrase();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RefContext ------------------------------------------------------------------

XanatharParser::RefContext::RefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::Variable_phraseContext *> XanatharParser::RefContext::variable_phrase() {
  return getRuleContexts<XanatharParser::Variable_phraseContext>();
}

XanatharParser::Variable_phraseContext* XanatharParser::RefContext::variable_phrase(size_t i) {
  return getRuleContext<XanatharParser::Variable_phraseContext>(i);
}

std::vector<XanatharParser::Raw_varContext *> XanatharParser::RefContext::raw_var() {
  return getRuleContexts<XanatharParser::Raw_varContext>();
}

XanatharParser::Raw_varContext* XanatharParser::RefContext::raw_var(size_t i) {
  return getRuleContext<XanatharParser::Raw_varContext>(i);
}


size_t XanatharParser::RefContext::getRuleIndex() const {
  return XanatharParser::RuleRef;
}

void XanatharParser::RefContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRef(this);
}

void XanatharParser::RefContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRef(this);
}


antlrcpp::Any XanatharParser::RefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitRef(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::RefContext* XanatharParser::ref() {
  RefContext *_localctx = _tracker.createInstance<RefContext>(_ctx, getState());
  enterRule(_localctx, 92, XanatharParser::RuleRef);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(608);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__10: {
        setState(606);
        variable_phrase();
        break;
      }

      case XanatharParser::T__11: {
        setState(607);
        raw_var();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(615); 
    _errHandler->sync(this);
    alt = 1;
    do {
      switch (alt) {
        case 1: {
              setState(610);
              match(XanatharParser::T__34);
              setState(613);
              _errHandler->sync(this);
              switch (_input->LA(1)) {
                case XanatharParser::T__10: {
                  setState(611);
                  variable_phrase();
                  break;
                }

                case XanatharParser::T__11: {
                  setState(612);
                  raw_var();
                  break;
                }

              default:
                throw NoViableAltException(this);
              }
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(617); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 57, _ctx);
    } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PtrContext ------------------------------------------------------------------

XanatharParser::PtrContext::PtrContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::PtrContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::PtrContext::getRuleIndex() const {
  return XanatharParser::RulePtr;
}

void XanatharParser::PtrContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPtr(this);
}

void XanatharParser::PtrContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPtr(this);
}


antlrcpp::Any XanatharParser::PtrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitPtr(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::PtrContext* XanatharParser::ptr() {
  PtrContext *_localctx = _tracker.createInstance<PtrContext>(_ctx, getState());
  enterRule(_localctx, 94, XanatharParser::RulePtr);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(619);
    match(XanatharParser::T__14);
    setState(620);
    expression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DerefContext ------------------------------------------------------------------

XanatharParser::DerefContext::DerefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::DerefContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::DerefContext::getRuleIndex() const {
  return XanatharParser::RuleDeref;
}

void XanatharParser::DerefContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDeref(this);
}

void XanatharParser::DerefContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDeref(this);
}


antlrcpp::Any XanatharParser::DerefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDeref(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::DerefContext* XanatharParser::deref() {
  DerefContext *_localctx = _tracker.createInstance<DerefContext>(_ctx, getState());
  enterRule(_localctx, 96, XanatharParser::RuleDeref);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(622);
    match(XanatharParser::T__13);
    setState(623);
    expression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NumberContext ------------------------------------------------------------------

XanatharParser::NumberContext::NumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> XanatharParser::NumberContext::DIGIT() {
  return getTokens(XanatharParser::DIGIT);
}

tree::TerminalNode* XanatharParser::NumberContext::DIGIT(size_t i) {
  return getToken(XanatharParser::DIGIT, i);
}


size_t XanatharParser::NumberContext::getRuleIndex() const {
  return XanatharParser::RuleNumber;
}

void XanatharParser::NumberContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNumber(this);
}

void XanatharParser::NumberContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNumber(this);
}


antlrcpp::Any XanatharParser::NumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitNumber(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::NumberContext* XanatharParser::number() {
  NumberContext *_localctx = _tracker.createInstance<NumberContext>(_ctx, getState());
  enterRule(_localctx, 98, XanatharParser::RuleNumber);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(626);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == XanatharParser::T__45

    || _la == XanatharParser::T__46) {
      setState(625);
      _la = _input->LA(1);
      if (!(_la == XanatharParser::T__45

      || _la == XanatharParser::T__46)) {
      _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
    }
    setState(629); 
    _errHandler->sync(this);
    alt = 1;
    do {
      switch (alt) {
        case 1: {
              setState(628);
              match(XanatharParser::DIGIT);
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(631); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 59, _ctx);
    } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Float_Context ------------------------------------------------------------------

XanatharParser::Float_Context::Float_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::NumberContext *> XanatharParser::Float_Context::number() {
  return getRuleContexts<XanatharParser::NumberContext>();
}

XanatharParser::NumberContext* XanatharParser::Float_Context::number(size_t i) {
  return getRuleContext<XanatharParser::NumberContext>(i);
}


size_t XanatharParser::Float_Context::getRuleIndex() const {
  return XanatharParser::RuleFloat_;
}

void XanatharParser::Float_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFloat_(this);
}

void XanatharParser::Float_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFloat_(this);
}


antlrcpp::Any XanatharParser::Float_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitFloat_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Float_Context* XanatharParser::float_() {
  Float_Context *_localctx = _tracker.createInstance<Float_Context>(_ctx, getState());
  enterRule(_localctx, 100, XanatharParser::RuleFloat_);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(633);
    number();
    setState(634);
    match(XanatharParser::T__47);
    setState(635);
    number();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Break_Context ------------------------------------------------------------------

XanatharParser::Break_Context::Break_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t XanatharParser::Break_Context::getRuleIndex() const {
  return XanatharParser::RuleBreak_;
}

void XanatharParser::Break_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBreak_(this);
}

void XanatharParser::Break_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBreak_(this);
}


antlrcpp::Any XanatharParser::Break_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitBreak_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Break_Context* XanatharParser::break_() {
  Break_Context *_localctx = _tracker.createInstance<Break_Context>(_ctx, getState());
  enterRule(_localctx, 102, XanatharParser::RuleBreak_);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(637);
    match(XanatharParser::T__48);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SkipContext ------------------------------------------------------------------

XanatharParser::SkipContext::SkipContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t XanatharParser::SkipContext::getRuleIndex() const {
  return XanatharParser::RuleSkip;
}

void XanatharParser::SkipContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSkip(this);
}

void XanatharParser::SkipContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSkip(this);
}


antlrcpp::Any XanatharParser::SkipContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitSkip(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::SkipContext* XanatharParser::skip() {
  SkipContext *_localctx = _tracker.createInstance<SkipContext>(_ctx, getState());
  enterRule(_localctx, 104, XanatharParser::RuleSkip);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(639);
    match(XanatharParser::T__49);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- New_Context ------------------------------------------------------------------

XanatharParser::New_Context::New_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::New_Context::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::FncallContext* XanatharParser::New_Context::fncall() {
  return getRuleContext<XanatharParser::FncallContext>(0);
}


size_t XanatharParser::New_Context::getRuleIndex() const {
  return XanatharParser::RuleNew_;
}

void XanatharParser::New_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNew_(this);
}

void XanatharParser::New_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNew_(this);
}


antlrcpp::Any XanatharParser::New_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitNew_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::New_Context* XanatharParser::new_() {
  New_Context *_localctx = _tracker.createInstance<New_Context>(_ctx, getState());
  enterRule(_localctx, 106, XanatharParser::RuleNew_);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(641);
    match(XanatharParser::T__50);
    setState(642);
    expression(0);
    setState(643);
    match(XanatharParser::T__12);
    setState(644);
    fncall();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Call_sealed_class_Context ------------------------------------------------------------------

XanatharParser::Call_sealed_class_Context::Call_sealed_class_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::FncallContext* XanatharParser::Call_sealed_class_Context::fncall() {
  return getRuleContext<XanatharParser::FncallContext>(0);
}

std::vector<XanatharParser::WordContext *> XanatharParser::Call_sealed_class_Context::word() {
  return getRuleContexts<XanatharParser::WordContext>();
}

XanatharParser::WordContext* XanatharParser::Call_sealed_class_Context::word(size_t i) {
  return getRuleContext<XanatharParser::WordContext>(i);
}


size_t XanatharParser::Call_sealed_class_Context::getRuleIndex() const {
  return XanatharParser::RuleCall_sealed_class_;
}

void XanatharParser::Call_sealed_class_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCall_sealed_class_(this);
}

void XanatharParser::Call_sealed_class_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCall_sealed_class_(this);
}


antlrcpp::Any XanatharParser::Call_sealed_class_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitCall_sealed_class_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Call_sealed_class_Context* XanatharParser::call_sealed_class_() {
  Call_sealed_class_Context *_localctx = _tracker.createInstance<Call_sealed_class_Context>(_ctx, getState());
  enterRule(_localctx, 108, XanatharParser::RuleCall_sealed_class_);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(649); 
    _errHandler->sync(this);
    alt = 1;
    do {
      switch (alt) {
        case 1: {
              setState(646);
              word();
              setState(647);
              match(XanatharParser::T__51);
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(651); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 60, _ctx);
    } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
    setState(653);
    fncall();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LockContext ------------------------------------------------------------------

XanatharParser::LockContext::LockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Variable_phraseContext* XanatharParser::LockContext::variable_phrase() {
  return getRuleContext<XanatharParser::Variable_phraseContext>(0);
}


size_t XanatharParser::LockContext::getRuleIndex() const {
  return XanatharParser::RuleLock;
}

void XanatharParser::LockContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLock(this);
}

void XanatharParser::LockContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLock(this);
}


antlrcpp::Any XanatharParser::LockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitLock(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::LockContext* XanatharParser::lock() {
  LockContext *_localctx = _tracker.createInstance<LockContext>(_ctx, getState());
  enterRule(_localctx, 110, XanatharParser::RuleLock);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(655);
    match(XanatharParser::T__52);
    setState(656);
    variable_phrase();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UnlockContext ------------------------------------------------------------------

XanatharParser::UnlockContext::UnlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::Variable_phraseContext* XanatharParser::UnlockContext::variable_phrase() {
  return getRuleContext<XanatharParser::Variable_phraseContext>(0);
}


size_t XanatharParser::UnlockContext::getRuleIndex() const {
  return XanatharParser::RuleUnlock;
}

void XanatharParser::UnlockContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterUnlock(this);
}

void XanatharParser::UnlockContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitUnlock(this);
}


antlrcpp::Any XanatharParser::UnlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitUnlock(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::UnlockContext* XanatharParser::unlock() {
  UnlockContext *_localctx = _tracker.createInstance<UnlockContext>(_ctx, getState());
  enterRule(_localctx, 112, XanatharParser::RuleUnlock);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(658);
    match(XanatharParser::T__53);
    setState(659);
    variable_phrase();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IncContext ------------------------------------------------------------------

XanatharParser::IncContext::IncContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::IncContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::IncContext::getRuleIndex() const {
  return XanatharParser::RuleInc;
}

void XanatharParser::IncContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterInc(this);
}

void XanatharParser::IncContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitInc(this);
}


antlrcpp::Any XanatharParser::IncContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitInc(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::IncContext* XanatharParser::inc() {
  IncContext *_localctx = _tracker.createInstance<IncContext>(_ctx, getState());
  enterRule(_localctx, 114, XanatharParser::RuleInc);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(661);
    expression(0);
    setState(662);
    match(XanatharParser::T__54);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DecContext ------------------------------------------------------------------

XanatharParser::DecContext::DecContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::DecContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::DecContext::getRuleIndex() const {
  return XanatharParser::RuleDec;
}

void XanatharParser::DecContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDec(this);
}

void XanatharParser::DecContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDec(this);
}


antlrcpp::Any XanatharParser::DecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDec(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::DecContext* XanatharParser::dec() {
  DecContext *_localctx = _tracker.createInstance<DecContext>(_ctx, getState());
  enterRule(_localctx, 116, XanatharParser::RuleDec);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(664);
    expression(0);
    setState(665);
    match(XanatharParser::T__55);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RegContext ------------------------------------------------------------------

XanatharParser::RegContext::RegContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::WordContext* XanatharParser::RegContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}


size_t XanatharParser::RegContext::getRuleIndex() const {
  return XanatharParser::RuleReg;
}

void XanatharParser::RegContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterReg(this);
}

void XanatharParser::RegContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitReg(this);
}


antlrcpp::Any XanatharParser::RegContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitReg(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::RegContext* XanatharParser::reg() {
  RegContext *_localctx = _tracker.createInstance<RegContext>(_ctx, getState());
  enterRule(_localctx, 118, XanatharParser::RuleReg);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(667);
    match(XanatharParser::T__56);
    setState(668);
    word();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Reg_setContext ------------------------------------------------------------------

XanatharParser::Reg_setContext::Reg_setContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::RegContext* XanatharParser::Reg_setContext::reg() {
  return getRuleContext<XanatharParser::RegContext>(0);
}

XanatharParser::ExpressionContext* XanatharParser::Reg_setContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}


size_t XanatharParser::Reg_setContext::getRuleIndex() const {
  return XanatharParser::RuleReg_set;
}

void XanatharParser::Reg_setContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterReg_set(this);
}

void XanatharParser::Reg_setContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitReg_set(this);
}


antlrcpp::Any XanatharParser::Reg_setContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitReg_set(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Reg_setContext* XanatharParser::reg_set() {
  Reg_setContext *_localctx = _tracker.createInstance<Reg_setContext>(_ctx, getState());
  enterRule(_localctx, 120, XanatharParser::RuleReg_set);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(670);
    reg();
    setState(671);
    match(XanatharParser::T__57);
    setState(672);
    expression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

XanatharParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t XanatharParser::ExpressionContext::getRuleIndex() const {
  return XanatharParser::RuleExpression;
}

void XanatharParser::ExpressionContext::copyFrom(ExpressionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- BinaryNotContext ------------------------------------------------------------------

XanatharParser::ExpressionContext* XanatharParser::BinaryNotContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::BinaryNotContext::BinaryNotContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::BinaryNotContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBinaryNot(this);
}
void XanatharParser::BinaryNotContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBinaryNot(this);
}

antlrcpp::Any XanatharParser::BinaryNotContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitBinaryNot(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LambdaCreationContext ------------------------------------------------------------------

XanatharParser::ParamsContext* XanatharParser::LambdaCreationContext::params() {
  return getRuleContext<XanatharParser::ParamsContext>(0);
}

XanatharParser::ExpressionContext* XanatharParser::LambdaCreationContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::LambdaCreationContext::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::LambdaCreationContext::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}

XanatharParser::LambdaCreationContext::LambdaCreationContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::LambdaCreationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLambdaCreation(this);
}
void XanatharParser::LambdaCreationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLambdaCreation(this);
}

antlrcpp::Any XanatharParser::LambdaCreationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitLambdaCreation(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LessOrEqualContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::LessOrEqualContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::LessOrEqualContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::LessOrEqualContext::LessOrEqualContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::LessOrEqualContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLessOrEqual(this);
}
void XanatharParser::LessOrEqualContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLessOrEqual(this);
}

antlrcpp::Any XanatharParser::LessOrEqualContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitLessOrEqual(this);
  else
    return visitor->visitChildren(this);
}
//----------------- MultiplicationContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::MultiplicationContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::MultiplicationContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::MultiplicationContext::MultiplicationContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::MultiplicationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterMultiplication(this);
}
void XanatharParser::MultiplicationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitMultiplication(this);
}

antlrcpp::Any XanatharParser::MultiplicationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitMultiplication(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BinaryAndContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::BinaryAndContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::BinaryAndContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::BinaryAndContext::BinaryAndContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::BinaryAndContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBinaryAnd(this);
}
void XanatharParser::BinaryAndContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBinaryAnd(this);
}

antlrcpp::Any XanatharParser::BinaryAndContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitBinaryAnd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BarewordContext ------------------------------------------------------------------

XanatharParser::WordContext* XanatharParser::BarewordContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

XanatharParser::BarewordContext::BarewordContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::BarewordContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBareword(this);
}
void XanatharParser::BarewordContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBareword(this);
}

antlrcpp::Any XanatharParser::BarewordContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitBareword(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CallSealedClassContext ------------------------------------------------------------------

std::vector<XanatharParser::WordContext *> XanatharParser::CallSealedClassContext::word() {
  return getRuleContexts<XanatharParser::WordContext>();
}

XanatharParser::WordContext* XanatharParser::CallSealedClassContext::word(size_t i) {
  return getRuleContext<XanatharParser::WordContext>(i);
}

std::vector<XanatharParser::ExpressionContext *> XanatharParser::CallSealedClassContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::CallSealedClassContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::CallSealedClassContext::CallSealedClassContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::CallSealedClassContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCallSealedClass(this);
}
void XanatharParser::CallSealedClassContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCallSealedClass(this);
}

antlrcpp::Any XanatharParser::CallSealedClassContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitCallSealedClass(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BinaryXorContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::BinaryXorContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::BinaryXorContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::BinaryXorContext::BinaryXorContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::BinaryXorContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBinaryXor(this);
}
void XanatharParser::BinaryXorContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBinaryXor(this);
}

antlrcpp::Any XanatharParser::BinaryXorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitBinaryXor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GetSizeOfContext ------------------------------------------------------------------

XanatharParser::TypespecContext* XanatharParser::GetSizeOfContext::typespec() {
  return getRuleContext<XanatharParser::TypespecContext>(0);
}

XanatharParser::WordContext* XanatharParser::GetSizeOfContext::word() {
  return getRuleContext<XanatharParser::WordContext>(0);
}

XanatharParser::GetSizeOfContext::GetSizeOfContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::GetSizeOfContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGetSizeOf(this);
}
void XanatharParser::GetSizeOfContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGetSizeOf(this);
}

antlrcpp::Any XanatharParser::GetSizeOfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGetSizeOf(this);
  else
    return visitor->visitChildren(this);
}
//----------------- PointerContext ------------------------------------------------------------------

XanatharParser::PtrContext* XanatharParser::PointerContext::ptr() {
  return getRuleContext<XanatharParser::PtrContext>(0);
}

XanatharParser::PointerContext::PointerContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::PointerContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPointer(this);
}
void XanatharParser::PointerContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPointer(this);
}

antlrcpp::Any XanatharParser::PointerContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitPointer(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IsContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::IsContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::IsContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::IsContext::IsContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::IsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterIs(this);
}
void XanatharParser::IsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitIs(this);
}

antlrcpp::Any XanatharParser::IsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitIs(this);
  else
    return visitor->visitChildren(this);
}
//----------------- StringContext ------------------------------------------------------------------

XanatharParser::StrContext* XanatharParser::StringContext::str() {
  return getRuleContext<XanatharParser::StrContext>(0);
}

XanatharParser::StringContext::StringContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::StringContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterString(this);
}
void XanatharParser::StringContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitString(this);
}

antlrcpp::Any XanatharParser::StringContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitString(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ArrayIndexContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::ArrayIndexContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::ArrayIndexContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::ArrayIndexContext::ArrayIndexContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::ArrayIndexContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterArrayIndex(this);
}
void XanatharParser::ArrayIndexContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitArrayIndex(this);
}

antlrcpp::Any XanatharParser::ArrayIndexContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitArrayIndex(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NewCallContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::NewCallContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::NewCallContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::NewCallContext::NewCallContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::NewCallContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNewCall(this);
}
void XanatharParser::NewCallContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNewCall(this);
}

antlrcpp::Any XanatharParser::NewCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitNewCall(this);
  else
    return visitor->visitChildren(this);
}
//----------------- EqualContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::EqualContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::EqualContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::EqualContext::EqualContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::EqualContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterEqual(this);
}
void XanatharParser::EqualContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitEqual(this);
}

antlrcpp::Any XanatharParser::EqualContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitEqual(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IncrementContext ------------------------------------------------------------------

XanatharParser::ExpressionContext* XanatharParser::IncrementContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::IncrementContext::IncrementContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::IncrementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterIncrement(this);
}
void XanatharParser::IncrementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitIncrement(this);
}

antlrcpp::Any XanatharParser::IncrementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitIncrement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GetRegisterContext ------------------------------------------------------------------

XanatharParser::RegContext* XanatharParser::GetRegisterContext::reg() {
  return getRuleContext<XanatharParser::RegContext>(0);
}

XanatharParser::GetRegisterContext::GetRegisterContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::GetRegisterContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGetRegister(this);
}
void XanatharParser::GetRegisterContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGetRegister(this);
}

antlrcpp::Any XanatharParser::GetRegisterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGetRegister(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BinaryOrContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::BinaryOrContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::BinaryOrContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::BinaryOrContext::BinaryOrContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::BinaryOrContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBinaryOr(this);
}
void XanatharParser::BinaryOrContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBinaryOr(this);
}

antlrcpp::Any XanatharParser::BinaryOrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitBinaryOr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DivisionContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::DivisionContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::DivisionContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::DivisionContext::DivisionContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::DivisionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDivision(this);
}
void XanatharParser::DivisionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDivision(this);
}

antlrcpp::Any XanatharParser::DivisionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDivision(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FunctionCallContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::FunctionCallContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::FunctionCallContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::FunctionCallContext::FunctionCallContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::FunctionCallContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionCall(this);
}
void XanatharParser::FunctionCallContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionCall(this);
}

antlrcpp::Any XanatharParser::FunctionCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitFunctionCall(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LessContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::LessContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::LessContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::LessContext::LessContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::LessContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLess(this);
}
void XanatharParser::LessContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLess(this);
}

antlrcpp::Any XanatharParser::LessContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitLess(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ParenthesesContext ------------------------------------------------------------------

XanatharParser::ExpressionContext* XanatharParser::ParenthesesContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::ParenthesesContext::ParenthesesContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::ParenthesesContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterParentheses(this);
}
void XanatharParser::ParenthesesContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitParentheses(this);
}

antlrcpp::Any XanatharParser::ParenthesesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitParentheses(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DereferenceContext ------------------------------------------------------------------

XanatharParser::DerefContext* XanatharParser::DereferenceContext::deref() {
  return getRuleContext<XanatharParser::DerefContext>(0);
}

XanatharParser::DereferenceContext::DereferenceContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::DereferenceContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDereference(this);
}
void XanatharParser::DereferenceContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDereference(this);
}

antlrcpp::Any XanatharParser::DereferenceContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDereference(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GenericCallContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::GenericCallContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::GenericCallContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::GpContext* XanatharParser::GenericCallContext::gp() {
  return getRuleContext<XanatharParser::GpContext>(0);
}

XanatharParser::GenericCallContext::GenericCallContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::GenericCallContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGenericCall(this);
}
void XanatharParser::GenericCallContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGenericCall(this);
}

antlrcpp::Any XanatharParser::GenericCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGenericCall(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GetTypeOfContext ------------------------------------------------------------------

XanatharParser::ExpressionContext* XanatharParser::GetTypeOfContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::GetTypeOfContext::GetTypeOfContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::GetTypeOfContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGetTypeOf(this);
}
void XanatharParser::GetTypeOfContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGetTypeOf(this);
}

antlrcpp::Any XanatharParser::GetTypeOfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGetTypeOf(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AdditionContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::AdditionContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::AdditionContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::AdditionContext::AdditionContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::AdditionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAddition(this);
}
void XanatharParser::AdditionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAddition(this);
}

antlrcpp::Any XanatharParser::AdditionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitAddition(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NotEqualContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::NotEqualContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::NotEqualContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::NotEqualContext::NotEqualContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::NotEqualContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNotEqual(this);
}
void XanatharParser::NotEqualContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNotEqual(this);
}

antlrcpp::Any XanatharParser::NotEqualContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitNotEqual(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DecrementContext ------------------------------------------------------------------

XanatharParser::ExpressionContext* XanatharParser::DecrementContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::DecrementContext::DecrementContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::DecrementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDecrement(this);
}
void XanatharParser::DecrementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDecrement(this);
}

antlrcpp::Any XanatharParser::DecrementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitDecrement(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ReferenceContext ------------------------------------------------------------------

XanatharParser::RefContext* XanatharParser::ReferenceContext::ref() {
  return getRuleContext<XanatharParser::RefContext>(0);
}

XanatharParser::ReferenceContext::ReferenceContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::ReferenceContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterReference(this);
}
void XanatharParser::ReferenceContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitReference(this);
}

antlrcpp::Any XanatharParser::ReferenceContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitReference(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ModuloContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::ModuloContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::ModuloContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::ModuloContext::ModuloContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::ModuloContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterModulo(this);
}
void XanatharParser::ModuloContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitModulo(this);
}

antlrcpp::Any XanatharParser::ModuloContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitModulo(this);
  else
    return visitor->visitChildren(this);
}
//----------------- RawVariableContext ------------------------------------------------------------------

XanatharParser::Raw_varContext* XanatharParser::RawVariableContext::raw_var() {
  return getRuleContext<XanatharParser::Raw_varContext>(0);
}

XanatharParser::RawVariableContext::RawVariableContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::RawVariableContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRawVariable(this);
}
void XanatharParser::RawVariableContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRawVariable(this);
}

antlrcpp::Any XanatharParser::RawVariableContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitRawVariable(this);
  else
    return visitor->visitChildren(this);
}
//----------------- VariablePhraseContext ------------------------------------------------------------------

XanatharParser::Variable_phraseContext* XanatharParser::VariablePhraseContext::variable_phrase() {
  return getRuleContext<XanatharParser::Variable_phraseContext>(0);
}

XanatharParser::VariablePhraseContext::VariablePhraseContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::VariablePhraseContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariablePhrase(this);
}
void XanatharParser::VariablePhraseContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariablePhrase(this);
}

antlrcpp::Any XanatharParser::VariablePhraseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitVariablePhrase(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GreaterOrEqualContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::GreaterOrEqualContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::GreaterOrEqualContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::GreaterOrEqualContext::GreaterOrEqualContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::GreaterOrEqualContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGreaterOrEqual(this);
}
void XanatharParser::GreaterOrEqualContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGreaterOrEqual(this);
}

antlrcpp::Any XanatharParser::GreaterOrEqualContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGreaterOrEqual(this);
  else
    return visitor->visitChildren(this);
}
//----------------- RangeContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::RangeContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::RangeContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::RangeContext::RangeContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::RangeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRange(this);
}
void XanatharParser::RangeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRange(this);
}

antlrcpp::Any XanatharParser::RangeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitRange(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NumberConstContext ------------------------------------------------------------------

XanatharParser::NumberContext* XanatharParser::NumberConstContext::number() {
  return getRuleContext<XanatharParser::NumberContext>(0);
}

XanatharParser::NumberConstContext::NumberConstContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::NumberConstContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNumberConst(this);
}
void XanatharParser::NumberConstContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNumberConst(this);
}

antlrcpp::Any XanatharParser::NumberConstContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitNumberConst(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FloatConstContext ------------------------------------------------------------------

XanatharParser::Float_Context* XanatharParser::FloatConstContext::float_() {
  return getRuleContext<XanatharParser::Float_Context>(0);
}

XanatharParser::FloatConstContext::FloatConstContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::FloatConstContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFloatConst(this);
}
void XanatharParser::FloatConstContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFloatConst(this);
}

antlrcpp::Any XanatharParser::FloatConstContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitFloatConst(this);
  else
    return visitor->visitChildren(this);
}
//----------------- RotateRightContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::RotateRightContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::RotateRightContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::RotateRightContext::RotateRightContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::RotateRightContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRotateRight(this);
}
void XanatharParser::RotateRightContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRotateRight(this);
}

antlrcpp::Any XanatharParser::RotateRightContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitRotateRight(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CastingContext ------------------------------------------------------------------

XanatharParser::CastContext* XanatharParser::CastingContext::cast() {
  return getRuleContext<XanatharParser::CastContext>(0);
}

XanatharParser::CastingContext::CastingContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::CastingContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCasting(this);
}
void XanatharParser::CastingContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCasting(this);
}

antlrcpp::Any XanatharParser::CastingContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitCasting(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SubtractionContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::SubtractionContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::SubtractionContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::SubtractionContext::SubtractionContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::SubtractionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSubtraction(this);
}
void XanatharParser::SubtractionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSubtraction(this);
}

antlrcpp::Any XanatharParser::SubtractionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitSubtraction(this);
  else
    return visitor->visitChildren(this);
}
//----------------- GreaterContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::GreaterContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::GreaterContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::GreaterContext::GreaterContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::GreaterContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGreater(this);
}
void XanatharParser::GreaterContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGreater(this);
}

antlrcpp::Any XanatharParser::GreaterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitGreater(this);
  else
    return visitor->visitChildren(this);
}
//----------------- SpecContext ------------------------------------------------------------------

XanatharParser::ExpressionContext* XanatharParser::SpecContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

std::vector<XanatharParser::WordContext *> XanatharParser::SpecContext::word() {
  return getRuleContexts<XanatharParser::WordContext>();
}

XanatharParser::WordContext* XanatharParser::SpecContext::word(size_t i) {
  return getRuleContext<XanatharParser::WordContext>(i);
}

XanatharParser::SpecContext::SpecContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::SpecContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSpec(this);
}
void XanatharParser::SpecContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSpec(this);
}

antlrcpp::Any XanatharParser::SpecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitSpec(this);
  else
    return visitor->visitChildren(this);
}
//----------------- RotateLeftContext ------------------------------------------------------------------

std::vector<XanatharParser::ExpressionContext *> XanatharParser::RotateLeftContext::expression() {
  return getRuleContexts<XanatharParser::ExpressionContext>();
}

XanatharParser::ExpressionContext* XanatharParser::RotateLeftContext::expression(size_t i) {
  return getRuleContext<XanatharParser::ExpressionContext>(i);
}

XanatharParser::RotateLeftContext::RotateLeftContext(ExpressionContext *ctx) { copyFrom(ctx); }

void XanatharParser::RotateLeftContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRotateLeft(this);
}
void XanatharParser::RotateLeftContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRotateLeft(this);
}

antlrcpp::Any XanatharParser::RotateLeftContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitRotateLeft(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::ExpressionContext* XanatharParser::expression() {
   return expression(0);
}

XanatharParser::ExpressionContext* XanatharParser::expression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  XanatharParser::ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, parentState);
  XanatharParser::ExpressionContext *previousContext = _localctx;
  size_t startState = 122;
  enterRecursionRule(_localctx, 122, XanatharParser::RuleExpression, precedence);

    size_t _la = 0;

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(755);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 69, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<BarewordContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(675);
      word();
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<ParenthesesContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(676);
      match(XanatharParser::T__8);
      setState(677);
      expression(0);
      setState(678);
      match(XanatharParser::T__9);
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<GetRegisterContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(680);
      reg();
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<GetSizeOfContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(681);
      match(XanatharParser::T__58);
      setState(682);
      match(XanatharParser::T__11);
      setState(685);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case XanatharParser::T__13:
        case XanatharParser::T__14: {
          setState(683);
          typespec();
          break;
        }

        case XanatharParser::T__3:
        case XanatharParser::LCASELETT:
        case XanatharParser::UCASELET: {
          setState(684);
          word();
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      break;
    }

    case 5: {
      _localctx = _tracker.createInstance<GetTypeOfContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(687);
      match(XanatharParser::T__59);
      setState(688);
      match(XanatharParser::T__11);
      setState(689);
      expression(36);
      break;
    }

    case 6: {
      _localctx = _tracker.createInstance<ReferenceContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(690);
      ref();
      break;
    }

    case 7: {
      _localctx = _tracker.createInstance<CastingContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(691);
      cast();
      break;
    }

    case 8: {
      _localctx = _tracker.createInstance<PointerContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(692);
      ptr();
      break;
    }

    case 9: {
      _localctx = _tracker.createInstance<DereferenceContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(693);
      deref();
      break;
    }

    case 10: {
      _localctx = _tracker.createInstance<NumberConstContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(694);
      number();
      break;
    }

    case 11: {
      _localctx = _tracker.createInstance<FloatConstContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(695);
      float_();
      break;
    }

    case 12: {
      _localctx = _tracker.createInstance<StringContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(696);
      str();
      break;
    }

    case 13: {
      _localctx = _tracker.createInstance<VariablePhraseContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(697);
      variable_phrase();
      break;
    }

    case 14: {
      _localctx = _tracker.createInstance<RawVariableContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(698);
      raw_var();
      break;
    }

    case 15: {
      _localctx = _tracker.createInstance<LambdaCreationContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(699);
      match(XanatharParser::T__35);
      setState(700);
      params();
      setState(701);
      match(XanatharParser::T__34);
      setState(711);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case XanatharParser::T__1:
        case XanatharParser::T__3:
        case XanatharParser::T__4:
        case XanatharParser::T__8:
        case XanatharParser::T__10:
        case XanatharParser::T__11:
        case XanatharParser::T__13:
        case XanatharParser::T__14:
        case XanatharParser::T__35:
        case XanatharParser::T__45:
        case XanatharParser::T__46:
        case XanatharParser::T__56:
        case XanatharParser::T__58:
        case XanatharParser::T__59:
        case XanatharParser::T__71:
        case XanatharParser::T__72:
        case XanatharParser::DIGIT:
        case XanatharParser::LCASELETT:
        case XanatharParser::UCASELET: {
          setState(702);
          expression(0);
          break;
        }

        case XanatharParser::T__21: {
          setState(703);
          match(XanatharParser::T__21);
          setState(707);
          _errHandler->sync(this);
          _la = _input->LA(1);
          while ((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
            | (1ULL << XanatharParser::T__1)
            | (1ULL << XanatharParser::T__3)
            | (1ULL << XanatharParser::T__4)
            | (1ULL << XanatharParser::T__6)
            | (1ULL << XanatharParser::T__7)
            | (1ULL << XanatharParser::T__8)
            | (1ULL << XanatharParser::T__10)
            | (1ULL << XanatharParser::T__11)
            | (1ULL << XanatharParser::T__13)
            | (1ULL << XanatharParser::T__14)
            | (1ULL << XanatharParser::T__16)
            | (1ULL << XanatharParser::T__17)
            | (1ULL << XanatharParser::T__23)
            | (1ULL << XanatharParser::T__24)
            | (1ULL << XanatharParser::T__25)
            | (1ULL << XanatharParser::T__26)
            | (1ULL << XanatharParser::T__27)
            | (1ULL << XanatharParser::T__29)
            | (1ULL << XanatharParser::T__30)
            | (1ULL << XanatharParser::T__32)
            | (1ULL << XanatharParser::T__33)
            | (1ULL << XanatharParser::T__35)
            | (1ULL << XanatharParser::T__36)
            | (1ULL << XanatharParser::T__38)
            | (1ULL << XanatharParser::T__40)
            | (1ULL << XanatharParser::T__41)
            | (1ULL << XanatharParser::T__42)
            | (1ULL << XanatharParser::T__45)
            | (1ULL << XanatharParser::T__46)
            | (1ULL << XanatharParser::T__48)
            | (1ULL << XanatharParser::T__49)
            | (1ULL << XanatharParser::T__50)
            | (1ULL << XanatharParser::T__52)
            | (1ULL << XanatharParser::T__53)
            | (1ULL << XanatharParser::T__56)
            | (1ULL << XanatharParser::T__58)
            | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
            ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
            | (1ULL << (XanatharParser::T__72 - 72))
            | (1ULL << (XanatharParser::T__73 - 72))
            | (1ULL << (XanatharParser::DIGIT - 72))
            | (1ULL << (XanatharParser::LCASELETT - 72))
            | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
            setState(704);
            phrase();
            setState(709);
            _errHandler->sync(this);
            _la = _input->LA(1);
          }
          setState(710);
          match(XanatharParser::T__22);
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      break;
    }

    case 16: {
      _localctx = _tracker.createInstance<CallSealedClassContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(716); 
      _errHandler->sync(this);
      alt = 1;
      do {
        switch (alt) {
          case 1: {
                setState(713);
                word();
                setState(714);
                match(XanatharParser::T__51);
                break;
              }

        default:
          throw NoViableAltException(this);
        }
        setState(718); 
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 64, _ctx);
      } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
      setState(720);
      word();
      setState(721);
      match(XanatharParser::T__4);
      setState(727);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 65, _ctx);
      while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
        if (alt == 1) {
          setState(722);
          expression(0);
          setState(723);
          match(XanatharParser::T__28); 
        }
        setState(729);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 65, _ctx);
      }
      setState(731);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << XanatharParser::T__1)
        | (1ULL << XanatharParser::T__3)
        | (1ULL << XanatharParser::T__4)
        | (1ULL << XanatharParser::T__8)
        | (1ULL << XanatharParser::T__10)
        | (1ULL << XanatharParser::T__11)
        | (1ULL << XanatharParser::T__13)
        | (1ULL << XanatharParser::T__14)
        | (1ULL << XanatharParser::T__35)
        | (1ULL << XanatharParser::T__45)
        | (1ULL << XanatharParser::T__46)
        | (1ULL << XanatharParser::T__56)
        | (1ULL << XanatharParser::T__58)
        | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
        ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
        | (1ULL << (XanatharParser::T__72 - 72))
        | (1ULL << (XanatharParser::DIGIT - 72))
        | (1ULL << (XanatharParser::LCASELETT - 72))
        | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
        setState(730);
        expression(0);
      }
      setState(733);
      match(XanatharParser::T__5);
      break;
    }

    case 17: {
      _localctx = _tracker.createInstance<BinaryNotContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(735);
      match(XanatharParser::T__71);
      setState(736);
      expression(2);
      break;
    }

    case 18: {
      _localctx = _tracker.createInstance<NewCallContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(737);
      match(XanatharParser::T__72);
      setState(738);
      expression(0);
      setState(739);
      match(XanatharParser::T__12);
      setState(740);
      expression(0);
      setState(741);
      match(XanatharParser::T__4);
      setState(747);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 67, _ctx);
      while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
        if (alt == 1) {
          setState(742);
          expression(0);
          setState(743);
          match(XanatharParser::T__28); 
        }
        setState(749);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 67, _ctx);
      }
      setState(751);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << XanatharParser::T__1)
        | (1ULL << XanatharParser::T__3)
        | (1ULL << XanatharParser::T__4)
        | (1ULL << XanatharParser::T__8)
        | (1ULL << XanatharParser::T__10)
        | (1ULL << XanatharParser::T__11)
        | (1ULL << XanatharParser::T__13)
        | (1ULL << XanatharParser::T__14)
        | (1ULL << XanatharParser::T__35)
        | (1ULL << XanatharParser::T__45)
        | (1ULL << XanatharParser::T__46)
        | (1ULL << XanatharParser::T__56)
        | (1ULL << XanatharParser::T__58)
        | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
        ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
        | (1ULL << (XanatharParser::T__72 - 72))
        | (1ULL << (XanatharParser::DIGIT - 72))
        | (1ULL << (XanatharParser::LCASELETT - 72))
        | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
        setState(750);
        expression(0);
      }
      setState(753);
      match(XanatharParser::T__5);
      break;
    }

    }
    _ctx->stop = _input->LT(-1);
    setState(865);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 76, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(863);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 75, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<AdditionContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(757);

          if (!(precpred(_ctx, 25))) throw FailedPredicateException(this, "precpred(_ctx, 25)");
          setState(758);
          match(XanatharParser::T__45);
          setState(759);
          expression(26);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<SubtractionContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(760);

          if (!(precpred(_ctx, 24))) throw FailedPredicateException(this, "precpred(_ctx, 24)");
          setState(761);
          match(XanatharParser::T__46);
          setState(762);
          expression(25);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<MultiplicationContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(763);

          if (!(precpred(_ctx, 23))) throw FailedPredicateException(this, "precpred(_ctx, 23)");
          setState(764);
          match(XanatharParser::T__13);
          setState(765);
          expression(24);
          break;
        }

        case 4: {
          auto newContext = _tracker.createInstance<DivisionContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(766);

          if (!(precpred(_ctx, 22))) throw FailedPredicateException(this, "precpred(_ctx, 22)");
          setState(767);
          match(XanatharParser::T__60);
          setState(768);
          expression(23);
          break;
        }

        case 5: {
          auto newContext = _tracker.createInstance<BinaryAndContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(769);

          if (!(precpred(_ctx, 21))) throw FailedPredicateException(this, "precpred(_ctx, 21)");
          setState(770);
          match(XanatharParser::T__14);
          setState(771);
          expression(22);
          break;
        }

        case 6: {
          auto newContext = _tracker.createInstance<BinaryOrContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(772);

          if (!(precpred(_ctx, 20))) throw FailedPredicateException(this, "precpred(_ctx, 20)");
          setState(773);
          match(XanatharParser::T__61);
          setState(774);
          expression(21);
          break;
        }

        case 7: {
          auto newContext = _tracker.createInstance<BinaryXorContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(775);

          if (!(precpred(_ctx, 19))) throw FailedPredicateException(this, "precpred(_ctx, 19)");
          setState(776);
          match(XanatharParser::T__62);
          setState(777);
          expression(20);
          break;
        }

        case 8: {
          auto newContext = _tracker.createInstance<ModuloContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(778);

          if (!(precpred(_ctx, 18))) throw FailedPredicateException(this, "precpred(_ctx, 18)");
          setState(779);
          match(XanatharParser::T__56);
          setState(780);
          expression(19);
          break;
        }

        case 9: {
          auto newContext = _tracker.createInstance<RangeContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(781);

          if (!(precpred(_ctx, 13))) throw FailedPredicateException(this, "precpred(_ctx, 13)");
          setState(782);
          match(XanatharParser::T__63);
          setState(783);
          expression(14);
          break;
        }

        case 10: {
          auto newContext = _tracker.createInstance<IsContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(784);

          if (!(precpred(_ctx, 12))) throw FailedPredicateException(this, "precpred(_ctx, 12)");
          setState(785);
          match(XanatharParser::T__64);
          setState(786);
          expression(13);
          break;
        }

        case 11: {
          auto newContext = _tracker.createInstance<RotateLeftContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(787);

          if (!(precpred(_ctx, 11))) throw FailedPredicateException(this, "precpred(_ctx, 11)");
          setState(788);
          match(XanatharParser::T__65);
          setState(789);
          expression(12);
          break;
        }

        case 12: {
          auto newContext = _tracker.createInstance<RotateRightContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(790);

          if (!(precpred(_ctx, 10))) throw FailedPredicateException(this, "precpred(_ctx, 10)");
          setState(791);
          match(XanatharParser::T__66);
          setState(792);
          expression(11);
          break;
        }

        case 13: {
          auto newContext = _tracker.createInstance<LessContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(793);

          if (!(precpred(_ctx, 9))) throw FailedPredicateException(this, "precpred(_ctx, 9)");
          setState(794);
          match(XanatharParser::T__18);
          setState(795);
          expression(10);
          break;
        }

        case 14: {
          auto newContext = _tracker.createInstance<GreaterContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(796);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(797);
          match(XanatharParser::T__19);
          setState(798);
          expression(9);
          break;
        }

        case 15: {
          auto newContext = _tracker.createInstance<LessOrEqualContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(799);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(800);
          match(XanatharParser::T__67);
          setState(801);
          expression(8);
          break;
        }

        case 16: {
          auto newContext = _tracker.createInstance<GreaterOrEqualContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(802);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(803);
          match(XanatharParser::T__68);
          setState(804);
          expression(7);
          break;
        }

        case 17: {
          auto newContext = _tracker.createInstance<NotEqualContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(805);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(806);
          match(XanatharParser::T__69);
          setState(807);
          expression(6);
          break;
        }

        case 18: {
          auto newContext = _tracker.createInstance<EqualContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(808);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(809);
          match(XanatharParser::T__70);
          setState(810);
          expression(5);
          break;
        }

        case 19: {
          auto newContext = _tracker.createInstance<IncrementContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(811);

          if (!(precpred(_ctx, 39))) throw FailedPredicateException(this, "precpred(_ctx, 39)");
          setState(812);
          match(XanatharParser::T__54);
          break;
        }

        case 20: {
          auto newContext = _tracker.createInstance<DecrementContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(813);

          if (!(precpred(_ctx, 38))) throw FailedPredicateException(this, "precpred(_ctx, 38)");
          setState(814);
          match(XanatharParser::T__55);
          break;
        }

        case 21: {
          auto newContext = _tracker.createInstance<ArrayIndexContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(815);

          if (!(precpred(_ctx, 32))) throw FailedPredicateException(this, "precpred(_ctx, 32)");
          setState(816);
          match(XanatharParser::T__8);
          setState(817);
          expression(0);
          setState(818);
          match(XanatharParser::T__9);
          break;
        }

        case 22: {
          auto newContext = _tracker.createInstance<FunctionCallContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(820);

          if (!(precpred(_ctx, 17))) throw FailedPredicateException(this, "precpred(_ctx, 17)");
          setState(821);
          match(XanatharParser::T__4);
          setState(827);
          _errHandler->sync(this);
          alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 70, _ctx);
          while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
            if (alt == 1) {
              setState(822);
              expression(0);
              setState(823);
              match(XanatharParser::T__28); 
            }
            setState(829);
            _errHandler->sync(this);
            alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 70, _ctx);
          }
          setState(831);
          _errHandler->sync(this);

          _la = _input->LA(1);
          if ((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << XanatharParser::T__1)
            | (1ULL << XanatharParser::T__3)
            | (1ULL << XanatharParser::T__4)
            | (1ULL << XanatharParser::T__8)
            | (1ULL << XanatharParser::T__10)
            | (1ULL << XanatharParser::T__11)
            | (1ULL << XanatharParser::T__13)
            | (1ULL << XanatharParser::T__14)
            | (1ULL << XanatharParser::T__35)
            | (1ULL << XanatharParser::T__45)
            | (1ULL << XanatharParser::T__46)
            | (1ULL << XanatharParser::T__56)
            | (1ULL << XanatharParser::T__58)
            | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
            ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
            | (1ULL << (XanatharParser::T__72 - 72))
            | (1ULL << (XanatharParser::DIGIT - 72))
            | (1ULL << (XanatharParser::LCASELETT - 72))
            | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
            setState(830);
            expression(0);
          }
          setState(833);
          match(XanatharParser::T__5);
          break;
        }

        case 23: {
          auto newContext = _tracker.createInstance<GenericCallContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(834);

          if (!(precpred(_ctx, 16))) throw FailedPredicateException(this, "precpred(_ctx, 16)");
          setState(835);
          match(XanatharParser::T__18);
          setState(836);
          gp();
          setState(837);
          match(XanatharParser::T__19);
          setState(838);
          match(XanatharParser::T__4);
          setState(844);
          _errHandler->sync(this);
          alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 72, _ctx);
          while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
            if (alt == 1) {
              setState(839);
              expression(0);
              setState(840);
              match(XanatharParser::T__28); 
            }
            setState(846);
            _errHandler->sync(this);
            alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 72, _ctx);
          }
          setState(848);
          _errHandler->sync(this);

          _la = _input->LA(1);
          if ((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << XanatharParser::T__1)
            | (1ULL << XanatharParser::T__3)
            | (1ULL << XanatharParser::T__4)
            | (1ULL << XanatharParser::T__8)
            | (1ULL << XanatharParser::T__10)
            | (1ULL << XanatharParser::T__11)
            | (1ULL << XanatharParser::T__13)
            | (1ULL << XanatharParser::T__14)
            | (1ULL << XanatharParser::T__35)
            | (1ULL << XanatharParser::T__45)
            | (1ULL << XanatharParser::T__46)
            | (1ULL << XanatharParser::T__56)
            | (1ULL << XanatharParser::T__58)
            | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
            ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
            | (1ULL << (XanatharParser::T__72 - 72))
            | (1ULL << (XanatharParser::DIGIT - 72))
            | (1ULL << (XanatharParser::LCASELETT - 72))
            | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
            setState(847);
            expression(0);
          }
          setState(850);
          match(XanatharParser::T__5);
          break;
        }

        case 24: {
          auto newContext = _tracker.createInstance<SpecContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(852);

          if (!(precpred(_ctx, 3))) throw FailedPredicateException(this, "precpred(_ctx, 3)");
          setState(853);
          match(XanatharParser::T__47);
          setState(859);
          _errHandler->sync(this);
          alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 74, _ctx);
          while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
            if (alt == 1) {
              setState(854);
              word();
              setState(855);
              match(XanatharParser::T__47); 
            }
            setState(861);
            _errHandler->sync(this);
            alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 74, _ctx);
          }
          setState(862);
          word();
          break;
        }

        } 
      }
      setState(867);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 76, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- PhraseContext ------------------------------------------------------------------

XanatharParser::PhraseContext::PhraseContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

XanatharParser::ExpressionContext* XanatharParser::PhraseContext::expression() {
  return getRuleContext<XanatharParser::ExpressionContext>(0);
}

XanatharParser::Reg_setContext* XanatharParser::PhraseContext::reg_set() {
  return getRuleContext<XanatharParser::Reg_setContext>(0);
}

XanatharParser::PrepContext* XanatharParser::PhraseContext::prep() {
  return getRuleContext<XanatharParser::PrepContext>(0);
}

XanatharParser::IncContext* XanatharParser::PhraseContext::inc() {
  return getRuleContext<XanatharParser::IncContext>(0);
}

XanatharParser::DecContext* XanatharParser::PhraseContext::dec() {
  return getRuleContext<XanatharParser::DecContext>(0);
}

XanatharParser::Asm_Context* XanatharParser::PhraseContext::asm_() {
  return getRuleContext<XanatharParser::Asm_Context>(0);
}

XanatharParser::Use_statementContext* XanatharParser::PhraseContext::use_statement() {
  return getRuleContext<XanatharParser::Use_statementContext>(0);
}

XanatharParser::Declare_statementContext* XanatharParser::PhraseContext::declare_statement() {
  return getRuleContext<XanatharParser::Declare_statementContext>(0);
}

XanatharParser::Template_Context* XanatharParser::PhraseContext::template_() {
  return getRuleContext<XanatharParser::Template_Context>(0);
}

XanatharParser::LambdaContext* XanatharParser::PhraseContext::lambda() {
  return getRuleContext<XanatharParser::LambdaContext>(0);
}

XanatharParser::RetContext* XanatharParser::PhraseContext::ret() {
  return getRuleContext<XanatharParser::RetContext>(0);
}

XanatharParser::LockContext* XanatharParser::PhraseContext::lock() {
  return getRuleContext<XanatharParser::LockContext>(0);
}

XanatharParser::UnlockContext* XanatharParser::PhraseContext::unlock() {
  return getRuleContext<XanatharParser::UnlockContext>(0);
}

XanatharParser::Variable_settingContext* XanatharParser::PhraseContext::variable_setting() {
  return getRuleContext<XanatharParser::Variable_settingContext>(0);
}

XanatharParser::LetContext* XanatharParser::PhraseContext::let() {
  return getRuleContext<XanatharParser::LetContext>(0);
}

XanatharParser::Break_Context* XanatharParser::PhraseContext::break_() {
  return getRuleContext<XanatharParser::Break_Context>(0);
}

XanatharParser::SkipContext* XanatharParser::PhraseContext::skip() {
  return getRuleContext<XanatharParser::SkipContext>(0);
}

XanatharParser::New_Context* XanatharParser::PhraseContext::new_() {
  return getRuleContext<XanatharParser::New_Context>(0);
}

XanatharParser::If_Context* XanatharParser::PhraseContext::if_() {
  return getRuleContext<XanatharParser::If_Context>(0);
}

XanatharParser::FnContext* XanatharParser::PhraseContext::fn() {
  return getRuleContext<XanatharParser::FnContext>(0);
}

XanatharParser::HookContext* XanatharParser::PhraseContext::hook() {
  return getRuleContext<XanatharParser::HookContext>(0);
}

XanatharParser::Virtual_fnContext* XanatharParser::PhraseContext::virtual_fn() {
  return getRuleContext<XanatharParser::Virtual_fnContext>(0);
}

XanatharParser::Match_Context* XanatharParser::PhraseContext::match_() {
  return getRuleContext<XanatharParser::Match_Context>(0);
}

XanatharParser::While_Context* XanatharParser::PhraseContext::while_() {
  return getRuleContext<XanatharParser::While_Context>(0);
}

XanatharParser::For_Context* XanatharParser::PhraseContext::for_() {
  return getRuleContext<XanatharParser::For_Context>(0);
}

XanatharParser::Class_Context* XanatharParser::PhraseContext::class_() {
  return getRuleContext<XanatharParser::Class_Context>(0);
}

XanatharParser::Interface_Context* XanatharParser::PhraseContext::interface_() {
  return getRuleContext<XanatharParser::Interface_Context>(0);
}

XanatharParser::OverrideContext* XanatharParser::PhraseContext::override() {
  return getRuleContext<XanatharParser::OverrideContext>(0);
}

XanatharParser::Extends_Context* XanatharParser::PhraseContext::extends_() {
  return getRuleContext<XanatharParser::Extends_Context>(0);
}


size_t XanatharParser::PhraseContext::getRuleIndex() const {
  return XanatharParser::RulePhrase;
}

void XanatharParser::PhraseContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPhrase(this);
}

void XanatharParser::PhraseContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPhrase(this);
}


antlrcpp::Any XanatharParser::PhraseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitPhrase(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::PhraseContext* XanatharParser::phrase() {
  PhraseContext *_localctx = _tracker.createInstance<PhraseContext>(_ctx, getState());
  enterRule(_localctx, 124, XanatharParser::RulePhrase);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(903);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case XanatharParser::T__0:
      case XanatharParser::T__1:
      case XanatharParser::T__3:
      case XanatharParser::T__4:
      case XanatharParser::T__6:
      case XanatharParser::T__7:
      case XanatharParser::T__8:
      case XanatharParser::T__10:
      case XanatharParser::T__11:
      case XanatharParser::T__13:
      case XanatharParser::T__14:
      case XanatharParser::T__16:
      case XanatharParser::T__30:
      case XanatharParser::T__35:
      case XanatharParser::T__41:
      case XanatharParser::T__45:
      case XanatharParser::T__46:
      case XanatharParser::T__48:
      case XanatharParser::T__49:
      case XanatharParser::T__50:
      case XanatharParser::T__52:
      case XanatharParser::T__53:
      case XanatharParser::T__56:
      case XanatharParser::T__58:
      case XanatharParser::T__59:
      case XanatharParser::T__71:
      case XanatharParser::T__72:
      case XanatharParser::T__73:
      case XanatharParser::DIGIT:
      case XanatharParser::LCASELETT:
      case XanatharParser::UCASELET: {
        enterOuterAlt(_localctx, 1);
        setState(886);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 77, _ctx)) {
        case 1: {
          setState(868);
          expression(0);
          break;
        }

        case 2: {
          setState(869);
          reg_set();
          break;
        }

        case 3: {
          setState(870);
          prep();
          break;
        }

        case 4: {
          setState(871);
          inc();
          break;
        }

        case 5: {
          setState(872);
          dec();
          break;
        }

        case 6: {
          setState(873);
          asm_();
          break;
        }

        case 7: {
          setState(874);
          use_statement();
          break;
        }

        case 8: {
          setState(875);
          declare_statement();
          break;
        }

        case 9: {
          setState(876);
          template_();
          break;
        }

        case 10: {
          setState(877);
          lambda();
          break;
        }

        case 11: {
          setState(878);
          ret();
          break;
        }

        case 12: {
          setState(879);
          lock();
          break;
        }

        case 13: {
          setState(880);
          unlock();
          break;
        }

        case 14: {
          setState(881);
          variable_setting();
          break;
        }

        case 15: {
          setState(882);
          let();
          break;
        }

        case 16: {
          setState(883);
          break_();
          break;
        }

        case 17: {
          setState(884);
          skip();
          break;
        }

        case 18: {
          setState(885);
          new_();
          break;
        }

        }
        setState(888);
        match(XanatharParser::T__39);
        break;
      }

      case XanatharParser::T__17:
      case XanatharParser::T__23:
      case XanatharParser::T__24:
      case XanatharParser::T__25:
      case XanatharParser::T__26:
      case XanatharParser::T__27:
      case XanatharParser::T__29:
      case XanatharParser::T__32:
      case XanatharParser::T__33:
      case XanatharParser::T__36:
      case XanatharParser::T__38:
      case XanatharParser::T__40:
      case XanatharParser::T__42: {
        enterOuterAlt(_localctx, 2);
        setState(901);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case XanatharParser::T__36: {
            setState(890);
            if_();
            break;
          }

          case XanatharParser::T__27: {
            setState(891);
            fn();
            break;
          }

          case XanatharParser::T__29: {
            setState(892);
            hook();
            break;
          }

          case XanatharParser::T__32: {
            setState(893);
            virtual_fn();
            break;
          }

          case XanatharParser::T__42: {
            setState(894);
            match_();
            break;
          }

          case XanatharParser::T__40: {
            setState(895);
            while_();
            break;
          }

          case XanatharParser::T__38: {
            setState(896);
            for_();
            break;
          }

          case XanatharParser::T__17:
          case XanatharParser::T__23:
          case XanatharParser::T__24: {
            setState(897);
            class_();
            break;
          }

          case XanatharParser::T__26: {
            setState(898);
            interface_();
            break;
          }

          case XanatharParser::T__33: {
            setState(899);
            override();
            break;
          }

          case XanatharParser::T__25: {
            setState(900);
            extends_();
            break;
          }

        default:
          throw NoViableAltException(this);
        }
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StartContext ------------------------------------------------------------------

XanatharParser::StartContext::StartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<XanatharParser::PhraseContext *> XanatharParser::StartContext::phrase() {
  return getRuleContexts<XanatharParser::PhraseContext>();
}

XanatharParser::PhraseContext* XanatharParser::StartContext::phrase(size_t i) {
  return getRuleContext<XanatharParser::PhraseContext>(i);
}


size_t XanatharParser::StartContext::getRuleIndex() const {
  return XanatharParser::RuleStart;
}

void XanatharParser::StartContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStart(this);
}

void XanatharParser::StartContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStart(this);
}


antlrcpp::Any XanatharParser::StartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitStart(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::StartContext* XanatharParser::start() {
  StartContext *_localctx = _tracker.createInstance<StartContext>(_ctx, getState());
  enterRule(_localctx, 126, XanatharParser::RuleStart);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(908);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << XanatharParser::T__0)
      | (1ULL << XanatharParser::T__1)
      | (1ULL << XanatharParser::T__3)
      | (1ULL << XanatharParser::T__4)
      | (1ULL << XanatharParser::T__6)
      | (1ULL << XanatharParser::T__7)
      | (1ULL << XanatharParser::T__8)
      | (1ULL << XanatharParser::T__10)
      | (1ULL << XanatharParser::T__11)
      | (1ULL << XanatharParser::T__13)
      | (1ULL << XanatharParser::T__14)
      | (1ULL << XanatharParser::T__16)
      | (1ULL << XanatharParser::T__17)
      | (1ULL << XanatharParser::T__23)
      | (1ULL << XanatharParser::T__24)
      | (1ULL << XanatharParser::T__25)
      | (1ULL << XanatharParser::T__26)
      | (1ULL << XanatharParser::T__27)
      | (1ULL << XanatharParser::T__29)
      | (1ULL << XanatharParser::T__30)
      | (1ULL << XanatharParser::T__32)
      | (1ULL << XanatharParser::T__33)
      | (1ULL << XanatharParser::T__35)
      | (1ULL << XanatharParser::T__36)
      | (1ULL << XanatharParser::T__38)
      | (1ULL << XanatharParser::T__40)
      | (1ULL << XanatharParser::T__41)
      | (1ULL << XanatharParser::T__42)
      | (1ULL << XanatharParser::T__45)
      | (1ULL << XanatharParser::T__46)
      | (1ULL << XanatharParser::T__48)
      | (1ULL << XanatharParser::T__49)
      | (1ULL << XanatharParser::T__50)
      | (1ULL << XanatharParser::T__52)
      | (1ULL << XanatharParser::T__53)
      | (1ULL << XanatharParser::T__56)
      | (1ULL << XanatharParser::T__58)
      | (1ULL << XanatharParser::T__59))) != 0) || ((((_la - 72) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 72)) & ((1ULL << (XanatharParser::T__71 - 72))
      | (1ULL << (XanatharParser::T__72 - 72))
      | (1ULL << (XanatharParser::T__73 - 72))
      | (1ULL << (XanatharParser::DIGIT - 72))
      | (1ULL << (XanatharParser::LCASELETT - 72))
      | (1ULL << (XanatharParser::UCASELET - 72)))) != 0)) {
      setState(905);
      phrase();
      setState(910);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Asm_Context ------------------------------------------------------------------

XanatharParser::Asm_Context::Asm_Context(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t XanatharParser::Asm_Context::getRuleIndex() const {
  return XanatharParser::RuleAsm_;
}

void XanatharParser::Asm_Context::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAsm_(this);
}

void XanatharParser::Asm_Context::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<XanatharListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAsm_(this);
}


antlrcpp::Any XanatharParser::Asm_Context::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<XanatharVisitor*>(visitor))
    return parserVisitor->visitAsm_(this);
  else
    return visitor->visitChildren(this);
}

XanatharParser::Asm_Context* XanatharParser::asm_() {
  Asm_Context *_localctx = _tracker.createInstance<Asm_Context>(_ctx, getState());
  enterRule(_localctx, 128, XanatharParser::RuleAsm_);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(911);
    match(XanatharParser::T__73);
    setState(913); 
    _errHandler->sync(this);
    alt = 1 + 1;
    do {
      switch (alt) {
        case 1 + 1: {
              setState(912);
              matchWildcard();
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(915); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 81, _ctx);
    } while (alt != 1 && alt != atn::ATN::INVALID_ALT_NUMBER);
    setState(917);
    match(XanatharParser::T__74);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool XanatharParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 61: return expressionSempred(dynamic_cast<ExpressionContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool XanatharParser::expressionSempred(ExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 25);
    case 1: return precpred(_ctx, 24);
    case 2: return precpred(_ctx, 23);
    case 3: return precpred(_ctx, 22);
    case 4: return precpred(_ctx, 21);
    case 5: return precpred(_ctx, 20);
    case 6: return precpred(_ctx, 19);
    case 7: return precpred(_ctx, 18);
    case 8: return precpred(_ctx, 13);
    case 9: return precpred(_ctx, 12);
    case 10: return precpred(_ctx, 11);
    case 11: return precpred(_ctx, 10);
    case 12: return precpred(_ctx, 9);
    case 13: return precpred(_ctx, 8);
    case 14: return precpred(_ctx, 7);
    case 15: return precpred(_ctx, 6);
    case 16: return precpred(_ctx, 5);
    case 17: return precpred(_ctx, 4);
    case 18: return precpred(_ctx, 39);
    case 19: return precpred(_ctx, 38);
    case 20: return precpred(_ctx, 32);
    case 21: return precpred(_ctx, 17);
    case 22: return precpred(_ctx, 16);
    case 23: return precpred(_ctx, 3);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> XanatharParser::_decisionToDFA;
atn::PredictionContextCache XanatharParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN XanatharParser::_atn;
std::vector<uint16_t> XanatharParser::_serializedATN;

std::vector<std::string> XanatharParser::_ruleNames = {
  "prep", "string_inner", "escaped_string", "letter", "cname", "word", "str", 
  "cast", "use_statement", "declare_statement", "variable_phrase", "raw_var", 
  "as_phrase", "typespec", "variable_setting", "let", "class_", "class__specs", 
  "extends_", "interface_", "interface_fn", "declared_var", "types", "inheritance", 
  "fncall", "generic_call", "gp", "fn", "hook", "template_", "class_es", 
  "class__", "virtual_fn", "override", "override_name", "override_param", 
  "lambda", "params", "if_", "else_", "for_", "while_", "ret", "match_", 
  "match_arm", "default_arm", "ref", "ptr", "deref", "number", "float_", 
  "break_", "skip", "new_", "call_sealed_class_", "lock", "unlock", "inc", 
  "dec", "reg", "reg_set", "expression", "phrase", "start", "asm_"
};

std::vector<std::string> XanatharParser::_literalNames = {
  "", "'!=!'", "'\"'", "'\\'", "'_'", "'['", "']'", "'use'", "'declare'", 
  "'('", "')'", "'$'", "'@'", "'as'", "'*'", "'&'", "'='", "'let'", "'class_'", 
  "'<'", "'>'", "':'", "'{'", "'}'", "'static'", "'sealed'", "'extends'", 
  "'interface'", "'fn'", "','", "'hook'", "'template'", "'class'", "'virtual'", 
  "'override'", "'->'", "'\u03BB'", "'if'", "'else'", "'for'", "';'", "'while'", 
  "'ret'", "'match'", "'=>'", "'default'", "'+'", "'-'", "'.'", "'break'", 
  "'skip'", "'new_'", "'::'", "'lock'", "'unlock'", "'++'", "'--'", "'%'", 
  "'<-'", "'sizeof'", "'typeof'", "'/'", "'|'", "'^'", "'..'", "'is'", "'<<'", 
  "'>>'", "'<='", "'>='", "'!='", "'=='", "'!'", "'new'", "'__asm__'", "'__end-asm__'"
};

std::vector<std::string> XanatharParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "WS", "COMMENT", "DIGIT", "LCASELETT", "UCASELET", "OP"
};

dfa::Vocabulary XanatharParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> XanatharParser::_tokenNames;

XanatharParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x53, 0x39a, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x4, 0x23, 0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x4, 0x25, 0x9, 
    0x25, 0x4, 0x26, 0x9, 0x26, 0x4, 0x27, 0x9, 0x27, 0x4, 0x28, 0x9, 0x28, 
    0x4, 0x29, 0x9, 0x29, 0x4, 0x2a, 0x9, 0x2a, 0x4, 0x2b, 0x9, 0x2b, 0x4, 
    0x2c, 0x9, 0x2c, 0x4, 0x2d, 0x9, 0x2d, 0x4, 0x2e, 0x9, 0x2e, 0x4, 0x2f, 
    0x9, 0x2f, 0x4, 0x30, 0x9, 0x30, 0x4, 0x31, 0x9, 0x31, 0x4, 0x32, 0x9, 
    0x32, 0x4, 0x33, 0x9, 0x33, 0x4, 0x34, 0x9, 0x34, 0x4, 0x35, 0x9, 0x35, 
    0x4, 0x36, 0x9, 0x36, 0x4, 0x37, 0x9, 0x37, 0x4, 0x38, 0x9, 0x38, 0x4, 
    0x39, 0x9, 0x39, 0x4, 0x3a, 0x9, 0x3a, 0x4, 0x3b, 0x9, 0x3b, 0x4, 0x3c, 
    0x9, 0x3c, 0x4, 0x3d, 0x9, 0x3d, 0x4, 0x3e, 0x9, 0x3e, 0x4, 0x3f, 0x9, 
    0x3f, 0x4, 0x40, 0x9, 0x40, 0x4, 0x41, 0x9, 0x41, 0x4, 0x42, 0x9, 0x42, 
    0x3, 0x2, 0x3, 0x2, 0x6, 0x2, 0x87, 0xa, 0x2, 0xd, 0x2, 0xe, 0x2, 0x88, 
    0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x5, 0x3, 0x8f, 0xa, 0x3, 0x3, 
    0x4, 0x3, 0x4, 0x7, 0x4, 0x93, 0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 0x96, 0xb, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 0x6, 0x5, 
    0x6, 0x9e, 0xa, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x7, 0x6, 0xa3, 0xa, 
    0x6, 0xc, 0x6, 0xe, 0x6, 0xa6, 0xb, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 
    0x3, 0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0xa, 
    0x3, 0xa, 0x3, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 
    0x3, 0xb, 0x3, 0xb, 0x5, 0xb, 0xbb, 0xa, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 
    0xc, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x5, 
    0xe, 0xc6, 0xa, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x7, 0xe, 
    0xcc, 0xa, 0xe, 0xc, 0xe, 0xe, 0xe, 0xcf, 0xb, 0xe, 0x3, 0xf, 0x6, 0xf, 
    0xd2, 0xa, 0xf, 0xd, 0xf, 0xe, 0xf, 0xd3, 0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 
    0x3, 0x10, 0x5, 0x10, 0xda, 0xa, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 
    0x3, 0x10, 0x5, 0x10, 0xe0, 0xa, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 
    0x5, 0x10, 0xe5, 0xa, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x5, 0x12, 0xf5, 0xa, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x7, 0x12, 0xf9, 0xa, 0x12, 0xc, 0x12, 0xe, 0x12, 0xfc, 
    0xb, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x13, 0x7, 0x13, 0x101, 0xa, 0x13, 
    0xc, 0x13, 0xe, 0x13, 0x104, 0xb, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 
    0x3, 0x14, 0x7, 0x14, 0x10a, 0xa, 0x14, 0xc, 0x14, 0xe, 0x14, 0x10d, 
    0xb, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 
    0x15, 0x7, 0x15, 0x115, 0xa, 0x15, 0xc, 0x15, 0xe, 0x15, 0x118, 0xb, 
    0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 
    0x3, 0x16, 0x3, 0x16, 0x7, 0x16, 0x122, 0xa, 0x16, 0xc, 0x16, 0xe, 0x16, 
    0x125, 0xb, 0x16, 0x3, 0x16, 0x5, 0x16, 0x128, 0xa, 0x16, 0x3, 0x16, 
    0x3, 0x16, 0x3, 0x16, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x18, 0x3, 
    0x18, 0x3, 0x18, 0x7, 0x18, 0x133, 0xa, 0x18, 0xc, 0x18, 0xe, 0x18, 
    0x136, 0xb, 0x18, 0x3, 0x18, 0x5, 0x18, 0x139, 0xa, 0x18, 0x3, 0x19, 
    0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x7, 0x19, 0x13f, 0xa, 0x19, 0xc, 0x19, 
    0xe, 0x19, 0x142, 0xb, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x5, 0x19, 
    0x147, 0xa, 0x19, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 
    0x7, 0x1a, 0x14e, 0xa, 0x1a, 0xc, 0x1a, 0xe, 0x1a, 0x151, 0xb, 0x1a, 
    0x3, 0x1a, 0x5, 0x1a, 0x154, 0xa, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1b, 
    0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 0x3, 
    0x1b, 0x7, 0x1b, 0x160, 0xa, 0x1b, 0xc, 0x1b, 0xe, 0x1b, 0x163, 0xb, 
    0x1b, 0x3, 0x1b, 0x5, 0x1b, 0x166, 0xa, 0x1b, 0x3, 0x1b, 0x3, 0x1b, 
    0x3, 0x1c, 0x3, 0x1c, 0x5, 0x1c, 0x16c, 0xa, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 
    0x7, 0x1c, 0x170, 0xa, 0x1c, 0xc, 0x1c, 0xe, 0x1c, 0x173, 0xb, 0x1c, 
    0x3, 0x1c, 0x3, 0x1c, 0x5, 0x1c, 0x177, 0xa, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 
    0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x7, 
    0x1d, 0x181, 0xa, 0x1d, 0xc, 0x1d, 0xe, 0x1d, 0x184, 0xb, 0x1d, 0x3, 
    0x1d, 0x3, 0x1d, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 
    0x3, 0x1e, 0x5, 0x1e, 0x18e, 0xa, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x7, 0x1e, 
    0x192, 0xa, 0x1e, 0xc, 0x1e, 0xe, 0x1e, 0x195, 0xb, 0x1e, 0x3, 0x1e, 
    0x3, 0x1e, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x3, 
    0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x7, 0x1f, 
    0x1a4, 0xa, 0x1f, 0xc, 0x1f, 0xe, 0x1f, 0x1a7, 0xb, 0x1f, 0x3, 0x1f, 
    0x3, 0x1f, 0x3, 0x20, 0x3, 0x20, 0x3, 0x20, 0x7, 0x20, 0x1ae, 0xa, 0x20, 
    0xc, 0x20, 0xe, 0x20, 0x1b1, 0xb, 0x20, 0x3, 0x20, 0x3, 0x20, 0x3, 0x21, 
    0x3, 0x21, 0x3, 0x21, 0x3, 0x22, 0x3, 0x22, 0x3, 0x22, 0x3, 0x22, 0x3, 
    0x22, 0x3, 0x22, 0x7, 0x22, 0x1be, 0xa, 0x22, 0xc, 0x22, 0xe, 0x22, 
    0x1c1, 0xb, 0x22, 0x3, 0x22, 0x5, 0x22, 0x1c4, 0xa, 0x22, 0x3, 0x22, 
    0x3, 0x22, 0x3, 0x22, 0x3, 0x22, 0x5, 0x22, 0x1ca, 0xa, 0x22, 0x3, 0x23, 
    0x3, 0x23, 0x3, 0x23, 0x3, 0x23, 0x3, 0x23, 0x3, 0x23, 0x3, 0x23, 0x3, 
    0x23, 0x7, 0x23, 0x1d4, 0xa, 0x23, 0xc, 0x23, 0xe, 0x23, 0x1d7, 0xb, 
    0x23, 0x3, 0x23, 0x3, 0x23, 0x3, 0x24, 0x3, 0x24, 0x3, 0x24, 0x6, 0x24, 
    0x1de, 0xa, 0x24, 0xd, 0x24, 0xe, 0x24, 0x1df, 0x3, 0x24, 0x3, 0x24, 
    0x3, 0x25, 0x3, 0x25, 0x3, 0x25, 0x7, 0x25, 0x1e7, 0xa, 0x25, 0xc, 0x25, 
    0xe, 0x25, 0x1ea, 0xb, 0x25, 0x3, 0x25, 0x5, 0x25, 0x1ed, 0xa, 0x25, 
    0x3, 0x26, 0x3, 0x26, 0x3, 0x26, 0x3, 0x26, 0x3, 0x26, 0x3, 0x26, 0x7, 
    0x26, 0x1f5, 0xa, 0x26, 0xc, 0x26, 0xe, 0x26, 0x1f8, 0xb, 0x26, 0x3, 
    0x26, 0x5, 0x26, 0x1fb, 0xa, 0x26, 0x3, 0x27, 0x3, 0x27, 0x3, 0x27, 
    0x3, 0x27, 0x7, 0x27, 0x201, 0xa, 0x27, 0xc, 0x27, 0xe, 0x27, 0x204, 
    0xb, 0x27, 0x3, 0x27, 0x3, 0x27, 0x3, 0x27, 0x3, 0x28, 0x3, 0x28, 0x3, 
    0x28, 0x3, 0x28, 0x3, 0x28, 0x3, 0x28, 0x7, 0x28, 0x20f, 0xa, 0x28, 
    0xc, 0x28, 0xe, 0x28, 0x212, 0xb, 0x28, 0x3, 0x28, 0x3, 0x28, 0x5, 0x28, 
    0x216, 0xa, 0x28, 0x3, 0x29, 0x3, 0x29, 0x3, 0x29, 0x7, 0x29, 0x21b, 
    0xa, 0x29, 0xc, 0x29, 0xe, 0x29, 0x21e, 0xb, 0x29, 0x3, 0x29, 0x3, 0x29, 
    0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 
    0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x7, 0x2a, 0x22b, 0xa, 0x2a, 0xc, 0x2a, 
    0xe, 0x2a, 0x22e, 0xb, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2b, 0x3, 0x2b, 
    0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 0x7, 0x2b, 0x238, 0xa, 0x2b, 
    0xc, 0x2b, 0xe, 0x2b, 0x23b, 0xb, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2c, 
    0x3, 0x2c, 0x5, 0x2c, 0x241, 0xa, 0x2c, 0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2d, 
    0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2d, 0x7, 0x2d, 0x24b, 
    0xa, 0x2d, 0xc, 0x2d, 0xe, 0x2d, 0x24e, 0xb, 0x2d, 0x3, 0x2d, 0x3, 0x2d, 
    0x5, 0x2d, 0x252, 0xa, 0x2d, 0x3, 0x2d, 0x5, 0x2d, 0x255, 0xa, 0x2d, 
    0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 
    0x2f, 0x3, 0x2f, 0x3, 0x2f, 0x3, 0x2f, 0x3, 0x30, 0x3, 0x30, 0x5, 0x30, 
    0x263, 0xa, 0x30, 0x3, 0x30, 0x3, 0x30, 0x3, 0x30, 0x5, 0x30, 0x268, 
    0xa, 0x30, 0x6, 0x30, 0x26a, 0xa, 0x30, 0xd, 0x30, 0xe, 0x30, 0x26b, 
    0x3, 0x31, 0x3, 0x31, 0x3, 0x31, 0x3, 0x32, 0x3, 0x32, 0x3, 0x32, 0x3, 
    0x33, 0x5, 0x33, 0x275, 0xa, 0x33, 0x3, 0x33, 0x6, 0x33, 0x278, 0xa, 
    0x33, 0xd, 0x33, 0xe, 0x33, 0x279, 0x3, 0x34, 0x3, 0x34, 0x3, 0x34, 
    0x3, 0x34, 0x3, 0x35, 0x3, 0x35, 0x3, 0x36, 0x3, 0x36, 0x3, 0x37, 0x3, 
    0x37, 0x3, 0x37, 0x3, 0x37, 0x3, 0x37, 0x3, 0x38, 0x3, 0x38, 0x3, 0x38, 
    0x6, 0x38, 0x28c, 0xa, 0x38, 0xd, 0x38, 0xe, 0x38, 0x28d, 0x3, 0x38, 
    0x3, 0x38, 0x3, 0x39, 0x3, 0x39, 0x3, 0x39, 0x3, 0x3a, 0x3, 0x3a, 0x3, 
    0x3a, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3c, 0x3, 0x3c, 0x3, 0x3c, 
    0x3, 0x3d, 0x3, 0x3d, 0x3, 0x3d, 0x3, 0x3e, 0x3, 0x3e, 0x3, 0x3e, 0x3, 
    0x3e, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x5, 0x3f, 0x2b0, 
    0xa, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x7, 
    0x3f, 0x2c4, 0xa, 0x3f, 0xc, 0x3f, 0xe, 0x3f, 0x2c7, 0xb, 0x3f, 0x3, 
    0x3f, 0x5, 0x3f, 0x2ca, 0xa, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x6, 0x3f, 0x2cf, 0xa, 0x3f, 0xd, 0x3f, 0xe, 0x3f, 0x2d0, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x7, 0x3f, 0x2d8, 0xa, 0x3f, 
    0xc, 0x3f, 0xe, 0x3f, 0x2db, 0xb, 0x3f, 0x3, 0x3f, 0x5, 0x3f, 0x2de, 
    0xa, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x7, 0x3f, 0x2ec, 0xa, 0x3f, 0xc, 0x3f, 0xe, 0x3f, 0x2ef, 0xb, 0x3f, 
    0x3, 0x3f, 0x5, 0x3f, 0x2f2, 0xa, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x5, 0x3f, 
    0x2f6, 0xa, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x7, 0x3f, 0x33c, 
    0xa, 0x3f, 0xc, 0x3f, 0xe, 0x3f, 0x33f, 0xb, 0x3f, 0x3, 0x3f, 0x5, 0x3f, 
    0x342, 0xa, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 
    0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x7, 0x3f, 0x34d, 0xa, 0x3f, 
    0xc, 0x3f, 0xe, 0x3f, 0x350, 0xb, 0x3f, 0x3, 0x3f, 0x5, 0x3f, 0x353, 
    0xa, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x7, 0x3f, 0x35c, 0xa, 0x3f, 0xc, 0x3f, 0xe, 0x3f, 
    0x35f, 0xb, 0x3f, 0x3, 0x3f, 0x7, 0x3f, 0x362, 0xa, 0x3f, 0xc, 0x3f, 
    0xe, 0x3f, 0x365, 0xb, 0x3f, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 
    0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 
    0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 
    0x3, 0x40, 0x5, 0x40, 0x379, 0xa, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 
    0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 
    0x40, 0x3, 0x40, 0x3, 0x40, 0x3, 0x40, 0x5, 0x40, 0x388, 0xa, 0x40, 
    0x5, 0x40, 0x38a, 0xa, 0x40, 0x3, 0x41, 0x7, 0x41, 0x38d, 0xa, 0x41, 
    0xc, 0x41, 0xe, 0x41, 0x390, 0xb, 0x41, 0x3, 0x42, 0x3, 0x42, 0x6, 0x42, 
    0x394, 0xa, 0x42, 0xd, 0x42, 0xe, 0x42, 0x395, 0x3, 0x42, 0x3, 0x42, 
    0x3, 0x42, 0x4, 0x88, 0x395, 0x3, 0x7c, 0x43, 0x2, 0x4, 0x6, 0x8, 0xa, 
    0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 
    0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 
    0x3c, 0x3e, 0x40, 0x42, 0x44, 0x46, 0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 
    0x54, 0x56, 0x58, 0x5a, 0x5c, 0x5e, 0x60, 0x62, 0x64, 0x66, 0x68, 0x6a, 
    0x6c, 0x6e, 0x70, 0x72, 0x74, 0x76, 0x78, 0x7a, 0x7c, 0x7e, 0x80, 0x82, 
    0x2, 0x7, 0x3, 0x2, 0x4, 0x4, 0x3, 0x2, 0x51, 0x52, 0x3, 0x2, 0x10, 
    0x11, 0x3, 0x2, 0x1a, 0x1b, 0x3, 0x2, 0x30, 0x31, 0x2, 0x3eb, 0x2, 0x84, 
    0x3, 0x2, 0x2, 0x2, 0x4, 0x8e, 0x3, 0x2, 0x2, 0x2, 0x6, 0x90, 0x3, 0x2, 
    0x2, 0x2, 0x8, 0x99, 0x3, 0x2, 0x2, 0x2, 0xa, 0x9d, 0x3, 0x2, 0x2, 0x2, 
    0xc, 0xa7, 0x3, 0x2, 0x2, 0x2, 0xe, 0xa9, 0x3, 0x2, 0x2, 0x2, 0x10, 
    0xab, 0x3, 0x2, 0x2, 0x2, 0x12, 0xb0, 0x3, 0x2, 0x2, 0x2, 0x14, 0xb3, 
    0x3, 0x2, 0x2, 0x2, 0x16, 0xbc, 0x3, 0x2, 0x2, 0x2, 0x18, 0xbf, 0x3, 
    0x2, 0x2, 0x2, 0x1a, 0xc2, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xd1, 0x3, 0x2, 
    0x2, 0x2, 0x1e, 0xd9, 0x3, 0x2, 0x2, 0x2, 0x20, 0xe6, 0x3, 0x2, 0x2, 
    0x2, 0x22, 0xeb, 0x3, 0x2, 0x2, 0x2, 0x24, 0x102, 0x3, 0x2, 0x2, 0x2, 
    0x26, 0x105, 0x3, 0x2, 0x2, 0x2, 0x28, 0x110, 0x3, 0x2, 0x2, 0x2, 0x2a, 
    0x11b, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x12c, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x138, 
    0x3, 0x2, 0x2, 0x2, 0x30, 0x146, 0x3, 0x2, 0x2, 0x2, 0x32, 0x148, 0x3, 
    0x2, 0x2, 0x2, 0x34, 0x157, 0x3, 0x2, 0x2, 0x2, 0x36, 0x171, 0x3, 0x2, 
    0x2, 0x2, 0x38, 0x178, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x187, 0x3, 0x2, 0x2, 
    0x2, 0x3c, 0x198, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x1af, 0x3, 0x2, 0x2, 0x2, 
    0x40, 0x1b4, 0x3, 0x2, 0x2, 0x2, 0x42, 0x1b7, 0x3, 0x2, 0x2, 0x2, 0x44, 
    0x1cb, 0x3, 0x2, 0x2, 0x2, 0x46, 0x1dd, 0x3, 0x2, 0x2, 0x2, 0x48, 0x1e8, 
    0x3, 0x2, 0x2, 0x2, 0x4a, 0x1ee, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x202, 0x3, 
    0x2, 0x2, 0x2, 0x4e, 0x208, 0x3, 0x2, 0x2, 0x2, 0x50, 0x217, 0x3, 0x2, 
    0x2, 0x2, 0x52, 0x221, 0x3, 0x2, 0x2, 0x2, 0x54, 0x231, 0x3, 0x2, 0x2, 
    0x2, 0x56, 0x23e, 0x3, 0x2, 0x2, 0x2, 0x58, 0x242, 0x3, 0x2, 0x2, 0x2, 
    0x5a, 0x258, 0x3, 0x2, 0x2, 0x2, 0x5c, 0x25c, 0x3, 0x2, 0x2, 0x2, 0x5e, 
    0x262, 0x3, 0x2, 0x2, 0x2, 0x60, 0x26d, 0x3, 0x2, 0x2, 0x2, 0x62, 0x270, 
    0x3, 0x2, 0x2, 0x2, 0x64, 0x274, 0x3, 0x2, 0x2, 0x2, 0x66, 0x27b, 0x3, 
    0x2, 0x2, 0x2, 0x68, 0x27f, 0x3, 0x2, 0x2, 0x2, 0x6a, 0x281, 0x3, 0x2, 
    0x2, 0x2, 0x6c, 0x283, 0x3, 0x2, 0x2, 0x2, 0x6e, 0x28b, 0x3, 0x2, 0x2, 
    0x2, 0x70, 0x291, 0x3, 0x2, 0x2, 0x2, 0x72, 0x294, 0x3, 0x2, 0x2, 0x2, 
    0x74, 0x297, 0x3, 0x2, 0x2, 0x2, 0x76, 0x29a, 0x3, 0x2, 0x2, 0x2, 0x78, 
    0x29d, 0x3, 0x2, 0x2, 0x2, 0x7a, 0x2a0, 0x3, 0x2, 0x2, 0x2, 0x7c, 0x2f5, 
    0x3, 0x2, 0x2, 0x2, 0x7e, 0x389, 0x3, 0x2, 0x2, 0x2, 0x80, 0x38e, 0x3, 
    0x2, 0x2, 0x2, 0x82, 0x391, 0x3, 0x2, 0x2, 0x2, 0x84, 0x86, 0x7, 0x3, 
    0x2, 0x2, 0x85, 0x87, 0xb, 0x2, 0x2, 0x2, 0x86, 0x85, 0x3, 0x2, 0x2, 
    0x2, 0x87, 0x88, 0x3, 0x2, 0x2, 0x2, 0x88, 0x89, 0x3, 0x2, 0x2, 0x2, 
    0x88, 0x86, 0x3, 0x2, 0x2, 0x2, 0x89, 0x8a, 0x3, 0x2, 0x2, 0x2, 0x8a, 
    0x8b, 0x7, 0x3, 0x2, 0x2, 0x8b, 0x3, 0x3, 0x2, 0x2, 0x2, 0x8c, 0x8f, 
    0xa, 0x2, 0x2, 0x2, 0x8d, 0x8f, 0x7, 0x5, 0x2, 0x2, 0x8e, 0x8c, 0x3, 
    0x2, 0x2, 0x2, 0x8e, 0x8d, 0x3, 0x2, 0x2, 0x2, 0x8f, 0x5, 0x3, 0x2, 
    0x2, 0x2, 0x90, 0x94, 0x7, 0x4, 0x2, 0x2, 0x91, 0x93, 0x5, 0x4, 0x3, 
    0x2, 0x92, 0x91, 0x3, 0x2, 0x2, 0x2, 0x93, 0x96, 0x3, 0x2, 0x2, 0x2, 
    0x94, 0x92, 0x3, 0x2, 0x2, 0x2, 0x94, 0x95, 0x3, 0x2, 0x2, 0x2, 0x95, 
    0x97, 0x3, 0x2, 0x2, 0x2, 0x96, 0x94, 0x3, 0x2, 0x2, 0x2, 0x97, 0x98, 
    0x7, 0x4, 0x2, 0x2, 0x98, 0x7, 0x3, 0x2, 0x2, 0x2, 0x99, 0x9a, 0x9, 
    0x3, 0x2, 0x2, 0x9a, 0x9, 0x3, 0x2, 0x2, 0x2, 0x9b, 0x9e, 0x7, 0x6, 
    0x2, 0x2, 0x9c, 0x9e, 0x5, 0x8, 0x5, 0x2, 0x9d, 0x9b, 0x3, 0x2, 0x2, 
    0x2, 0x9d, 0x9c, 0x3, 0x2, 0x2, 0x2, 0x9e, 0xa4, 0x3, 0x2, 0x2, 0x2, 
    0x9f, 0xa3, 0x7, 0x6, 0x2, 0x2, 0xa0, 0xa3, 0x5, 0x8, 0x5, 0x2, 0xa1, 
    0xa3, 0x7, 0x50, 0x2, 0x2, 0xa2, 0x9f, 0x3, 0x2, 0x2, 0x2, 0xa2, 0xa0, 
    0x3, 0x2, 0x2, 0x2, 0xa2, 0xa1, 0x3, 0x2, 0x2, 0x2, 0xa3, 0xa6, 0x3, 
    0x2, 0x2, 0x2, 0xa4, 0xa2, 0x3, 0x2, 0x2, 0x2, 0xa4, 0xa5, 0x3, 0x2, 
    0x2, 0x2, 0xa5, 0xb, 0x3, 0x2, 0x2, 0x2, 0xa6, 0xa4, 0x3, 0x2, 0x2, 
    0x2, 0xa7, 0xa8, 0x5, 0xa, 0x6, 0x2, 0xa8, 0xd, 0x3, 0x2, 0x2, 0x2, 
    0xa9, 0xaa, 0x5, 0x6, 0x4, 0x2, 0xaa, 0xf, 0x3, 0x2, 0x2, 0x2, 0xab, 
    0xac, 0x7, 0x7, 0x2, 0x2, 0xac, 0xad, 0x5, 0x7c, 0x3f, 0x2, 0xad, 0xae, 
    0x5, 0x1a, 0xe, 0x2, 0xae, 0xaf, 0x7, 0x8, 0x2, 0x2, 0xaf, 0x11, 0x3, 
    0x2, 0x2, 0x2, 0xb0, 0xb1, 0x7, 0x9, 0x2, 0x2, 0xb1, 0xb2, 0x5, 0xc, 
    0x7, 0x2, 0xb2, 0x13, 0x3, 0x2, 0x2, 0x2, 0xb3, 0xb4, 0x7, 0xa, 0x2, 
    0x2, 0xb4, 0xb5, 0x5, 0x16, 0xc, 0x2, 0xb5, 0xba, 0x5, 0x1a, 0xe, 0x2, 
    0xb6, 0xb7, 0x7, 0xb, 0x2, 0x2, 0xb7, 0xb8, 0x5, 0x7c, 0x3f, 0x2, 0xb8, 
    0xb9, 0x7, 0xc, 0x2, 0x2, 0xb9, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xba, 0xb6, 
    0x3, 0x2, 0x2, 0x2, 0xba, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xbb, 0x15, 0x3, 
    0x2, 0x2, 0x2, 0xbc, 0xbd, 0x7, 0xd, 0x2, 0x2, 0xbd, 0xbe, 0x5, 0xc, 
    0x7, 0x2, 0xbe, 0x17, 0x3, 0x2, 0x2, 0x2, 0xbf, 0xc0, 0x7, 0xe, 0x2, 
    0x2, 0xc0, 0xc1, 0x5, 0xc, 0x7, 0x2, 0xc1, 0x19, 0x3, 0x2, 0x2, 0x2, 
    0xc2, 0xc5, 0x7, 0xf, 0x2, 0x2, 0xc3, 0xc6, 0x5, 0x1c, 0xf, 0x2, 0xc4, 
    0xc6, 0x5, 0xc, 0x7, 0x2, 0xc5, 0xc3, 0x3, 0x2, 0x2, 0x2, 0xc5, 0xc4, 
    0x3, 0x2, 0x2, 0x2, 0xc6, 0xcd, 0x3, 0x2, 0x2, 0x2, 0xc7, 0xc8, 0x7, 
    0x7, 0x2, 0x2, 0xc8, 0xc9, 0x5, 0x64, 0x33, 0x2, 0xc9, 0xca, 0x7, 0x8, 
    0x2, 0x2, 0xca, 0xcc, 0x3, 0x2, 0x2, 0x2, 0xcb, 0xc7, 0x3, 0x2, 0x2, 
    0x2, 0xcc, 0xcf, 0x3, 0x2, 0x2, 0x2, 0xcd, 0xcb, 0x3, 0x2, 0x2, 0x2, 
    0xcd, 0xce, 0x3, 0x2, 0x2, 0x2, 0xce, 0x1b, 0x3, 0x2, 0x2, 0x2, 0xcf, 
    0xcd, 0x3, 0x2, 0x2, 0x2, 0xd0, 0xd2, 0x9, 0x4, 0x2, 0x2, 0xd1, 0xd0, 
    0x3, 0x2, 0x2, 0x2, 0xd2, 0xd3, 0x3, 0x2, 0x2, 0x2, 0xd3, 0xd1, 0x3, 
    0x2, 0x2, 0x2, 0xd3, 0xd4, 0x3, 0x2, 0x2, 0x2, 0xd4, 0xd5, 0x3, 0x2, 
    0x2, 0x2, 0xd5, 0xd6, 0x5, 0xc, 0x7, 0x2, 0xd6, 0x1d, 0x3, 0x2, 0x2, 
    0x2, 0xd7, 0xda, 0x5, 0x16, 0xc, 0x2, 0xd8, 0xda, 0x5, 0x5e, 0x30, 0x2, 
    0xd9, 0xd7, 0x3, 0x2, 0x2, 0x2, 0xd9, 0xd8, 0x3, 0x2, 0x2, 0x2, 0xda, 
    0xdf, 0x3, 0x2, 0x2, 0x2, 0xdb, 0xdc, 0x7, 0xb, 0x2, 0x2, 0xdc, 0xdd, 
    0x5, 0x7c, 0x3f, 0x2, 0xdd, 0xde, 0x7, 0xc, 0x2, 0x2, 0xde, 0xe0, 0x3, 
    0x2, 0x2, 0x2, 0xdf, 0xdb, 0x3, 0x2, 0x2, 0x2, 0xdf, 0xe0, 0x3, 0x2, 
    0x2, 0x2, 0xe0, 0xe1, 0x3, 0x2, 0x2, 0x2, 0xe1, 0xe4, 0x7, 0x12, 0x2, 
    0x2, 0xe2, 0xe5, 0x5, 0x7c, 0x3f, 0x2, 0xe3, 0xe5, 0x5, 0x32, 0x1a, 
    0x2, 0xe4, 0xe2, 0x3, 0x2, 0x2, 0x2, 0xe4, 0xe3, 0x3, 0x2, 0x2, 0x2, 
    0xe5, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xe6, 0xe7, 0x7, 0x13, 0x2, 0x2, 0xe7, 
    0xe8, 0x5, 0x16, 0xc, 0x2, 0xe8, 0xe9, 0x7, 0x12, 0x2, 0x2, 0xe9, 0xea, 
    0x5, 0x7c, 0x3f, 0x2, 0xea, 0x21, 0x3, 0x2, 0x2, 0x2, 0xeb, 0xec, 0x5, 
    0x24, 0x13, 0x2, 0xec, 0xed, 0x7, 0x14, 0x2, 0x2, 0xed, 0xee, 0x5, 0xc, 
    0x7, 0x2, 0xee, 0xef, 0x7, 0x15, 0x2, 0x2, 0xef, 0xf0, 0x5, 0x2e, 0x18, 
    0x2, 0xf0, 0xf1, 0x7, 0x16, 0x2, 0x2, 0xf1, 0xf4, 0x5, 0x30, 0x19, 0x2, 
    0xf2, 0xf3, 0x7, 0x17, 0x2, 0x2, 0xf3, 0xf5, 0x5, 0x30, 0x19, 0x2, 0xf4, 
    0xf2, 0x3, 0x2, 0x2, 0x2, 0xf4, 0xf5, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf6, 
    0x3, 0x2, 0x2, 0x2, 0xf6, 0xfa, 0x7, 0x18, 0x2, 0x2, 0xf7, 0xf9, 0x5, 
    0x7e, 0x40, 0x2, 0xf8, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xf9, 0xfc, 0x3, 0x2, 
    0x2, 0x2, 0xfa, 0xf8, 0x3, 0x2, 0x2, 0x2, 0xfa, 0xfb, 0x3, 0x2, 0x2, 
    0x2, 0xfb, 0xfd, 0x3, 0x2, 0x2, 0x2, 0xfc, 0xfa, 0x3, 0x2, 0x2, 0x2, 
    0xfd, 0xfe, 0x7, 0x19, 0x2, 0x2, 0xfe, 0x23, 0x3, 0x2, 0x2, 0x2, 0xff, 
    0x101, 0x9, 0x5, 0x2, 0x2, 0x100, 0xff, 0x3, 0x2, 0x2, 0x2, 0x101, 0x104, 
    0x3, 0x2, 0x2, 0x2, 0x102, 0x100, 0x3, 0x2, 0x2, 0x2, 0x102, 0x103, 
    0x3, 0x2, 0x2, 0x2, 0x103, 0x25, 0x3, 0x2, 0x2, 0x2, 0x104, 0x102, 0x3, 
    0x2, 0x2, 0x2, 0x105, 0x106, 0x7, 0x1c, 0x2, 0x2, 0x106, 0x107, 0x5, 
    0xc, 0x7, 0x2, 0x107, 0x10b, 0x7, 0x18, 0x2, 0x2, 0x108, 0x10a, 0x5, 
    0x7e, 0x40, 0x2, 0x109, 0x108, 0x3, 0x2, 0x2, 0x2, 0x10a, 0x10d, 0x3, 
    0x2, 0x2, 0x2, 0x10b, 0x109, 0x3, 0x2, 0x2, 0x2, 0x10b, 0x10c, 0x3, 
    0x2, 0x2, 0x2, 0x10c, 0x10e, 0x3, 0x2, 0x2, 0x2, 0x10d, 0x10b, 0x3, 
    0x2, 0x2, 0x2, 0x10e, 0x10f, 0x7, 0x19, 0x2, 0x2, 0x10f, 0x27, 0x3, 
    0x2, 0x2, 0x2, 0x110, 0x111, 0x7, 0x1d, 0x2, 0x2, 0x111, 0x112, 0x5, 
    0xc, 0x7, 0x2, 0x112, 0x116, 0x7, 0x18, 0x2, 0x2, 0x113, 0x115, 0x5, 
    0x2a, 0x16, 0x2, 0x114, 0x113, 0x3, 0x2, 0x2, 0x2, 0x115, 0x118, 0x3, 
    0x2, 0x2, 0x2, 0x116, 0x114, 0x3, 0x2, 0x2, 0x2, 0x116, 0x117, 0x3, 
    0x2, 0x2, 0x2, 0x117, 0x119, 0x3, 0x2, 0x2, 0x2, 0x118, 0x116, 0x3, 
    0x2, 0x2, 0x2, 0x119, 0x11a, 0x7, 0x19, 0x2, 0x2, 0x11a, 0x29, 0x3, 
    0x2, 0x2, 0x2, 0x11b, 0x11c, 0x7, 0x1e, 0x2, 0x2, 0x11c, 0x11d, 0x5, 
    0xc, 0x7, 0x2, 0x11d, 0x123, 0x7, 0x7, 0x2, 0x2, 0x11e, 0x11f, 0x5, 
    0x1c, 0xf, 0x2, 0x11f, 0x120, 0x7, 0x1f, 0x2, 0x2, 0x120, 0x122, 0x3, 
    0x2, 0x2, 0x2, 0x121, 0x11e, 0x3, 0x2, 0x2, 0x2, 0x122, 0x125, 0x3, 
    0x2, 0x2, 0x2, 0x123, 0x121, 0x3, 0x2, 0x2, 0x2, 0x123, 0x124, 0x3, 
    0x2, 0x2, 0x2, 0x124, 0x127, 0x3, 0x2, 0x2, 0x2, 0x125, 0x123, 0x3, 
    0x2, 0x2, 0x2, 0x126, 0x128, 0x5, 0x1c, 0xf, 0x2, 0x127, 0x126, 0x3, 
    0x2, 0x2, 0x2, 0x127, 0x128, 0x3, 0x2, 0x2, 0x2, 0x128, 0x129, 0x3, 
    0x2, 0x2, 0x2, 0x129, 0x12a, 0x7, 0x8, 0x2, 0x2, 0x12a, 0x12b, 0x5, 
    0x1a, 0xe, 0x2, 0x12b, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x12c, 0x12d, 0x5, 
    0x16, 0xc, 0x2, 0x12d, 0x12e, 0x5, 0x1a, 0xe, 0x2, 0x12e, 0x2d, 0x3, 
    0x2, 0x2, 0x2, 0x12f, 0x130, 0x5, 0x2c, 0x17, 0x2, 0x130, 0x131, 0x7, 
    0x1f, 0x2, 0x2, 0x131, 0x133, 0x3, 0x2, 0x2, 0x2, 0x132, 0x12f, 0x3, 
    0x2, 0x2, 0x2, 0x133, 0x136, 0x3, 0x2, 0x2, 0x2, 0x134, 0x132, 0x3, 
    0x2, 0x2, 0x2, 0x134, 0x135, 0x3, 0x2, 0x2, 0x2, 0x135, 0x137, 0x3, 
    0x2, 0x2, 0x2, 0x136, 0x134, 0x3, 0x2, 0x2, 0x2, 0x137, 0x139, 0x5, 
    0x2c, 0x17, 0x2, 0x138, 0x134, 0x3, 0x2, 0x2, 0x2, 0x138, 0x139, 0x3, 
    0x2, 0x2, 0x2, 0x139, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x13a, 0x140, 0x7, 0xb, 
    0x2, 0x2, 0x13b, 0x13c, 0x5, 0xc, 0x7, 0x2, 0x13c, 0x13d, 0x7, 0x1f, 
    0x2, 0x2, 0x13d, 0x13f, 0x3, 0x2, 0x2, 0x2, 0x13e, 0x13b, 0x3, 0x2, 
    0x2, 0x2, 0x13f, 0x142, 0x3, 0x2, 0x2, 0x2, 0x140, 0x13e, 0x3, 0x2, 
    0x2, 0x2, 0x140, 0x141, 0x3, 0x2, 0x2, 0x2, 0x141, 0x143, 0x3, 0x2, 
    0x2, 0x2, 0x142, 0x140, 0x3, 0x2, 0x2, 0x2, 0x143, 0x144, 0x5, 0xc, 
    0x7, 0x2, 0x144, 0x145, 0x7, 0xc, 0x2, 0x2, 0x145, 0x147, 0x3, 0x2, 
    0x2, 0x2, 0x146, 0x13a, 0x3, 0x2, 0x2, 0x2, 0x146, 0x147, 0x3, 0x2, 
    0x2, 0x2, 0x147, 0x31, 0x3, 0x2, 0x2, 0x2, 0x148, 0x149, 0x5, 0x7c, 
    0x3f, 0x2, 0x149, 0x14f, 0x7, 0x7, 0x2, 0x2, 0x14a, 0x14b, 0x5, 0x7c, 
    0x3f, 0x2, 0x14b, 0x14c, 0x7, 0x1f, 0x2, 0x2, 0x14c, 0x14e, 0x3, 0x2, 
    0x2, 0x2, 0x14d, 0x14a, 0x3, 0x2, 0x2, 0x2, 0x14e, 0x151, 0x3, 0x2, 
    0x2, 0x2, 0x14f, 0x14d, 0x3, 0x2, 0x2, 0x2, 0x14f, 0x150, 0x3, 0x2, 
    0x2, 0x2, 0x150, 0x153, 0x3, 0x2, 0x2, 0x2, 0x151, 0x14f, 0x3, 0x2, 
    0x2, 0x2, 0x152, 0x154, 0x5, 0x7c, 0x3f, 0x2, 0x153, 0x152, 0x3, 0x2, 
    0x2, 0x2, 0x153, 0x154, 0x3, 0x2, 0x2, 0x2, 0x154, 0x155, 0x3, 0x2, 
    0x2, 0x2, 0x155, 0x156, 0x7, 0x8, 0x2, 0x2, 0x156, 0x33, 0x3, 0x2, 0x2, 
    0x2, 0x157, 0x158, 0x5, 0x7c, 0x3f, 0x2, 0x158, 0x159, 0x7, 0x15, 0x2, 
    0x2, 0x159, 0x15a, 0x5, 0x36, 0x1c, 0x2, 0x15a, 0x15b, 0x7, 0x16, 0x2, 
    0x2, 0x15b, 0x161, 0x7, 0x7, 0x2, 0x2, 0x15c, 0x15d, 0x5, 0x7c, 0x3f, 
    0x2, 0x15d, 0x15e, 0x7, 0x1f, 0x2, 0x2, 0x15e, 0x160, 0x3, 0x2, 0x2, 
    0x2, 0x15f, 0x15c, 0x3, 0x2, 0x2, 0x2, 0x160, 0x163, 0x3, 0x2, 0x2, 
    0x2, 0x161, 0x15f, 0x3, 0x2, 0x2, 0x2, 0x161, 0x162, 0x3, 0x2, 0x2, 
    0x2, 0x162, 0x165, 0x3, 0x2, 0x2, 0x2, 0x163, 0x161, 0x3, 0x2, 0x2, 
    0x2, 0x164, 0x166, 0x5, 0x7c, 0x3f, 0x2, 0x165, 0x164, 0x3, 0x2, 0x2, 
    0x2, 0x165, 0x166, 0x3, 0x2, 0x2, 0x2, 0x166, 0x167, 0x3, 0x2, 0x2, 
    0x2, 0x167, 0x168, 0x7, 0x8, 0x2, 0x2, 0x168, 0x35, 0x3, 0x2, 0x2, 0x2, 
    0x169, 0x16c, 0x5, 0x1c, 0xf, 0x2, 0x16a, 0x16c, 0x5, 0xc, 0x7, 0x2, 
    0x16b, 0x169, 0x3, 0x2, 0x2, 0x2, 0x16b, 0x16a, 0x3, 0x2, 0x2, 0x2, 
    0x16c, 0x16d, 0x3, 0x2, 0x2, 0x2, 0x16d, 0x16e, 0x7, 0x1f, 0x2, 0x2, 
    0x16e, 0x170, 0x3, 0x2, 0x2, 0x2, 0x16f, 0x16b, 0x3, 0x2, 0x2, 0x2, 
    0x170, 0x173, 0x3, 0x2, 0x2, 0x2, 0x171, 0x16f, 0x3, 0x2, 0x2, 0x2, 
    0x171, 0x172, 0x3, 0x2, 0x2, 0x2, 0x172, 0x176, 0x3, 0x2, 0x2, 0x2, 
    0x173, 0x171, 0x3, 0x2, 0x2, 0x2, 0x174, 0x177, 0x5, 0x1c, 0xf, 0x2, 
    0x175, 0x177, 0x5, 0xc, 0x7, 0x2, 0x176, 0x174, 0x3, 0x2, 0x2, 0x2, 
    0x176, 0x175, 0x3, 0x2, 0x2, 0x2, 0x177, 0x37, 0x3, 0x2, 0x2, 0x2, 0x178, 
    0x179, 0x7, 0x1e, 0x2, 0x2, 0x179, 0x17a, 0x5, 0xc, 0x7, 0x2, 0x17a, 
    0x17b, 0x7, 0x7, 0x2, 0x2, 0x17b, 0x17c, 0x5, 0x4c, 0x27, 0x2, 0x17c, 
    0x17d, 0x7, 0x8, 0x2, 0x2, 0x17d, 0x17e, 0x5, 0x1a, 0xe, 0x2, 0x17e, 
    0x182, 0x7, 0x18, 0x2, 0x2, 0x17f, 0x181, 0x5, 0x7e, 0x40, 0x2, 0x180, 
    0x17f, 0x3, 0x2, 0x2, 0x2, 0x181, 0x184, 0x3, 0x2, 0x2, 0x2, 0x182, 
    0x180, 0x3, 0x2, 0x2, 0x2, 0x182, 0x183, 0x3, 0x2, 0x2, 0x2, 0x183, 
    0x185, 0x3, 0x2, 0x2, 0x2, 0x184, 0x182, 0x3, 0x2, 0x2, 0x2, 0x185, 
    0x186, 0x7, 0x19, 0x2, 0x2, 0x186, 0x39, 0x3, 0x2, 0x2, 0x2, 0x187, 
    0x188, 0x7, 0x20, 0x2, 0x2, 0x188, 0x189, 0x7, 0x53, 0x2, 0x2, 0x189, 
    0x18a, 0x7, 0x7, 0x2, 0x2, 0x18a, 0x18b, 0x5, 0x4c, 0x27, 0x2, 0x18b, 
    0x18d, 0x7, 0x8, 0x2, 0x2, 0x18c, 0x18e, 0x5, 0x1a, 0xe, 0x2, 0x18d, 
    0x18c, 0x3, 0x2, 0x2, 0x2, 0x18d, 0x18e, 0x3, 0x2, 0x2, 0x2, 0x18e, 
    0x18f, 0x3, 0x2, 0x2, 0x2, 0x18f, 0x193, 0x7, 0x18, 0x2, 0x2, 0x190, 
    0x192, 0x5, 0x7e, 0x40, 0x2, 0x191, 0x190, 0x3, 0x2, 0x2, 0x2, 0x192, 
    0x195, 0x3, 0x2, 0x2, 0x2, 0x193, 0x191, 0x3, 0x2, 0x2, 0x2, 0x193, 
    0x194, 0x3, 0x2, 0x2, 0x2, 0x194, 0x196, 0x3, 0x2, 0x2, 0x2, 0x195, 
    0x193, 0x3, 0x2, 0x2, 0x2, 0x196, 0x197, 0x7, 0x19, 0x2, 0x2, 0x197, 
    0x3b, 0x3, 0x2, 0x2, 0x2, 0x198, 0x199, 0x7, 0x21, 0x2, 0x2, 0x199, 
    0x19a, 0x7, 0x15, 0x2, 0x2, 0x19a, 0x19b, 0x5, 0x3e, 0x20, 0x2, 0x19b, 
    0x19c, 0x7, 0x16, 0x2, 0x2, 0x19c, 0x19d, 0x5, 0xc, 0x7, 0x2, 0x19d, 
    0x19e, 0x7, 0x7, 0x2, 0x2, 0x19e, 0x19f, 0x5, 0x4c, 0x27, 0x2, 0x19f, 
    0x1a0, 0x7, 0x8, 0x2, 0x2, 0x1a0, 0x1a1, 0x5, 0x1a, 0xe, 0x2, 0x1a1, 
    0x1a5, 0x7, 0x18, 0x2, 0x2, 0x1a2, 0x1a4, 0x5, 0x7e, 0x40, 0x2, 0x1a3, 
    0x1a2, 0x3, 0x2, 0x2, 0x2, 0x1a4, 0x1a7, 0x3, 0x2, 0x2, 0x2, 0x1a5, 
    0x1a3, 0x3, 0x2, 0x2, 0x2, 0x1a5, 0x1a6, 0x3, 0x2, 0x2, 0x2, 0x1a6, 
    0x1a8, 0x3, 0x2, 0x2, 0x2, 0x1a7, 0x1a5, 0x3, 0x2, 0x2, 0x2, 0x1a8, 
    0x1a9, 0x7, 0x19, 0x2, 0x2, 0x1a9, 0x3d, 0x3, 0x2, 0x2, 0x2, 0x1aa, 
    0x1ab, 0x5, 0x40, 0x21, 0x2, 0x1ab, 0x1ac, 0x7, 0x1f, 0x2, 0x2, 0x1ac, 
    0x1ae, 0x3, 0x2, 0x2, 0x2, 0x1ad, 0x1aa, 0x3, 0x2, 0x2, 0x2, 0x1ae, 
    0x1b1, 0x3, 0x2, 0x2, 0x2, 0x1af, 0x1ad, 0x3, 0x2, 0x2, 0x2, 0x1af, 
    0x1b0, 0x3, 0x2, 0x2, 0x2, 0x1b0, 0x1b2, 0x3, 0x2, 0x2, 0x2, 0x1b1, 
    0x1af, 0x3, 0x2, 0x2, 0x2, 0x1b2, 0x1b3, 0x5, 0x40, 0x21, 0x2, 0x1b3, 
    0x3f, 0x3, 0x2, 0x2, 0x2, 0x1b4, 0x1b5, 0x7, 0x22, 0x2, 0x2, 0x1b5, 
    0x1b6, 0x5, 0xc, 0x7, 0x2, 0x1b6, 0x41, 0x3, 0x2, 0x2, 0x2, 0x1b7, 0x1b8, 
    0x7, 0x23, 0x2, 0x2, 0x1b8, 0x1b9, 0x5, 0xc, 0x7, 0x2, 0x1b9, 0x1bf, 
    0x7, 0x7, 0x2, 0x2, 0x1ba, 0x1bb, 0x5, 0x1c, 0xf, 0x2, 0x1bb, 0x1bc, 
    0x7, 0x1f, 0x2, 0x2, 0x1bc, 0x1be, 0x3, 0x2, 0x2, 0x2, 0x1bd, 0x1ba, 
    0x3, 0x2, 0x2, 0x2, 0x1be, 0x1c1, 0x3, 0x2, 0x2, 0x2, 0x1bf, 0x1bd, 
    0x3, 0x2, 0x2, 0x2, 0x1bf, 0x1c0, 0x3, 0x2, 0x2, 0x2, 0x1c0, 0x1c3, 
    0x3, 0x2, 0x2, 0x2, 0x1c1, 0x1bf, 0x3, 0x2, 0x2, 0x2, 0x1c2, 0x1c4, 
    0x5, 0x1c, 0xf, 0x2, 0x1c3, 0x1c2, 0x3, 0x2, 0x2, 0x2, 0x1c3, 0x1c4, 
    0x3, 0x2, 0x2, 0x2, 0x1c4, 0x1c5, 0x3, 0x2, 0x2, 0x2, 0x1c5, 0x1c6, 
    0x7, 0x8, 0x2, 0x2, 0x1c6, 0x1c9, 0x7, 0xf, 0x2, 0x2, 0x1c7, 0x1ca, 
    0x5, 0x1c, 0xf, 0x2, 0x1c8, 0x1ca, 0x5, 0xc, 0x7, 0x2, 0x1c9, 0x1c7, 
    0x3, 0x2, 0x2, 0x2, 0x1c9, 0x1c8, 0x3, 0x2, 0x2, 0x2, 0x1ca, 0x43, 0x3, 
    0x2, 0x2, 0x2, 0x1cb, 0x1cc, 0x7, 0x24, 0x2, 0x2, 0x1cc, 0x1cd, 0x7, 
    0xe, 0x2, 0x2, 0x1cd, 0x1ce, 0x5, 0x46, 0x24, 0x2, 0x1ce, 0x1cf, 0x7, 
    0x15, 0x2, 0x2, 0x1cf, 0x1d0, 0x5, 0x48, 0x25, 0x2, 0x1d0, 0x1d1, 0x7, 
    0x16, 0x2, 0x2, 0x1d1, 0x1d5, 0x7, 0x18, 0x2, 0x2, 0x1d2, 0x1d4, 0x5, 
    0x7e, 0x40, 0x2, 0x1d3, 0x1d2, 0x3, 0x2, 0x2, 0x2, 0x1d4, 0x1d7, 0x3, 
    0x2, 0x2, 0x2, 0x1d5, 0x1d3, 0x3, 0x2, 0x2, 0x2, 0x1d5, 0x1d6, 0x3, 
    0x2, 0x2, 0x2, 0x1d6, 0x1d8, 0x3, 0x2, 0x2, 0x2, 0x1d7, 0x1d5, 0x3, 
    0x2, 0x2, 0x2, 0x1d8, 0x1d9, 0x7, 0x19, 0x2, 0x2, 0x1d9, 0x45, 0x3, 
    0x2, 0x2, 0x2, 0x1da, 0x1db, 0x5, 0xc, 0x7, 0x2, 0x1db, 0x1dc, 0x7, 
    0x25, 0x2, 0x2, 0x1dc, 0x1de, 0x3, 0x2, 0x2, 0x2, 0x1dd, 0x1da, 0x3, 
    0x2, 0x2, 0x2, 0x1de, 0x1df, 0x3, 0x2, 0x2, 0x2, 0x1df, 0x1dd, 0x3, 
    0x2, 0x2, 0x2, 0x1df, 0x1e0, 0x3, 0x2, 0x2, 0x2, 0x1e0, 0x1e1, 0x3, 
    0x2, 0x2, 0x2, 0x1e1, 0x1e2, 0x5, 0xc, 0x7, 0x2, 0x1e2, 0x47, 0x3, 0x2, 
    0x2, 0x2, 0x1e3, 0x1e4, 0x5, 0x16, 0xc, 0x2, 0x1e4, 0x1e5, 0x7, 0x1f, 
    0x2, 0x2, 0x1e5, 0x1e7, 0x3, 0x2, 0x2, 0x2, 0x1e6, 0x1e3, 0x3, 0x2, 
    0x2, 0x2, 0x1e7, 0x1ea, 0x3, 0x2, 0x2, 0x2, 0x1e8, 0x1e6, 0x3, 0x2, 
    0x2, 0x2, 0x1e8, 0x1e9, 0x3, 0x2, 0x2, 0x2, 0x1e9, 0x1ec, 0x3, 0x2, 
    0x2, 0x2, 0x1ea, 0x1e8, 0x3, 0x2, 0x2, 0x2, 0x1eb, 0x1ed, 0x5, 0x16, 
    0xc, 0x2, 0x1ec, 0x1eb, 0x3, 0x2, 0x2, 0x2, 0x1ec, 0x1ed, 0x3, 0x2, 
    0x2, 0x2, 0x1ed, 0x49, 0x3, 0x2, 0x2, 0x2, 0x1ee, 0x1ef, 0x7, 0x26, 
    0x2, 0x2, 0x1ef, 0x1f0, 0x5, 0x4c, 0x27, 0x2, 0x1f0, 0x1fa, 0x7, 0x25, 
    0x2, 0x2, 0x1f1, 0x1fb, 0x5, 0x7c, 0x3f, 0x2, 0x1f2, 0x1f6, 0x7, 0x18, 
    0x2, 0x2, 0x1f3, 0x1f5, 0x5, 0x7e, 0x40, 0x2, 0x1f4, 0x1f3, 0x3, 0x2, 
    0x2, 0x2, 0x1f5, 0x1f8, 0x3, 0x2, 0x2, 0x2, 0x1f6, 0x1f4, 0x3, 0x2, 
    0x2, 0x2, 0x1f6, 0x1f7, 0x3, 0x2, 0x2, 0x2, 0x1f7, 0x1f9, 0x3, 0x2, 
    0x2, 0x2, 0x1f8, 0x1f6, 0x3, 0x2, 0x2, 0x2, 0x1f9, 0x1fb, 0x7, 0x19, 
    0x2, 0x2, 0x1fa, 0x1f1, 0x3, 0x2, 0x2, 0x2, 0x1fa, 0x1f2, 0x3, 0x2, 
    0x2, 0x2, 0x1fb, 0x4b, 0x3, 0x2, 0x2, 0x2, 0x1fc, 0x1fd, 0x5, 0x16, 
    0xc, 0x2, 0x1fd, 0x1fe, 0x5, 0x1a, 0xe, 0x2, 0x1fe, 0x1ff, 0x7, 0x1f, 
    0x2, 0x2, 0x1ff, 0x201, 0x3, 0x2, 0x2, 0x2, 0x200, 0x1fc, 0x3, 0x2, 
    0x2, 0x2, 0x201, 0x204, 0x3, 0x2, 0x2, 0x2, 0x202, 0x200, 0x3, 0x2, 
    0x2, 0x2, 0x202, 0x203, 0x3, 0x2, 0x2, 0x2, 0x203, 0x205, 0x3, 0x2, 
    0x2, 0x2, 0x204, 0x202, 0x3, 0x2, 0x2, 0x2, 0x205, 0x206, 0x5, 0x16, 
    0xc, 0x2, 0x206, 0x207, 0x5, 0x1a, 0xe, 0x2, 0x207, 0x4d, 0x3, 0x2, 
    0x2, 0x2, 0x208, 0x209, 0x7, 0x27, 0x2, 0x2, 0x209, 0x20a, 0x7, 0x7, 
    0x2, 0x2, 0x20a, 0x20b, 0x5, 0x7c, 0x3f, 0x2, 0x20b, 0x20c, 0x7, 0x8, 
    0x2, 0x2, 0x20c, 0x210, 0x7, 0x18, 0x2, 0x2, 0x20d, 0x20f, 0x5, 0x7e, 
    0x40, 0x2, 0x20e, 0x20d, 0x3, 0x2, 0x2, 0x2, 0x20f, 0x212, 0x3, 0x2, 
    0x2, 0x2, 0x210, 0x20e, 0x3, 0x2, 0x2, 0x2, 0x210, 0x211, 0x3, 0x2, 
    0x2, 0x2, 0x211, 0x213, 0x3, 0x2, 0x2, 0x2, 0x212, 0x210, 0x3, 0x2, 
    0x2, 0x2, 0x213, 0x215, 0x7, 0x19, 0x2, 0x2, 0x214, 0x216, 0x5, 0x50, 
    0x29, 0x2, 0x215, 0x214, 0x3, 0x2, 0x2, 0x2, 0x215, 0x216, 0x3, 0x2, 
    0x2, 0x2, 0x216, 0x4f, 0x3, 0x2, 0x2, 0x2, 0x217, 0x218, 0x7, 0x28, 
    0x2, 0x2, 0x218, 0x21c, 0x7, 0x18, 0x2, 0x2, 0x219, 0x21b, 0x5, 0x7e, 
    0x40, 0x2, 0x21a, 0x219, 0x3, 0x2, 0x2, 0x2, 0x21b, 0x21e, 0x3, 0x2, 
    0x2, 0x2, 0x21c, 0x21a, 0x3, 0x2, 0x2, 0x2, 0x21c, 0x21d, 0x3, 0x2, 
    0x2, 0x2, 0x21d, 0x21f, 0x3, 0x2, 0x2, 0x2, 0x21e, 0x21c, 0x3, 0x2, 
    0x2, 0x2, 0x21f, 0x220, 0x7, 0x19, 0x2, 0x2, 0x220, 0x51, 0x3, 0x2, 
    0x2, 0x2, 0x221, 0x222, 0x7, 0x29, 0x2, 0x2, 0x222, 0x223, 0x7, 0x7, 
    0x2, 0x2, 0x223, 0x224, 0x5, 0x7e, 0x40, 0x2, 0x224, 0x225, 0x5, 0x7c, 
    0x3f, 0x2, 0x225, 0x226, 0x7, 0x2a, 0x2, 0x2, 0x226, 0x227, 0x5, 0x7e, 
    0x40, 0x2, 0x227, 0x228, 0x7, 0x8, 0x2, 0x2, 0x228, 0x22c, 0x7, 0x18, 
    0x2, 0x2, 0x229, 0x22b, 0x5, 0x7e, 0x40, 0x2, 0x22a, 0x229, 0x3, 0x2, 
    0x2, 0x2, 0x22b, 0x22e, 0x3, 0x2, 0x2, 0x2, 0x22c, 0x22a, 0x3, 0x2, 
    0x2, 0x2, 0x22c, 0x22d, 0x3, 0x2, 0x2, 0x2, 0x22d, 0x22f, 0x3, 0x2, 
    0x2, 0x2, 0x22e, 0x22c, 0x3, 0x2, 0x2, 0x2, 0x22f, 0x230, 0x7, 0x19, 
    0x2, 0x2, 0x230, 0x53, 0x3, 0x2, 0x2, 0x2, 0x231, 0x232, 0x7, 0x2b, 
    0x2, 0x2, 0x232, 0x233, 0x7, 0x7, 0x2, 0x2, 0x233, 0x234, 0x5, 0x7c, 
    0x3f, 0x2, 0x234, 0x235, 0x7, 0x8, 0x2, 0x2, 0x235, 0x239, 0x7, 0x18, 
    0x2, 0x2, 0x236, 0x238, 0x5, 0x7e, 0x40, 0x2, 0x237, 0x236, 0x3, 0x2, 
    0x2, 0x2, 0x238, 0x23b, 0x3, 0x2, 0x2, 0x2, 0x239, 0x237, 0x3, 0x2, 
    0x2, 0x2, 0x239, 0x23a, 0x3, 0x2, 0x2, 0x2, 0x23a, 0x23c, 0x3, 0x2, 
    0x2, 0x2, 0x23b, 0x239, 0x3, 0x2, 0x2, 0x2, 0x23c, 0x23d, 0x7, 0x19, 
    0x2, 0x2, 0x23d, 0x55, 0x3, 0x2, 0x2, 0x2, 0x23e, 0x240, 0x7, 0x2c, 
    0x2, 0x2, 0x23f, 0x241, 0x5, 0x7c, 0x3f, 0x2, 0x240, 0x23f, 0x3, 0x2, 
    0x2, 0x2, 0x240, 0x241, 0x3, 0x2, 0x2, 0x2, 0x241, 0x57, 0x3, 0x2, 0x2, 
    0x2, 0x242, 0x243, 0x7, 0x2d, 0x2, 0x2, 0x243, 0x244, 0x7, 0x7, 0x2, 
    0x2, 0x244, 0x245, 0x5, 0x7c, 0x3f, 0x2, 0x245, 0x246, 0x7, 0x8, 0x2, 
    0x2, 0x246, 0x24c, 0x7, 0x18, 0x2, 0x2, 0x247, 0x248, 0x5, 0x5a, 0x2e, 
    0x2, 0x248, 0x249, 0x7, 0x1f, 0x2, 0x2, 0x249, 0x24b, 0x3, 0x2, 0x2, 
    0x2, 0x24a, 0x247, 0x3, 0x2, 0x2, 0x2, 0x24b, 0x24e, 0x3, 0x2, 0x2, 
    0x2, 0x24c, 0x24a, 0x3, 0x2, 0x2, 0x2, 0x24c, 0x24d, 0x3, 0x2, 0x2, 
    0x2, 0x24d, 0x251, 0x3, 0x2, 0x2, 0x2, 0x24e, 0x24c, 0x3, 0x2, 0x2, 
    0x2, 0x24f, 0x252, 0x5, 0x5a, 0x2e, 0x2, 0x250, 0x252, 0x5, 0x5c, 0x2f, 
    0x2, 0x251, 0x24f, 0x3, 0x2, 0x2, 0x2, 0x251, 0x250, 0x3, 0x2, 0x2, 
    0x2, 0x251, 0x252, 0x3, 0x2, 0x2, 0x2, 0x252, 0x254, 0x3, 0x2, 0x2, 
    0x2, 0x253, 0x255, 0x7, 0x1f, 0x2, 0x2, 0x254, 0x253, 0x3, 0x2, 0x2, 
    0x2, 0x254, 0x255, 0x3, 0x2, 0x2, 0x2, 0x255, 0x256, 0x3, 0x2, 0x2, 
    0x2, 0x256, 0x257, 0x7, 0x19, 0x2, 0x2, 0x257, 0x59, 0x3, 0x2, 0x2, 
    0x2, 0x258, 0x259, 0x5, 0x7c, 0x3f, 0x2, 0x259, 0x25a, 0x7, 0x2e, 0x2, 
    0x2, 0x25a, 0x25b, 0x5, 0x7e, 0x40, 0x2, 0x25b, 0x5b, 0x3, 0x2, 0x2, 
    0x2, 0x25c, 0x25d, 0x7, 0x2f, 0x2, 0x2, 0x25d, 0x25e, 0x7, 0x2e, 0x2, 
    0x2, 0x25e, 0x25f, 0x5, 0x7e, 0x40, 0x2, 0x25f, 0x5d, 0x3, 0x2, 0x2, 
    0x2, 0x260, 0x263, 0x5, 0x16, 0xc, 0x2, 0x261, 0x263, 0x5, 0x18, 0xd, 
    0x2, 0x262, 0x260, 0x3, 0x2, 0x2, 0x2, 0x262, 0x261, 0x3, 0x2, 0x2, 
    0x2, 0x263, 0x269, 0x3, 0x2, 0x2, 0x2, 0x264, 0x267, 0x7, 0x25, 0x2, 
    0x2, 0x265, 0x268, 0x5, 0x16, 0xc, 0x2, 0x266, 0x268, 0x5, 0x18, 0xd, 
    0x2, 0x267, 0x265, 0x3, 0x2, 0x2, 0x2, 0x267, 0x266, 0x3, 0x2, 0x2, 
    0x2, 0x268, 0x26a, 0x3, 0x2, 0x2, 0x2, 0x269, 0x264, 0x3, 0x2, 0x2, 
    0x2, 0x26a, 0x26b, 0x3, 0x2, 0x2, 0x2, 0x26b, 0x269, 0x3, 0x2, 0x2, 
    0x2, 0x26b, 0x26c, 0x3, 0x2, 0x2, 0x2, 0x26c, 0x5f, 0x3, 0x2, 0x2, 0x2, 
    0x26d, 0x26e, 0x7, 0x11, 0x2, 0x2, 0x26e, 0x26f, 0x5, 0x7c, 0x3f, 0x2, 
    0x26f, 0x61, 0x3, 0x2, 0x2, 0x2, 0x270, 0x271, 0x7, 0x10, 0x2, 0x2, 
    0x271, 0x272, 0x5, 0x7c, 0x3f, 0x2, 0x272, 0x63, 0x3, 0x2, 0x2, 0x2, 
    0x273, 0x275, 0x9, 0x6, 0x2, 0x2, 0x274, 0x273, 0x3, 0x2, 0x2, 0x2, 
    0x274, 0x275, 0x3, 0x2, 0x2, 0x2, 0x275, 0x277, 0x3, 0x2, 0x2, 0x2, 
    0x276, 0x278, 0x7, 0x50, 0x2, 0x2, 0x277, 0x276, 0x3, 0x2, 0x2, 0x2, 
    0x278, 0x279, 0x3, 0x2, 0x2, 0x2, 0x279, 0x277, 0x3, 0x2, 0x2, 0x2, 
    0x279, 0x27a, 0x3, 0x2, 0x2, 0x2, 0x27a, 0x65, 0x3, 0x2, 0x2, 0x2, 0x27b, 
    0x27c, 0x5, 0x64, 0x33, 0x2, 0x27c, 0x27d, 0x7, 0x32, 0x2, 0x2, 0x27d, 
    0x27e, 0x5, 0x64, 0x33, 0x2, 0x27e, 0x67, 0x3, 0x2, 0x2, 0x2, 0x27f, 
    0x280, 0x7, 0x33, 0x2, 0x2, 0x280, 0x69, 0x3, 0x2, 0x2, 0x2, 0x281, 
    0x282, 0x7, 0x34, 0x2, 0x2, 0x282, 0x6b, 0x3, 0x2, 0x2, 0x2, 0x283, 
    0x284, 0x7, 0x35, 0x2, 0x2, 0x284, 0x285, 0x5, 0x7c, 0x3f, 0x2, 0x285, 
    0x286, 0x7, 0xf, 0x2, 0x2, 0x286, 0x287, 0x5, 0x32, 0x1a, 0x2, 0x287, 
    0x6d, 0x3, 0x2, 0x2, 0x2, 0x288, 0x289, 0x5, 0xc, 0x7, 0x2, 0x289, 0x28a, 
    0x7, 0x36, 0x2, 0x2, 0x28a, 0x28c, 0x3, 0x2, 0x2, 0x2, 0x28b, 0x288, 
    0x3, 0x2, 0x2, 0x2, 0x28c, 0x28d, 0x3, 0x2, 0x2, 0x2, 0x28d, 0x28b, 
    0x3, 0x2, 0x2, 0x2, 0x28d, 0x28e, 0x3, 0x2, 0x2, 0x2, 0x28e, 0x28f, 
    0x3, 0x2, 0x2, 0x2, 0x28f, 0x290, 0x5, 0x32, 0x1a, 0x2, 0x290, 0x6f, 
    0x3, 0x2, 0x2, 0x2, 0x291, 0x292, 0x7, 0x37, 0x2, 0x2, 0x292, 0x293, 
    0x5, 0x16, 0xc, 0x2, 0x293, 0x71, 0x3, 0x2, 0x2, 0x2, 0x294, 0x295, 
    0x7, 0x38, 0x2, 0x2, 0x295, 0x296, 0x5, 0x16, 0xc, 0x2, 0x296, 0x73, 
    0x3, 0x2, 0x2, 0x2, 0x297, 0x298, 0x5, 0x7c, 0x3f, 0x2, 0x298, 0x299, 
    0x7, 0x39, 0x2, 0x2, 0x299, 0x75, 0x3, 0x2, 0x2, 0x2, 0x29a, 0x29b, 
    0x5, 0x7c, 0x3f, 0x2, 0x29b, 0x29c, 0x7, 0x3a, 0x2, 0x2, 0x29c, 0x77, 
    0x3, 0x2, 0x2, 0x2, 0x29d, 0x29e, 0x7, 0x3b, 0x2, 0x2, 0x29e, 0x29f, 
    0x5, 0xc, 0x7, 0x2, 0x29f, 0x79, 0x3, 0x2, 0x2, 0x2, 0x2a0, 0x2a1, 0x5, 
    0x78, 0x3d, 0x2, 0x2a1, 0x2a2, 0x7, 0x3c, 0x2, 0x2, 0x2a2, 0x2a3, 0x5, 
    0x7c, 0x3f, 0x2, 0x2a3, 0x7b, 0x3, 0x2, 0x2, 0x2, 0x2a4, 0x2a5, 0x8, 
    0x3f, 0x1, 0x2, 0x2a5, 0x2f6, 0x5, 0xc, 0x7, 0x2, 0x2a6, 0x2a7, 0x7, 
    0xb, 0x2, 0x2, 0x2a7, 0x2a8, 0x5, 0x7c, 0x3f, 0x2, 0x2a8, 0x2a9, 0x7, 
    0xc, 0x2, 0x2, 0x2a9, 0x2f6, 0x3, 0x2, 0x2, 0x2, 0x2aa, 0x2f6, 0x5, 
    0x78, 0x3d, 0x2, 0x2ab, 0x2ac, 0x7, 0x3d, 0x2, 0x2, 0x2ac, 0x2af, 0x7, 
    0xe, 0x2, 0x2, 0x2ad, 0x2b0, 0x5, 0x1c, 0xf, 0x2, 0x2ae, 0x2b0, 0x5, 
    0xc, 0x7, 0x2, 0x2af, 0x2ad, 0x3, 0x2, 0x2, 0x2, 0x2af, 0x2ae, 0x3, 
    0x2, 0x2, 0x2, 0x2b0, 0x2f6, 0x3, 0x2, 0x2, 0x2, 0x2b1, 0x2b2, 0x7, 
    0x3e, 0x2, 0x2, 0x2b2, 0x2b3, 0x7, 0xe, 0x2, 0x2, 0x2b3, 0x2f6, 0x5, 
    0x7c, 0x3f, 0x26, 0x2b4, 0x2f6, 0x5, 0x5e, 0x30, 0x2, 0x2b5, 0x2f6, 
    0x5, 0x10, 0x9, 0x2, 0x2b6, 0x2f6, 0x5, 0x60, 0x31, 0x2, 0x2b7, 0x2f6, 
    0x5, 0x62, 0x32, 0x2, 0x2b8, 0x2f6, 0x5, 0x64, 0x33, 0x2, 0x2b9, 0x2f6, 
    0x5, 0x66, 0x34, 0x2, 0x2ba, 0x2f6, 0x5, 0xe, 0x8, 0x2, 0x2bb, 0x2f6, 
    0x5, 0x16, 0xc, 0x2, 0x2bc, 0x2f6, 0x5, 0x18, 0xd, 0x2, 0x2bd, 0x2be, 
    0x7, 0x26, 0x2, 0x2, 0x2be, 0x2bf, 0x5, 0x4c, 0x27, 0x2, 0x2bf, 0x2c9, 
    0x7, 0x25, 0x2, 0x2, 0x2c0, 0x2ca, 0x5, 0x7c, 0x3f, 0x2, 0x2c1, 0x2c5, 
    0x7, 0x18, 0x2, 0x2, 0x2c2, 0x2c4, 0x5, 0x7e, 0x40, 0x2, 0x2c3, 0x2c2, 
    0x3, 0x2, 0x2, 0x2, 0x2c4, 0x2c7, 0x3, 0x2, 0x2, 0x2, 0x2c5, 0x2c3, 
    0x3, 0x2, 0x2, 0x2, 0x2c5, 0x2c6, 0x3, 0x2, 0x2, 0x2, 0x2c6, 0x2c8, 
    0x3, 0x2, 0x2, 0x2, 0x2c7, 0x2c5, 0x3, 0x2, 0x2, 0x2, 0x2c8, 0x2ca, 
    0x7, 0x19, 0x2, 0x2, 0x2c9, 0x2c0, 0x3, 0x2, 0x2, 0x2, 0x2c9, 0x2c1, 
    0x3, 0x2, 0x2, 0x2, 0x2ca, 0x2f6, 0x3, 0x2, 0x2, 0x2, 0x2cb, 0x2cc, 
    0x5, 0xc, 0x7, 0x2, 0x2cc, 0x2cd, 0x7, 0x36, 0x2, 0x2, 0x2cd, 0x2cf, 
    0x3, 0x2, 0x2, 0x2, 0x2ce, 0x2cb, 0x3, 0x2, 0x2, 0x2, 0x2cf, 0x2d0, 
    0x3, 0x2, 0x2, 0x2, 0x2d0, 0x2ce, 0x3, 0x2, 0x2, 0x2, 0x2d0, 0x2d1, 
    0x3, 0x2, 0x2, 0x2, 0x2d1, 0x2d2, 0x3, 0x2, 0x2, 0x2, 0x2d2, 0x2d3, 
    0x5, 0xc, 0x7, 0x2, 0x2d3, 0x2d9, 0x7, 0x7, 0x2, 0x2, 0x2d4, 0x2d5, 
    0x5, 0x7c, 0x3f, 0x2, 0x2d5, 0x2d6, 0x7, 0x1f, 0x2, 0x2, 0x2d6, 0x2d8, 
    0x3, 0x2, 0x2, 0x2, 0x2d7, 0x2d4, 0x3, 0x2, 0x2, 0x2, 0x2d8, 0x2db, 
    0x3, 0x2, 0x2, 0x2, 0x2d9, 0x2d7, 0x3, 0x2, 0x2, 0x2, 0x2d9, 0x2da, 
    0x3, 0x2, 0x2, 0x2, 0x2da, 0x2dd, 0x3, 0x2, 0x2, 0x2, 0x2db, 0x2d9, 
    0x3, 0x2, 0x2, 0x2, 0x2dc, 0x2de, 0x5, 0x7c, 0x3f, 0x2, 0x2dd, 0x2dc, 
    0x3, 0x2, 0x2, 0x2, 0x2dd, 0x2de, 0x3, 0x2, 0x2, 0x2, 0x2de, 0x2df, 
    0x3, 0x2, 0x2, 0x2, 0x2df, 0x2e0, 0x7, 0x8, 0x2, 0x2, 0x2e0, 0x2f6, 
    0x3, 0x2, 0x2, 0x2, 0x2e1, 0x2e2, 0x7, 0x4a, 0x2, 0x2, 0x2e2, 0x2f6, 
    0x5, 0x7c, 0x3f, 0x4, 0x2e3, 0x2e4, 0x7, 0x4b, 0x2, 0x2, 0x2e4, 0x2e5, 
    0x5, 0x7c, 0x3f, 0x2, 0x2e5, 0x2e6, 0x7, 0xf, 0x2, 0x2, 0x2e6, 0x2e7, 
    0x5, 0x7c, 0x3f, 0x2, 0x2e7, 0x2ed, 0x7, 0x7, 0x2, 0x2, 0x2e8, 0x2e9, 
    0x5, 0x7c, 0x3f, 0x2, 0x2e9, 0x2ea, 0x7, 0x1f, 0x2, 0x2, 0x2ea, 0x2ec, 
    0x3, 0x2, 0x2, 0x2, 0x2eb, 0x2e8, 0x3, 0x2, 0x2, 0x2, 0x2ec, 0x2ef, 
    0x3, 0x2, 0x2, 0x2, 0x2ed, 0x2eb, 0x3, 0x2, 0x2, 0x2, 0x2ed, 0x2ee, 
    0x3, 0x2, 0x2, 0x2, 0x2ee, 0x2f1, 0x3, 0x2, 0x2, 0x2, 0x2ef, 0x2ed, 
    0x3, 0x2, 0x2, 0x2, 0x2f0, 0x2f2, 0x5, 0x7c, 0x3f, 0x2, 0x2f1, 0x2f0, 
    0x3, 0x2, 0x2, 0x2, 0x2f1, 0x2f2, 0x3, 0x2, 0x2, 0x2, 0x2f2, 0x2f3, 
    0x3, 0x2, 0x2, 0x2, 0x2f3, 0x2f4, 0x7, 0x8, 0x2, 0x2, 0x2f4, 0x2f6, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2a4, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2a6, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2aa, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2ab, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2b1, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2b4, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2b5, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2b6, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2b7, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2b8, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2b9, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2ba, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2bb, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2bc, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2bd, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2ce, 
    0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2e1, 0x3, 0x2, 0x2, 0x2, 0x2f5, 0x2e3, 
    0x3, 0x2, 0x2, 0x2, 0x2f6, 0x363, 0x3, 0x2, 0x2, 0x2, 0x2f7, 0x2f8, 
    0xc, 0x1b, 0x2, 0x2, 0x2f8, 0x2f9, 0x7, 0x30, 0x2, 0x2, 0x2f9, 0x362, 
    0x5, 0x7c, 0x3f, 0x1c, 0x2fa, 0x2fb, 0xc, 0x1a, 0x2, 0x2, 0x2fb, 0x2fc, 
    0x7, 0x31, 0x2, 0x2, 0x2fc, 0x362, 0x5, 0x7c, 0x3f, 0x1b, 0x2fd, 0x2fe, 
    0xc, 0x19, 0x2, 0x2, 0x2fe, 0x2ff, 0x7, 0x10, 0x2, 0x2, 0x2ff, 0x362, 
    0x5, 0x7c, 0x3f, 0x1a, 0x300, 0x301, 0xc, 0x18, 0x2, 0x2, 0x301, 0x302, 
    0x7, 0x3f, 0x2, 0x2, 0x302, 0x362, 0x5, 0x7c, 0x3f, 0x19, 0x303, 0x304, 
    0xc, 0x17, 0x2, 0x2, 0x304, 0x305, 0x7, 0x11, 0x2, 0x2, 0x305, 0x362, 
    0x5, 0x7c, 0x3f, 0x18, 0x306, 0x307, 0xc, 0x16, 0x2, 0x2, 0x307, 0x308, 
    0x7, 0x40, 0x2, 0x2, 0x308, 0x362, 0x5, 0x7c, 0x3f, 0x17, 0x309, 0x30a, 
    0xc, 0x15, 0x2, 0x2, 0x30a, 0x30b, 0x7, 0x41, 0x2, 0x2, 0x30b, 0x362, 
    0x5, 0x7c, 0x3f, 0x16, 0x30c, 0x30d, 0xc, 0x14, 0x2, 0x2, 0x30d, 0x30e, 
    0x7, 0x3b, 0x2, 0x2, 0x30e, 0x362, 0x5, 0x7c, 0x3f, 0x15, 0x30f, 0x310, 
    0xc, 0xf, 0x2, 0x2, 0x310, 0x311, 0x7, 0x42, 0x2, 0x2, 0x311, 0x362, 
    0x5, 0x7c, 0x3f, 0x10, 0x312, 0x313, 0xc, 0xe, 0x2, 0x2, 0x313, 0x314, 
    0x7, 0x43, 0x2, 0x2, 0x314, 0x362, 0x5, 0x7c, 0x3f, 0xf, 0x315, 0x316, 
    0xc, 0xd, 0x2, 0x2, 0x316, 0x317, 0x7, 0x44, 0x2, 0x2, 0x317, 0x362, 
    0x5, 0x7c, 0x3f, 0xe, 0x318, 0x319, 0xc, 0xc, 0x2, 0x2, 0x319, 0x31a, 
    0x7, 0x45, 0x2, 0x2, 0x31a, 0x362, 0x5, 0x7c, 0x3f, 0xd, 0x31b, 0x31c, 
    0xc, 0xb, 0x2, 0x2, 0x31c, 0x31d, 0x7, 0x15, 0x2, 0x2, 0x31d, 0x362, 
    0x5, 0x7c, 0x3f, 0xc, 0x31e, 0x31f, 0xc, 0xa, 0x2, 0x2, 0x31f, 0x320, 
    0x7, 0x16, 0x2, 0x2, 0x320, 0x362, 0x5, 0x7c, 0x3f, 0xb, 0x321, 0x322, 
    0xc, 0x9, 0x2, 0x2, 0x322, 0x323, 0x7, 0x46, 0x2, 0x2, 0x323, 0x362, 
    0x5, 0x7c, 0x3f, 0xa, 0x324, 0x325, 0xc, 0x8, 0x2, 0x2, 0x325, 0x326, 
    0x7, 0x47, 0x2, 0x2, 0x326, 0x362, 0x5, 0x7c, 0x3f, 0x9, 0x327, 0x328, 
    0xc, 0x7, 0x2, 0x2, 0x328, 0x329, 0x7, 0x48, 0x2, 0x2, 0x329, 0x362, 
    0x5, 0x7c, 0x3f, 0x8, 0x32a, 0x32b, 0xc, 0x6, 0x2, 0x2, 0x32b, 0x32c, 
    0x7, 0x49, 0x2, 0x2, 0x32c, 0x362, 0x5, 0x7c, 0x3f, 0x7, 0x32d, 0x32e, 
    0xc, 0x29, 0x2, 0x2, 0x32e, 0x362, 0x7, 0x39, 0x2, 0x2, 0x32f, 0x330, 
    0xc, 0x28, 0x2, 0x2, 0x330, 0x362, 0x7, 0x3a, 0x2, 0x2, 0x331, 0x332, 
    0xc, 0x22, 0x2, 0x2, 0x332, 0x333, 0x7, 0xb, 0x2, 0x2, 0x333, 0x334, 
    0x5, 0x7c, 0x3f, 0x2, 0x334, 0x335, 0x7, 0xc, 0x2, 0x2, 0x335, 0x362, 
    0x3, 0x2, 0x2, 0x2, 0x336, 0x337, 0xc, 0x13, 0x2, 0x2, 0x337, 0x33d, 
    0x7, 0x7, 0x2, 0x2, 0x338, 0x339, 0x5, 0x7c, 0x3f, 0x2, 0x339, 0x33a, 
    0x7, 0x1f, 0x2, 0x2, 0x33a, 0x33c, 0x3, 0x2, 0x2, 0x2, 0x33b, 0x338, 
    0x3, 0x2, 0x2, 0x2, 0x33c, 0x33f, 0x3, 0x2, 0x2, 0x2, 0x33d, 0x33b, 
    0x3, 0x2, 0x2, 0x2, 0x33d, 0x33e, 0x3, 0x2, 0x2, 0x2, 0x33e, 0x341, 
    0x3, 0x2, 0x2, 0x2, 0x33f, 0x33d, 0x3, 0x2, 0x2, 0x2, 0x340, 0x342, 
    0x5, 0x7c, 0x3f, 0x2, 0x341, 0x340, 0x3, 0x2, 0x2, 0x2, 0x341, 0x342, 
    0x3, 0x2, 0x2, 0x2, 0x342, 0x343, 0x3, 0x2, 0x2, 0x2, 0x343, 0x362, 
    0x7, 0x8, 0x2, 0x2, 0x344, 0x345, 0xc, 0x12, 0x2, 0x2, 0x345, 0x346, 
    0x7, 0x15, 0x2, 0x2, 0x346, 0x347, 0x5, 0x36, 0x1c, 0x2, 0x347, 0x348, 
    0x7, 0x16, 0x2, 0x2, 0x348, 0x34e, 0x7, 0x7, 0x2, 0x2, 0x349, 0x34a, 
    0x5, 0x7c, 0x3f, 0x2, 0x34a, 0x34b, 0x7, 0x1f, 0x2, 0x2, 0x34b, 0x34d, 
    0x3, 0x2, 0x2, 0x2, 0x34c, 0x349, 0x3, 0x2, 0x2, 0x2, 0x34d, 0x350, 
    0x3, 0x2, 0x2, 0x2, 0x34e, 0x34c, 0x3, 0x2, 0x2, 0x2, 0x34e, 0x34f, 
    0x3, 0x2, 0x2, 0x2, 0x34f, 0x352, 0x3, 0x2, 0x2, 0x2, 0x350, 0x34e, 
    0x3, 0x2, 0x2, 0x2, 0x351, 0x353, 0x5, 0x7c, 0x3f, 0x2, 0x352, 0x351, 
    0x3, 0x2, 0x2, 0x2, 0x352, 0x353, 0x3, 0x2, 0x2, 0x2, 0x353, 0x354, 
    0x3, 0x2, 0x2, 0x2, 0x354, 0x355, 0x7, 0x8, 0x2, 0x2, 0x355, 0x362, 
    0x3, 0x2, 0x2, 0x2, 0x356, 0x357, 0xc, 0x5, 0x2, 0x2, 0x357, 0x35d, 
    0x7, 0x32, 0x2, 0x2, 0x358, 0x359, 0x5, 0xc, 0x7, 0x2, 0x359, 0x35a, 
    0x7, 0x32, 0x2, 0x2, 0x35a, 0x35c, 0x3, 0x2, 0x2, 0x2, 0x35b, 0x358, 
    0x3, 0x2, 0x2, 0x2, 0x35c, 0x35f, 0x3, 0x2, 0x2, 0x2, 0x35d, 0x35b, 
    0x3, 0x2, 0x2, 0x2, 0x35d, 0x35e, 0x3, 0x2, 0x2, 0x2, 0x35e, 0x360, 
    0x3, 0x2, 0x2, 0x2, 0x35f, 0x35d, 0x3, 0x2, 0x2, 0x2, 0x360, 0x362, 
    0x5, 0xc, 0x7, 0x2, 0x361, 0x2f7, 0x3, 0x2, 0x2, 0x2, 0x361, 0x2fa, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x2fd, 0x3, 0x2, 0x2, 0x2, 0x361, 0x300, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x303, 0x3, 0x2, 0x2, 0x2, 0x361, 0x306, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x309, 0x3, 0x2, 0x2, 0x2, 0x361, 0x30c, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x30f, 0x3, 0x2, 0x2, 0x2, 0x361, 0x312, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x315, 0x3, 0x2, 0x2, 0x2, 0x361, 0x318, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x31b, 0x3, 0x2, 0x2, 0x2, 0x361, 0x31e, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x321, 0x3, 0x2, 0x2, 0x2, 0x361, 0x324, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x327, 0x3, 0x2, 0x2, 0x2, 0x361, 0x32a, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x32d, 0x3, 0x2, 0x2, 0x2, 0x361, 0x32f, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x331, 0x3, 0x2, 0x2, 0x2, 0x361, 0x336, 
    0x3, 0x2, 0x2, 0x2, 0x361, 0x344, 0x3, 0x2, 0x2, 0x2, 0x361, 0x356, 
    0x3, 0x2, 0x2, 0x2, 0x362, 0x365, 0x3, 0x2, 0x2, 0x2, 0x363, 0x361, 
    0x3, 0x2, 0x2, 0x2, 0x363, 0x364, 0x3, 0x2, 0x2, 0x2, 0x364, 0x7d, 0x3, 
    0x2, 0x2, 0x2, 0x365, 0x363, 0x3, 0x2, 0x2, 0x2, 0x366, 0x379, 0x5, 
    0x7c, 0x3f, 0x2, 0x367, 0x379, 0x5, 0x7a, 0x3e, 0x2, 0x368, 0x379, 0x5, 
    0x2, 0x2, 0x2, 0x369, 0x379, 0x5, 0x74, 0x3b, 0x2, 0x36a, 0x379, 0x5, 
    0x76, 0x3c, 0x2, 0x36b, 0x379, 0x5, 0x82, 0x42, 0x2, 0x36c, 0x379, 0x5, 
    0x12, 0xa, 0x2, 0x36d, 0x379, 0x5, 0x14, 0xb, 0x2, 0x36e, 0x379, 0x5, 
    0x3c, 0x1f, 0x2, 0x36f, 0x379, 0x5, 0x4a, 0x26, 0x2, 0x370, 0x379, 0x5, 
    0x56, 0x2c, 0x2, 0x371, 0x379, 0x5, 0x70, 0x39, 0x2, 0x372, 0x379, 0x5, 
    0x72, 0x3a, 0x2, 0x373, 0x379, 0x5, 0x1e, 0x10, 0x2, 0x374, 0x379, 0x5, 
    0x20, 0x11, 0x2, 0x375, 0x379, 0x5, 0x68, 0x35, 0x2, 0x376, 0x379, 0x5, 
    0x6a, 0x36, 0x2, 0x377, 0x379, 0x5, 0x6c, 0x37, 0x2, 0x378, 0x366, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x367, 0x3, 0x2, 0x2, 0x2, 0x378, 0x368, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x369, 0x3, 0x2, 0x2, 0x2, 0x378, 0x36a, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x36b, 0x3, 0x2, 0x2, 0x2, 0x378, 0x36c, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x36d, 0x3, 0x2, 0x2, 0x2, 0x378, 0x36e, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x36f, 0x3, 0x2, 0x2, 0x2, 0x378, 0x370, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x371, 0x3, 0x2, 0x2, 0x2, 0x378, 0x372, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x373, 0x3, 0x2, 0x2, 0x2, 0x378, 0x374, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x375, 0x3, 0x2, 0x2, 0x2, 0x378, 0x376, 0x3, 
    0x2, 0x2, 0x2, 0x378, 0x377, 0x3, 0x2, 0x2, 0x2, 0x379, 0x37a, 0x3, 
    0x2, 0x2, 0x2, 0x37a, 0x37b, 0x7, 0x2a, 0x2, 0x2, 0x37b, 0x38a, 0x3, 
    0x2, 0x2, 0x2, 0x37c, 0x388, 0x5, 0x4e, 0x28, 0x2, 0x37d, 0x388, 0x5, 
    0x38, 0x1d, 0x2, 0x37e, 0x388, 0x5, 0x3a, 0x1e, 0x2, 0x37f, 0x388, 0x5, 
    0x42, 0x22, 0x2, 0x380, 0x388, 0x5, 0x58, 0x2d, 0x2, 0x381, 0x388, 0x5, 
    0x54, 0x2b, 0x2, 0x382, 0x388, 0x5, 0x52, 0x2a, 0x2, 0x383, 0x388, 0x5, 
    0x22, 0x12, 0x2, 0x384, 0x388, 0x5, 0x28, 0x15, 0x2, 0x385, 0x388, 0x5, 
    0x44, 0x23, 0x2, 0x386, 0x388, 0x5, 0x26, 0x14, 0x2, 0x387, 0x37c, 0x3, 
    0x2, 0x2, 0x2, 0x387, 0x37d, 0x3, 0x2, 0x2, 0x2, 0x387, 0x37e, 0x3, 
    0x2, 0x2, 0x2, 0x387, 0x37f, 0x3, 0x2, 0x2, 0x2, 0x387, 0x380, 0x3, 
    0x2, 0x2, 0x2, 0x387, 0x381, 0x3, 0x2, 0x2, 0x2, 0x387, 0x382, 0x3, 
    0x2, 0x2, 0x2, 0x387, 0x383, 0x3, 0x2, 0x2, 0x2, 0x387, 0x384, 0x3, 
    0x2, 0x2, 0x2, 0x387, 0x385, 0x3, 0x2, 0x2, 0x2, 0x387, 0x386, 0x3, 
    0x2, 0x2, 0x2, 0x388, 0x38a, 0x3, 0x2, 0x2, 0x2, 0x389, 0x378, 0x3, 
    0x2, 0x2, 0x2, 0x389, 0x387, 0x3, 0x2, 0x2, 0x2, 0x38a, 0x7f, 0x3, 0x2, 
    0x2, 0x2, 0x38b, 0x38d, 0x5, 0x7e, 0x40, 0x2, 0x38c, 0x38b, 0x3, 0x2, 
    0x2, 0x2, 0x38d, 0x390, 0x3, 0x2, 0x2, 0x2, 0x38e, 0x38c, 0x3, 0x2, 
    0x2, 0x2, 0x38e, 0x38f, 0x3, 0x2, 0x2, 0x2, 0x38f, 0x81, 0x3, 0x2, 0x2, 
    0x2, 0x390, 0x38e, 0x3, 0x2, 0x2, 0x2, 0x391, 0x393, 0x7, 0x4c, 0x2, 
    0x2, 0x392, 0x394, 0xb, 0x2, 0x2, 0x2, 0x393, 0x392, 0x3, 0x2, 0x2, 
    0x2, 0x394, 0x395, 0x3, 0x2, 0x2, 0x2, 0x395, 0x396, 0x3, 0x2, 0x2, 
    0x2, 0x395, 0x393, 0x3, 0x2, 0x2, 0x2, 0x396, 0x397, 0x3, 0x2, 0x2, 
    0x2, 0x397, 0x398, 0x7, 0x4d, 0x2, 0x2, 0x398, 0x83, 0x3, 0x2, 0x2, 
    0x2, 0x54, 0x88, 0x8e, 0x94, 0x9d, 0xa2, 0xa4, 0xba, 0xc5, 0xcd, 0xd3, 
    0xd9, 0xdf, 0xe4, 0xf4, 0xfa, 0x102, 0x10b, 0x116, 0x123, 0x127, 0x134, 
    0x138, 0x140, 0x146, 0x14f, 0x153, 0x161, 0x165, 0x16b, 0x171, 0x176, 
    0x182, 0x18d, 0x193, 0x1a5, 0x1af, 0x1bf, 0x1c3, 0x1c9, 0x1d5, 0x1df, 
    0x1e8, 0x1ec, 0x1f6, 0x1fa, 0x202, 0x210, 0x215, 0x21c, 0x22c, 0x239, 
    0x240, 0x24c, 0x251, 0x254, 0x262, 0x267, 0x26b, 0x274, 0x279, 0x28d, 
    0x2af, 0x2c5, 0x2c9, 0x2d0, 0x2d9, 0x2dd, 0x2ed, 0x2f1, 0x2f5, 0x33d, 
    0x341, 0x34e, 0x352, 0x35d, 0x361, 0x363, 0x378, 0x387, 0x389, 0x38e, 
    0x395, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

XanatharParser::Initializer XanatharParser::_init;
