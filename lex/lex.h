//
// Created by proc-daemon on 1/15/19.
//

#ifndef XANATHAR_2_0_LEX_H
#define XANATHAR_2_0_LEX_H

#include "XanatharLexer.h"
#include "XanatharParser.h"
#include "XanatharBaseVisitor.h"
#include "../options/options.h"
#include "../irgen/gen.h"
#include "../irgen/namemap.h"
#include "../irgen/ir.h"
#include <antlr4-runtime.h>
#include <iostream>
#include <fstream>

#include "ast/node.h"

namespace xanathar {
    namespace lex {
        void lex(const std::string &file, const options::xArgs &args);
    }
}

#endif //XANATHAR_2_0_LEX_H
