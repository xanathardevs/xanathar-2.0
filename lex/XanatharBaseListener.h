
// Generated from Xanathar.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "XanatharListener.h"


/**
 * This class provides an empty implementation of XanatharListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  XanatharBaseListener : public XanatharListener {
public:

  virtual void enterPrep(XanatharParser::PrepContext * /*ctx*/) override { }
  virtual void exitPrep(XanatharParser::PrepContext * /*ctx*/) override { }

  virtual void enterString_inner(XanatharParser::String_innerContext * /*ctx*/) override { }
  virtual void exitString_inner(XanatharParser::String_innerContext * /*ctx*/) override { }

  virtual void enterEscaped_string(XanatharParser::Escaped_stringContext * /*ctx*/) override { }
  virtual void exitEscaped_string(XanatharParser::Escaped_stringContext * /*ctx*/) override { }

  virtual void enterLetter(XanatharParser::LetterContext * /*ctx*/) override { }
  virtual void exitLetter(XanatharParser::LetterContext * /*ctx*/) override { }

  virtual void enterCname(XanatharParser::CnameContext * /*ctx*/) override { }
  virtual void exitCname(XanatharParser::CnameContext * /*ctx*/) override { }

  virtual void enterWord(XanatharParser::WordContext * /*ctx*/) override { }
  virtual void exitWord(XanatharParser::WordContext * /*ctx*/) override { }

  virtual void enterStr(XanatharParser::StrContext * /*ctx*/) override { }
  virtual void exitStr(XanatharParser::StrContext * /*ctx*/) override { }

  virtual void enterCast(XanatharParser::CastContext * /*ctx*/) override { }
  virtual void exitCast(XanatharParser::CastContext * /*ctx*/) override { }

  virtual void enterUse_statement(XanatharParser::Use_statementContext * /*ctx*/) override { }
  virtual void exitUse_statement(XanatharParser::Use_statementContext * /*ctx*/) override { }

  virtual void enterDeclare_statement(XanatharParser::Declare_statementContext * /*ctx*/) override { }
  virtual void exitDeclare_statement(XanatharParser::Declare_statementContext * /*ctx*/) override { }

  virtual void enterVariable_phrase(XanatharParser::Variable_phraseContext * /*ctx*/) override { }
  virtual void exitVariable_phrase(XanatharParser::Variable_phraseContext * /*ctx*/) override { }

  virtual void enterRaw_var(XanatharParser::Raw_varContext * /*ctx*/) override { }
  virtual void exitRaw_var(XanatharParser::Raw_varContext * /*ctx*/) override { }

  virtual void enterAs_phrase(XanatharParser::As_phraseContext * /*ctx*/) override { }
  virtual void exitAs_phrase(XanatharParser::As_phraseContext * /*ctx*/) override { }

  virtual void enterTypespec(XanatharParser::TypespecContext * /*ctx*/) override { }
  virtual void exitTypespec(XanatharParser::TypespecContext * /*ctx*/) override { }

  virtual void enterVariable_setting(XanatharParser::Variable_settingContext * /*ctx*/) override { }
  virtual void exitVariable_setting(XanatharParser::Variable_settingContext * /*ctx*/) override { }

  virtual void enterLet(XanatharParser::LetContext * /*ctx*/) override { }
  virtual void exitLet(XanatharParser::LetContext * /*ctx*/) override { }

  virtual void enterClass_(XanatharParser::Class_Context * /*ctx*/) override { }
  virtual void exitClass_(XanatharParser::Class_Context * /*ctx*/) override { }

  virtual void enterClass__specs(XanatharParser::Class__specsContext * /*ctx*/) override { }
  virtual void exitClass__specs(XanatharParser::Class__specsContext * /*ctx*/) override { }

  virtual void enterExtends_(XanatharParser::Extends_Context * /*ctx*/) override { }
  virtual void exitExtends_(XanatharParser::Extends_Context * /*ctx*/) override { }

  virtual void enterInterface_(XanatharParser::Interface_Context * /*ctx*/) override { }
  virtual void exitInterface_(XanatharParser::Interface_Context * /*ctx*/) override { }

  virtual void enterInterface_fn(XanatharParser::Interface_fnContext * /*ctx*/) override { }
  virtual void exitInterface_fn(XanatharParser::Interface_fnContext * /*ctx*/) override { }

  virtual void enterDeclared_var(XanatharParser::Declared_varContext * /*ctx*/) override { }
  virtual void exitDeclared_var(XanatharParser::Declared_varContext * /*ctx*/) override { }

  virtual void enterTypes(XanatharParser::TypesContext * /*ctx*/) override { }
  virtual void exitTypes(XanatharParser::TypesContext * /*ctx*/) override { }

  virtual void enterInheritance(XanatharParser::InheritanceContext * /*ctx*/) override { }
  virtual void exitInheritance(XanatharParser::InheritanceContext * /*ctx*/) override { }

  virtual void enterFncall(XanatharParser::FncallContext * /*ctx*/) override { }
  virtual void exitFncall(XanatharParser::FncallContext * /*ctx*/) override { }

  virtual void enterGeneric_call(XanatharParser::Generic_callContext * /*ctx*/) override { }
  virtual void exitGeneric_call(XanatharParser::Generic_callContext * /*ctx*/) override { }

  virtual void enterGp(XanatharParser::GpContext * /*ctx*/) override { }
  virtual void exitGp(XanatharParser::GpContext * /*ctx*/) override { }

  virtual void enterFn(XanatharParser::FnContext * /*ctx*/) override { }
  virtual void exitFn(XanatharParser::FnContext * /*ctx*/) override { }

  virtual void enterHook(XanatharParser::HookContext * /*ctx*/) override { }
  virtual void exitHook(XanatharParser::HookContext * /*ctx*/) override { }

  virtual void enterTemplate_(XanatharParser::Template_Context * /*ctx*/) override { }
  virtual void exitTemplate_(XanatharParser::Template_Context * /*ctx*/) override { }

  virtual void enterClass_es(XanatharParser::Class_esContext * /*ctx*/) override { }
  virtual void exitClass_es(XanatharParser::Class_esContext * /*ctx*/) override { }

  virtual void enterClass__(XanatharParser::Class__Context * /*ctx*/) override { }
  virtual void exitClass__(XanatharParser::Class__Context * /*ctx*/) override { }

  virtual void enterVirtual_fn(XanatharParser::Virtual_fnContext * /*ctx*/) override { }
  virtual void exitVirtual_fn(XanatharParser::Virtual_fnContext * /*ctx*/) override { }

  virtual void enterOverride(XanatharParser::OverrideContext * /*ctx*/) override { }
  virtual void exitOverride(XanatharParser::OverrideContext * /*ctx*/) override { }

  virtual void enterOverride_name(XanatharParser::Override_nameContext * /*ctx*/) override { }
  virtual void exitOverride_name(XanatharParser::Override_nameContext * /*ctx*/) override { }

  virtual void enterOverride_param(XanatharParser::Override_paramContext * /*ctx*/) override { }
  virtual void exitOverride_param(XanatharParser::Override_paramContext * /*ctx*/) override { }

  virtual void enterLambda(XanatharParser::LambdaContext * /*ctx*/) override { }
  virtual void exitLambda(XanatharParser::LambdaContext * /*ctx*/) override { }

  virtual void enterParams(XanatharParser::ParamsContext * /*ctx*/) override { }
  virtual void exitParams(XanatharParser::ParamsContext * /*ctx*/) override { }

  virtual void enterIf_(XanatharParser::If_Context * /*ctx*/) override { }
  virtual void exitIf_(XanatharParser::If_Context * /*ctx*/) override { }

  virtual void enterElse_(XanatharParser::Else_Context * /*ctx*/) override { }
  virtual void exitElse_(XanatharParser::Else_Context * /*ctx*/) override { }

  virtual void enterFor_(XanatharParser::For_Context * /*ctx*/) override { }
  virtual void exitFor_(XanatharParser::For_Context * /*ctx*/) override { }

  virtual void enterWhile_(XanatharParser::While_Context * /*ctx*/) override { }
  virtual void exitWhile_(XanatharParser::While_Context * /*ctx*/) override { }

  virtual void enterRet(XanatharParser::RetContext * /*ctx*/) override { }
  virtual void exitRet(XanatharParser::RetContext * /*ctx*/) override { }

  virtual void enterMatch_(XanatharParser::Match_Context * /*ctx*/) override { }
  virtual void exitMatch_(XanatharParser::Match_Context * /*ctx*/) override { }

  virtual void enterMatch_arm(XanatharParser::Match_armContext * /*ctx*/) override { }
  virtual void exitMatch_arm(XanatharParser::Match_armContext * /*ctx*/) override { }

  virtual void enterDefault_arm(XanatharParser::Default_armContext * /*ctx*/) override { }
  virtual void exitDefault_arm(XanatharParser::Default_armContext * /*ctx*/) override { }

  virtual void enterRef(XanatharParser::RefContext * /*ctx*/) override { }
  virtual void exitRef(XanatharParser::RefContext * /*ctx*/) override { }

  virtual void enterPtr(XanatharParser::PtrContext * /*ctx*/) override { }
  virtual void exitPtr(XanatharParser::PtrContext * /*ctx*/) override { }

  virtual void enterDeref(XanatharParser::DerefContext * /*ctx*/) override { }
  virtual void exitDeref(XanatharParser::DerefContext * /*ctx*/) override { }

  virtual void enterNumber(XanatharParser::NumberContext * /*ctx*/) override { }
  virtual void exitNumber(XanatharParser::NumberContext * /*ctx*/) override { }

  virtual void enterFloat_(XanatharParser::Float_Context * /*ctx*/) override { }
  virtual void exitFloat_(XanatharParser::Float_Context * /*ctx*/) override { }

  virtual void enterBreak_(XanatharParser::Break_Context * /*ctx*/) override { }
  virtual void exitBreak_(XanatharParser::Break_Context * /*ctx*/) override { }

  virtual void enterSkip(XanatharParser::SkipContext * /*ctx*/) override { }
  virtual void exitSkip(XanatharParser::SkipContext * /*ctx*/) override { }

  virtual void enterNew_(XanatharParser::New_Context * /*ctx*/) override { }
  virtual void exitNew_(XanatharParser::New_Context * /*ctx*/) override { }

  virtual void enterCall_sealed_class_(XanatharParser::Call_sealed_class_Context * /*ctx*/) override { }
  virtual void exitCall_sealed_class_(XanatharParser::Call_sealed_class_Context * /*ctx*/) override { }

  virtual void enterLock(XanatharParser::LockContext * /*ctx*/) override { }
  virtual void exitLock(XanatharParser::LockContext * /*ctx*/) override { }

  virtual void enterUnlock(XanatharParser::UnlockContext * /*ctx*/) override { }
  virtual void exitUnlock(XanatharParser::UnlockContext * /*ctx*/) override { }

  virtual void enterInc(XanatharParser::IncContext * /*ctx*/) override { }
  virtual void exitInc(XanatharParser::IncContext * /*ctx*/) override { }

  virtual void enterDec(XanatharParser::DecContext * /*ctx*/) override { }
  virtual void exitDec(XanatharParser::DecContext * /*ctx*/) override { }

  virtual void enterReg(XanatharParser::RegContext * /*ctx*/) override { }
  virtual void exitReg(XanatharParser::RegContext * /*ctx*/) override { }

  virtual void enterReg_set(XanatharParser::Reg_setContext * /*ctx*/) override { }
  virtual void exitReg_set(XanatharParser::Reg_setContext * /*ctx*/) override { }

  virtual void enterBinaryNot(XanatharParser::BinaryNotContext * /*ctx*/) override { }
  virtual void exitBinaryNot(XanatharParser::BinaryNotContext * /*ctx*/) override { }

  virtual void enterLambdaCreation(XanatharParser::LambdaCreationContext * /*ctx*/) override { }
  virtual void exitLambdaCreation(XanatharParser::LambdaCreationContext * /*ctx*/) override { }

  virtual void enterLessOrEqual(XanatharParser::LessOrEqualContext * /*ctx*/) override { }
  virtual void exitLessOrEqual(XanatharParser::LessOrEqualContext * /*ctx*/) override { }

  virtual void enterMultiplication(XanatharParser::MultiplicationContext * /*ctx*/) override { }
  virtual void exitMultiplication(XanatharParser::MultiplicationContext * /*ctx*/) override { }

  virtual void enterBinaryAnd(XanatharParser::BinaryAndContext * /*ctx*/) override { }
  virtual void exitBinaryAnd(XanatharParser::BinaryAndContext * /*ctx*/) override { }

  virtual void enterBareword(XanatharParser::BarewordContext * /*ctx*/) override { }
  virtual void exitBareword(XanatharParser::BarewordContext * /*ctx*/) override { }

  virtual void enterCallSealedClass(XanatharParser::CallSealedClassContext * /*ctx*/) override { }
  virtual void exitCallSealedClass(XanatharParser::CallSealedClassContext * /*ctx*/) override { }

  virtual void enterBinaryXor(XanatharParser::BinaryXorContext * /*ctx*/) override { }
  virtual void exitBinaryXor(XanatharParser::BinaryXorContext * /*ctx*/) override { }

  virtual void enterGetSizeOf(XanatharParser::GetSizeOfContext * /*ctx*/) override { }
  virtual void exitGetSizeOf(XanatharParser::GetSizeOfContext * /*ctx*/) override { }

  virtual void enterPointer(XanatharParser::PointerContext * /*ctx*/) override { }
  virtual void exitPointer(XanatharParser::PointerContext * /*ctx*/) override { }

  virtual void enterIs(XanatharParser::IsContext * /*ctx*/) override { }
  virtual void exitIs(XanatharParser::IsContext * /*ctx*/) override { }

  virtual void enterString(XanatharParser::StringContext * /*ctx*/) override { }
  virtual void exitString(XanatharParser::StringContext * /*ctx*/) override { }

  virtual void enterArrayIndex(XanatharParser::ArrayIndexContext * /*ctx*/) override { }
  virtual void exitArrayIndex(XanatharParser::ArrayIndexContext * /*ctx*/) override { }

  virtual void enterNewCall(XanatharParser::NewCallContext * /*ctx*/) override { }
  virtual void exitNewCall(XanatharParser::NewCallContext * /*ctx*/) override { }

  virtual void enterEqual(XanatharParser::EqualContext * /*ctx*/) override { }
  virtual void exitEqual(XanatharParser::EqualContext * /*ctx*/) override { }

  virtual void enterIncrement(XanatharParser::IncrementContext * /*ctx*/) override { }
  virtual void exitIncrement(XanatharParser::IncrementContext * /*ctx*/) override { }

  virtual void enterGetRegister(XanatharParser::GetRegisterContext * /*ctx*/) override { }
  virtual void exitGetRegister(XanatharParser::GetRegisterContext * /*ctx*/) override { }

  virtual void enterBinaryOr(XanatharParser::BinaryOrContext * /*ctx*/) override { }
  virtual void exitBinaryOr(XanatharParser::BinaryOrContext * /*ctx*/) override { }

  virtual void enterDivision(XanatharParser::DivisionContext * /*ctx*/) override { }
  virtual void exitDivision(XanatharParser::DivisionContext * /*ctx*/) override { }

  virtual void enterFunctionCall(XanatharParser::FunctionCallContext * /*ctx*/) override { }
  virtual void exitFunctionCall(XanatharParser::FunctionCallContext * /*ctx*/) override { }

  virtual void enterLess(XanatharParser::LessContext * /*ctx*/) override { }
  virtual void exitLess(XanatharParser::LessContext * /*ctx*/) override { }

  virtual void enterParentheses(XanatharParser::ParenthesesContext * /*ctx*/) override { }
  virtual void exitParentheses(XanatharParser::ParenthesesContext * /*ctx*/) override { }

  virtual void enterDereference(XanatharParser::DereferenceContext * /*ctx*/) override { }
  virtual void exitDereference(XanatharParser::DereferenceContext * /*ctx*/) override { }

  virtual void enterGenericCall(XanatharParser::GenericCallContext * /*ctx*/) override { }
  virtual void exitGenericCall(XanatharParser::GenericCallContext * /*ctx*/) override { }

  virtual void enterGetTypeOf(XanatharParser::GetTypeOfContext * /*ctx*/) override { }
  virtual void exitGetTypeOf(XanatharParser::GetTypeOfContext * /*ctx*/) override { }

  virtual void enterAddition(XanatharParser::AdditionContext * /*ctx*/) override { }
  virtual void exitAddition(XanatharParser::AdditionContext * /*ctx*/) override { }

  virtual void enterNotEqual(XanatharParser::NotEqualContext * /*ctx*/) override { }
  virtual void exitNotEqual(XanatharParser::NotEqualContext * /*ctx*/) override { }

  virtual void enterDecrement(XanatharParser::DecrementContext * /*ctx*/) override { }
  virtual void exitDecrement(XanatharParser::DecrementContext * /*ctx*/) override { }

  virtual void enterReference(XanatharParser::ReferenceContext * /*ctx*/) override { }
  virtual void exitReference(XanatharParser::ReferenceContext * /*ctx*/) override { }

  virtual void enterModulo(XanatharParser::ModuloContext * /*ctx*/) override { }
  virtual void exitModulo(XanatharParser::ModuloContext * /*ctx*/) override { }

  virtual void enterRawVariable(XanatharParser::RawVariableContext * /*ctx*/) override { }
  virtual void exitRawVariable(XanatharParser::RawVariableContext * /*ctx*/) override { }

  virtual void enterVariablePhrase(XanatharParser::VariablePhraseContext * /*ctx*/) override { }
  virtual void exitVariablePhrase(XanatharParser::VariablePhraseContext * /*ctx*/) override { }

  virtual void enterGreaterOrEqual(XanatharParser::GreaterOrEqualContext * /*ctx*/) override { }
  virtual void exitGreaterOrEqual(XanatharParser::GreaterOrEqualContext * /*ctx*/) override { }

  virtual void enterRange(XanatharParser::RangeContext * /*ctx*/) override { }
  virtual void exitRange(XanatharParser::RangeContext * /*ctx*/) override { }

  virtual void enterNumberConst(XanatharParser::NumberConstContext * /*ctx*/) override { }
  virtual void exitNumberConst(XanatharParser::NumberConstContext * /*ctx*/) override { }

  virtual void enterFloatConst(XanatharParser::FloatConstContext * /*ctx*/) override { }
  virtual void exitFloatConst(XanatharParser::FloatConstContext * /*ctx*/) override { }

  virtual void enterRotateRight(XanatharParser::RotateRightContext * /*ctx*/) override { }
  virtual void exitRotateRight(XanatharParser::RotateRightContext * /*ctx*/) override { }

  virtual void enterCasting(XanatharParser::CastingContext * /*ctx*/) override { }
  virtual void exitCasting(XanatharParser::CastingContext * /*ctx*/) override { }

  virtual void enterSubtraction(XanatharParser::SubtractionContext * /*ctx*/) override { }
  virtual void exitSubtraction(XanatharParser::SubtractionContext * /*ctx*/) override { }

  virtual void enterGreater(XanatharParser::GreaterContext * /*ctx*/) override { }
  virtual void exitGreater(XanatharParser::GreaterContext * /*ctx*/) override { }

  virtual void enterSpec(XanatharParser::SpecContext * /*ctx*/) override { }
  virtual void exitSpec(XanatharParser::SpecContext * /*ctx*/) override { }

  virtual void enterRotateLeft(XanatharParser::RotateLeftContext * /*ctx*/) override { }
  virtual void exitRotateLeft(XanatharParser::RotateLeftContext * /*ctx*/) override { }

  virtual void enterPhrase(XanatharParser::PhraseContext * /*ctx*/) override { }
  virtual void exitPhrase(XanatharParser::PhraseContext * /*ctx*/) override { }

  virtual void enterStart(XanatharParser::StartContext * /*ctx*/) override { }
  virtual void exitStart(XanatharParser::StartContext * /*ctx*/) override { }

  virtual void enterAsm_(XanatharParser::Asm_Context * /*ctx*/) override { }
  virtual void exitAsm_(XanatharParser::Asm_Context * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

