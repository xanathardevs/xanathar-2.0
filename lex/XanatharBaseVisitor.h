
// Generated from Xanathar.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "XanatharVisitor.h"


/**
 * This class provides an empty implementation of XanatharVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  XanatharBaseVisitor : public XanatharVisitor {
public:

  virtual antlrcpp::Any visitPrep(XanatharParser::PrepContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitString_inner(XanatharParser::String_innerContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEscaped_string(XanatharParser::Escaped_stringContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLetter(XanatharParser::LetterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCname(XanatharParser::CnameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitWord(XanatharParser::WordContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStr(XanatharParser::StrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCast(XanatharParser::CastContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUse_statement(XanatharParser::Use_statementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclare_statement(XanatharParser::Declare_statementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariable_phrase(XanatharParser::Variable_phraseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRaw_var(XanatharParser::Raw_varContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAs_phrase(XanatharParser::As_phraseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypespec(XanatharParser::TypespecContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariable_setting(XanatharParser::Variable_settingContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLet(XanatharParser::LetContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass_(XanatharParser::Class_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass__specs(XanatharParser::Class__specsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExtends_(XanatharParser::Extends_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInterface_(XanatharParser::Interface_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInterface_fn(XanatharParser::Interface_fnContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclared_var(XanatharParser::Declared_varContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypes(XanatharParser::TypesContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInheritance(XanatharParser::InheritanceContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFncall(XanatharParser::FncallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGeneric_call(XanatharParser::Generic_callContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGp(XanatharParser::GpContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFn(XanatharParser::FnContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitHook(XanatharParser::HookContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTemplate_(XanatharParser::Template_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass_es(XanatharParser::Class_esContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClass__(XanatharParser::Class__Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVirtual_fn(XanatharParser::Virtual_fnContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitOverride(XanatharParser::OverrideContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitOverride_name(XanatharParser::Override_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitOverride_param(XanatharParser::Override_paramContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLambda(XanatharParser::LambdaContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParams(XanatharParser::ParamsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIf_(XanatharParser::If_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitElse_(XanatharParser::Else_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFor_(XanatharParser::For_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitWhile_(XanatharParser::While_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRet(XanatharParser::RetContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMatch_(XanatharParser::Match_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMatch_arm(XanatharParser::Match_armContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDefault_arm(XanatharParser::Default_armContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRef(XanatharParser::RefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPtr(XanatharParser::PtrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeref(XanatharParser::DerefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNumber(XanatharParser::NumberContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloat_(XanatharParser::Float_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBreak_(XanatharParser::Break_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSkip(XanatharParser::SkipContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNew_(XanatharParser::New_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCall_sealed_class_(XanatharParser::Call_sealed_class_Context *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLock(XanatharParser::LockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUnlock(XanatharParser::UnlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInc(XanatharParser::IncContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDec(XanatharParser::DecContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitReg(XanatharParser::RegContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitReg_set(XanatharParser::Reg_setContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryNot(XanatharParser::BinaryNotContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLambdaCreation(XanatharParser::LambdaCreationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLessOrEqual(XanatharParser::LessOrEqualContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMultiplication(XanatharParser::MultiplicationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryAnd(XanatharParser::BinaryAndContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBareword(XanatharParser::BarewordContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCallSealedClass(XanatharParser::CallSealedClassContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryXor(XanatharParser::BinaryXorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGetSizeOf(XanatharParser::GetSizeOfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPointer(XanatharParser::PointerContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIs(XanatharParser::IsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitString(XanatharParser::StringContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitArrayIndex(XanatharParser::ArrayIndexContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNewCall(XanatharParser::NewCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEqual(XanatharParser::EqualContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIncrement(XanatharParser::IncrementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGetRegister(XanatharParser::GetRegisterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryOr(XanatharParser::BinaryOrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDivision(XanatharParser::DivisionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunctionCall(XanatharParser::FunctionCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLess(XanatharParser::LessContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParentheses(XanatharParser::ParenthesesContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDereference(XanatharParser::DereferenceContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGenericCall(XanatharParser::GenericCallContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGetTypeOf(XanatharParser::GetTypeOfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAddition(XanatharParser::AdditionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNotEqual(XanatharParser::NotEqualContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDecrement(XanatharParser::DecrementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitReference(XanatharParser::ReferenceContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitModulo(XanatharParser::ModuloContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRawVariable(XanatharParser::RawVariableContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariablePhrase(XanatharParser::VariablePhraseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGreaterOrEqual(XanatharParser::GreaterOrEqualContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRange(XanatharParser::RangeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNumberConst(XanatharParser::NumberConstContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloatConst(XanatharParser::FloatConstContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRotateRight(XanatharParser::RotateRightContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCasting(XanatharParser::CastingContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSubtraction(XanatharParser::SubtractionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGreater(XanatharParser::GreaterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSpec(XanatharParser::SpecContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRotateLeft(XanatharParser::RotateLeftContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPhrase(XanatharParser::PhraseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStart(XanatharParser::StartContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAsm_(XanatharParser::Asm_Context *ctx) override {
    return visitChildren(ctx);
  }


};

