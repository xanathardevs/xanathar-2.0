//
// Created by proc-daemon on 1/16/19.
//

#ifndef XANATHAR_2_0_NODE_H
#define XANATHAR_2_0_NODE_H

#include<string>
#include<vector>
#include<llvm/IR/Value.h>
#include<llvm/IR/GlobalValue.h>
#include "../../irgen/ir.h"

namespace xanathar{
    namespace ast{
        class node {
        public:
            std::string data;
            std::vector<node*> children;
            virtual llvm::Value* parse() = 0;

            explicit node(std::string data, std::initializer_list<node*> i) : data(data), children(i){}
        };

        class leaf : public node {
        public:
            explicit leaf(std::string data) : node(data, {}) {
//                this->data = data;
//                this->children = {};
            }
        };

        class string_leaf : public leaf {
        public:
            explicit string_leaf(std::string d): leaf(d){}
            llvm::Value* parse() override {
                return new llvm::GlobalVariable(llvm::PointerType::get(llvm::IntegerType::get(ir::xContext, 8), 0), true,
                        llvm::GlobalValue::LinkageTypes::InternalLinkage,
                        llvm::ConstantDataArray::getString(ir::xContext, this->data));
            }
        };
    }
}

#endif //XANATHAR_2_0_NODE_H
