grammar Xanathar;


WS : [ \t\r\n]+ -> channel(HIDDEN);

COMMENT: '#' ~( '\r' | '\n' )* -> channel(HIDDEN);

prep : '!=!' .+? '!=!';
DIGIT : [0-9];

//%ignore PREPROCESS;
LCASELETT : [a-z];
UCASELET : [A-Z];
string_inner : ~'"'|'\\';
escaped_string : '"' string_inner* '"';
letter: UCASELET | LCASELETT;
cname: ('_' | letter) ('_' | letter | DIGIT)*;
word: cname;
str : escaped_string;
cast : '[' expression as_phrase ']';

use_statement : 'use' word ;
declare_statement : 'declare' variable_phrase as_phrase ('(' expression ')')? ;
//modifiers : ('locked' | 'mut')*;
variable_phrase : '$' word;
raw_var : '@' word;
as_phrase : 'as' (typespec|word) ('[' number ']')*;
typespec : ('*' | '&')+ word;
variable_setting : (variable_phrase | ref) ('(' expression ')')? '=' (expression|fncall) ;
let : 'let' variable_phrase '=' expression ;

class_ : class__specs 'class_' word '<' types '>' inheritance (':' inheritance)? '{' (phrase*) '}';
class__specs : ('static' | 'sealed') *;
extends_ : 'extends' word '{' (phrase*) '}';

interface_ : 'interface' word '{' (interface_fn)* '}';
interface_fn : 'fn' word '[' (typespec ',')* (typespec)? ']' as_phrase ;
declared_var : variable_phrase as_phrase;
types : ((declared_var',')* declared_var)?;
inheritance : (('(' (word ',')* word ')'))?;

// fncall : varspec '[' (expression ',')* [expression] ']';
//spec : (raw_var | variable_phrase | ref | expression) '.' word;
fncall : expression '[' (expression ',')* (expression)? ']';
generic_call : expression '<' gp '>' '[' (expression ',')* (expression)? ']';
gp : ((typespec|word) ',')* (typespec|word);
fn : 'fn' word '[' params ']' as_phrase '{' ( phrase )* '}';
hook : 'hook' OP '[' params ']' (as_phrase)? '{' ( phrase )* '}';
OP : '+' | '-' | '*' | '/' | '%' | '|' | '&';
template_ : 'template' '<' class_es '>' word '[' params ']' as_phrase '{' (phrase)* '}';
class_es : (class__ ',')* class__;
class__ : 'class' word;
virtual_fn : 'virtual' word '[' (typespec ',')* (typespec)? ']' 'as' (typespec|word) ;
override : 'override' '@' override_name '<' override_param '>' '{' (phrase)* '}';
override_name : (word '->')+ word;
override_param : (variable_phrase ',')* (variable_phrase)? ;

lambda : 'λ' params '->' (expression|('{' (phrase)* '}')) ;

params : ( variable_phrase as_phrase ',' )* (variable_phrase as_phrase);
// if_ : 'if_' '[' ( expression ) ']' '{' ( phrase )* '}' ('else_' if_)* ['else_' '{' ( phrase )* '}'];
if_ : 'if' '[' ( expression ) ']' '{' ( phrase )* '}' (else_)?;
else_ : 'else' '{' ( phrase )* '}';
// for_ : 'for_' '[' variable_phrase 'is' expression ']' '{' (phrase)* '}';
for_ : 'for' '[' phrase expression ';' phrase ']' '{' (phrase)* '}';
while_ : 'while' '[' expression ']' '{' (phrase)* '}';
ret : 'ret' (expression)? ;
match_ : 'match' '[' expression ']' '{' (match_arm ',')* (match_arm|default_arm)? ','? '}';
match_arm : expression '=>' phrase;
default_arm : 'default' '=>' phrase;

ref : (variable_phrase|raw_var) ('->' (variable_phrase|raw_var))+;

ptr : '&' expression;
deref : '*' expression;

number : ('+' | '-')? DIGIT+;
float_ : number '.' number;

break_ : 'break' ;
skip : 'skip' ;

new_ : 'new_' expression 'as' fncall;

call_sealed_class_ : (word '::')+ fncall;

lock : 'lock' variable_phrase;
unlock : 'unlock' variable_phrase;

inc : expression '++';
dec : expression '--';

reg : '%' word;
reg_set : reg '<-' expression;

expression :
        word                                                                                    # Bareword
        | ('(' expression ')')                                                                  # Parentheses
        | reg                                                                                   # GetRegister
        | expression '++'                                                                       # Increment
        | expression '--'                                                                       # Decrement
        | 'sizeof' '@' (typespec|word)                                                          # GetSizeOf
        | 'typeof' '@' expression                                                               # GetTypeOf
        | ref                                                                                   # Reference
        | cast                                                                                  # Casting
        | ptr                                                                                   # Pointer
        | expression '(' expression ')'                                                         # ArrayIndex
        | deref                                                                                 # Dereference
        | number                                                                                # NumberConst
        | float_                                                                                # FloatConst
        | str                                                                                   # String
        | variable_phrase                                                                       # VariablePhrase
        | raw_var                                                                               # RawVariable
        | expression '+' expression                                                             # Addition
        | expression '-' expression                                                             # Subtraction
        | expression '*' expression                                                             # Multiplication
        | expression '/' expression                                                             # Division
        | expression '&' expression                                                             # BinaryAnd
        | expression '|' expression                                                             # BinaryOr
        | expression '^' expression                                                             # BinaryXor
        | expression '%' expression                                                             # Modulo
        | expression '[' (expression ',')* (expression)? ']'                                    # FunctionCall
        | expression '<' gp '>' '[' (expression ',')* (expression)? ']'                         # GenericCall
        | 'λ' params '->' (expression|('{' (phrase)* '}'))                                      # LambdaCreation
        | (word '::')+ word '[' (expression ',')* (expression)? ']'                             # CallSealedClass
        | expression '..' expression                                                            # Range
        | expression 'is' expression                                                            # Is
        | expression '<<' expression                                                            # RotateLeft
        | expression '>>' expression                                                            # RotateRight
        | expression '<' expression                                                             # Less
        | expression '>' expression                                                             # Greater
        | expression '<=' expression                                                            # LessOrEqual
        | expression '>=' expression                                                            # GreaterOrEqual
        | expression '!=' expression                                                            # NotEqual
        | expression '==' expression                                                            # Equal
        | expression '.' (word '.')* word                                                       # Spec
        | '!' expression                                                                        # BinaryNot
        | 'new' expression 'as' expression '[' (expression ',')* (expression)? ']'              # NewCall
        ;
phrase : ((expression
            | reg_set
            | prep
            | inc
            | dec
            | asm_
            | use_statement
            | declare_statement
            | template_
            | lambda
//            | as_phrase
            | ret
            | lock
            | unlock
            | variable_setting
            | let
            | break_
            | skip
            | new_
            ) ';') | (
            if_
            | fn
            | hook
            | virtual_fn
            | match_
            | while_
            | for_
            | class_
            | interface_
            | override
            | extends_
            );
start: phrase*;

asm_ : '__asm__' .+? '__end-asm__';
