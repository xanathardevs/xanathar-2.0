#include "lex.h"

namespace xanathar {
    namespace lex {
        void lex(const std::string &file, const options::xArgs &args) {
            // Yes, this was originally intended as the lexer-parser handler.
            // However, it evolved into the "Lexer, EXpression parser, and code generator" (LEX)
            // :)

            std::ifstream stream;
            stream.open(file);

            antlr4::ANTLRInputStream input(stream);
            XanatharLexer lexer(&input);
            antlr4::CommonTokenStream tokens(&lexer);
            XanatharParser parser(&tokens);

            XanatharParser::StartContext *start = parser.start();
            ir::NameMap locals{

            };
            ir::NameMap globals{

            };

            for (auto i : start->phrase()) {
                ir::gen::parsePhrase(i, ir::xBuilder, locals, globals);
            }

//            ir::xFPM->doFinalization(); // Needed?

//            antlr4::tree::ParseTree *tree = parser.start();
//            IRWalker visitor;
//            visitor.visit(tree);
//            IRWalker listener;
//            antlr4::tree::ParseTreeWalker::DEFAULT.walk(&listener, tree);

            stream.close();
        }
    }
}
