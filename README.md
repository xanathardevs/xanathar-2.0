# Xanathar 2.0
This is a rewrite of Xanathar in C++ (Xanathar 1.0 is in Python). 2.0 uses ANTLR4 (LL(*)) + LLVM 7.0, wheras 1.0 uses LARK (PEG) + LLVMlite 0.27.0 (LLVM 7.0). As such, the grammar has been revised. 

### Dependencies

+ ANTLR4CPP runtime (installed in /usr/lib/, not /usr/local/lib/)
+ LLVM-7.0 (installed in /usr/lib/)

### How to install

Just use `cmake`. Only one target is available.