//
// Created by proc-daemon on 1/15/19.
//

#ifndef XANATHAR_2_0_OPTIONS_H
#define XANATHAR_2_0_OPTIONS_H
#include<string>
#include "argparse.hpp"

namespace xanathar {
    namespace options {

        struct xArgs {
            // variables
            std::string out;
            int static_int_alloc;
            // flags
            bool shared;
            bool print_ast;
            bool debug;

            std::string file;
        };

        xArgs accumulateArgs(size_t, const char **);
    }
}


#endif //XANATHAR_2_0_OPTIONS_H
