//
// Created by proc-daemon on 1/15/19.
//

#include "options.h"

namespace xanathar {
    namespace options {
        xArgs accumulateArgs(size_t argc, const char **argv) {
            // make a new ArgumentParser
            ArgumentParser parser;

            // name
            parser.appName("Xanathar");

            // variables
            parser.addArgument("-o", "--out", 1);
            parser.addArgument("-s", "--static-int-alloc", 1);
            // flags
            parser.addArgument("-h", "--shared");
            parser.addArgument("-a", "--ast");
            parser.addArgument("-g", "--debug");
            // file
            parser.addFinalArgument("file");

            parser.parse(argc, argv);

            return xArgs{
                    parser.exists("out") && !parser.retrieve<std::string>("out").empty()
                                            ? parser.retrieve<std::string>("out") : "a.ll",
                    parser.exists("static-int-alloc") && !parser.retrieve<int>("static-int-alloc")
                                            ? parser.retrieve<int>("static-int-alloc") : 32,

                    parser.exists("shared"),
                    parser.exists("ast"),
                    parser.exists("debug"),

                    parser.retrieve<std::string>("file")
            };
        }
    }
}