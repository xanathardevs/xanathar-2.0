#include <fstream>
#include "irgen/ir.h"
#include "options/options.h"
#include "lex/lex.h"

int main (int argc, const char** argv) {
    xanathar::ir::init_irgen();
    xanathar::options::xArgs opts = xanathar::options::accumulateArgs((size_t)argc, argv);

    std::string data;
    llvm::raw_string_ostream rso(data);

    xanathar::lex::lex(opts.file, opts);
    xanathar::ir::dump(rso);

    std::ofstream out(opts.out);
    out << data;
    out.close();
}
