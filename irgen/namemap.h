//
// Created by proc-daemon on 1/16/19.
//

#ifndef XANATHAR_2_0_NAMEMAP_H
#define XANATHAR_2_0_NAMEMAP_H

namespace xanathar{
    namespace ir{
        class Name {
        public:
            std::string name;
            llvm::Type* ty;
            llvm::Value* val;

            virtual llvm::Value* get() {
                return this->val;
            }

            virtual Name* as_rvalue(llvm::IRBuilder<>) {
                return this;
            }

            Name(std::string name, llvm::Type* ty, llvm::Value* val) : name(name), ty(ty), val(val) {}

            Name(const Name& n) = default;

            Name() : name(""), ty(nullptr), val(nullptr) {}
        };

        class lvalue : public Name {
        public:
            virtual llvm::Value* get() override {
                return this->val;
            }

            virtual Name* as_rvalue(llvm::IRBuilder<> b) override {
                return new Name (
                    this->name,
                    this->ty->getPointerElementType(),
                    b.CreateLoad(this->val)
                );
            }
        };

        typedef std::map<std::string, Name> NameMap;
    }
}

#endif //XANATHAR_2_0_NAMEMAP_H
