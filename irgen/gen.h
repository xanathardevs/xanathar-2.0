//
// Created by proc-daemon on 1/15/19.
//

#ifndef XANATHAR_2_0_GEN_H
#define XANATHAR_2_0_GEN_H

#include "../lex/XanatharParser.h"
#include "../lex/XanatharBaseVisitor.h"
#include "ir.h"
#include "builders/builders.h"
#include "namemap.h"

namespace xanathar {
    namespace ir {
        namespace gen {
            class invalid_op : public std::exception {
            public:
                invalid_op() = default;

                const char* what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_USE_NOEXCEPT override {
                    return "Invalid operator!";
                }
            };

            llvm::Value* parsePhrase(XanatharParser::PhraseContext *ctx, const llvm::IRBuilder<>& builder,
                    NameMap& locals, NameMap& globals);
            llvm::Value* parseExpression(XanatharParser::ExpressionContext *ctx, llvm::IRBuilder<> builder,
                    NameMap& locals, NameMap& globals);
        }
    }
}

#endif //XANATHAR_2_0_GEN_H
