//
// Created by proc-daemon on 1/16/19.
//

#ifndef XANATHAR_2_0_BUILDERS_H
#define XANATHAR_2_0_BUILDERS_H

#include "asm_.h"
#include "break_.h"
#include "class_.h"
#include "dec.h"
#include "declare_statement.h"
#include "extends_.h"
#include "fn.h"
#include "for_.h"
#include "hook.h"
#include "if_.h"
#include "inc.h"
#include "interface_.h"
#include "lambda.h"
#include "let.h"
#include "lock.h"
#include "match_.h"
#include "new_.h"
#include "override.h"
#include "prep.h"
#include "reg_set.h"
#include "ret.h"
#include "skip.h"
#include "template_.h"
#include "unlock.h"
#include "use_statement.h"
#include "variable_setting.h"
#include "virtual_fn.h"
#include "while_.h"
#include "expression.h"


#include "value.h"

namespace xanathar{
    namespace ir{
        namespace builders{

        }
    }
}

#endif //XANATHAR_2_0_BUILDERS_H
