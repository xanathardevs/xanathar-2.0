#include "virtual_fn.h"
namespace xanathar {
    namespace ir {
        namespace builders {
            llvm::Value* virtual_fn(XanatharParser::Virtual_fnContext* ctx, llvm::IRBuilder<> builder, ir::NameMap locals, ir::NameMap globals) {
                return nullptr;
            }
        }
    }
}
