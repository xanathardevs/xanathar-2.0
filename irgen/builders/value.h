//
// Created by proc-daemon on 1/16/19.
//

#ifndef XANATHAR_2_0_VALUE_H
#define XANATHAR_2_0_VALUE_H


#include <llvm/IR/Value.h>

namespace xanathar {
    namespace ir {
        namespace value {
            class value {
            public:
                llvm::Value* v;
                explicit value() = default;
                explicit value(llvm::Value* v) {
                    this->v = v;
                }
//                llvm::Value val;
            };

            class rvalue : public value {

            };

            class lvalue : public rvalue {

            };
        }
    }
}


#endif //XANATHAR_2_0_VALUE_H
