#include "ret.h"
namespace xanathar {
    namespace ir {
        namespace builders {
            llvm::Value* ret(XanatharParser::RetContext* ctx, llvm::IRBuilder<> builder, ir::NameMap locals, ir::NameMap globals) {
                return builder.CreateRet(ir::gen::parseExpression(ctx->expression(), builder, globals, locals));
            }
        }
    }
}
