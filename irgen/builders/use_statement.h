// AUTO-GENERATED BY scripts.py -- DO NOT CHANGE

#ifndef XANATHAR_2_0_USE_STATEMENT_H
#define XANATHAR_2_0_USE_STATEMENT_H

#include "../../lex/XanatharParser.h"
#include "value.h"
#include <llvm/IR/IRBuilder.h>

#include "../ir-util.h"
#include "../ir.h"
#include "../gen.h"
#include "../namemap.h"

namespace xanathar {
    namespace ir {
        namespace builders {
            llvm::Value* use_statement(XanatharParser::Use_statementContext*, llvm::IRBuilder<>, ir::NameMap, ir::NameMap);
        }
    }
}

#endif //XANATHAR_2_0_USE_STATEMENT_H