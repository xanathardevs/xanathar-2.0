#include "skip.h"
namespace xanathar {
    namespace ir {
        namespace builders {
            llvm::Value* skip(XanatharParser::SkipContext* ctx, llvm::IRBuilder<> builder, ir::NameMap locals, ir::NameMap globals) {
                return nullptr;
            }
        }
    }
}
