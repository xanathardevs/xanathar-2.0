#include "fn.h"

namespace xanathar {
    namespace ir {
        namespace builders {
//            static llvm::FunctionType* to_fn_ty(XanatharParser::FnContext* ctx) {
//                llvm::Type* ret = utils::to_type(ctx->as_phrase());
//                std::vector<llvm::Type*> params;
//                for(auto i : ctx->params()->as_phrase()) {
//                    params.push_back(utils::to_type(i));
//                }
//                return llvm::FunctionType::get(ret, params, false);
//            } --> moved to |
//                           v

            llvm::Value* fn(XanatharParser::FnContext* ctx, llvm::IRBuilder<> builder,
                    ir::NameMap locals, ir::NameMap globals) {
                llvm::Type* ret = utils::to_type(ctx->as_phrase());
                std::vector<llvm::Type*> params;
                for(auto i : ctx->params()->as_phrase()) {
                    params.push_back(utils::to_type(i));
                }
                auto ty =  llvm::FunctionType::get(ret, params, false);

                // Insert function
                auto fn = llvm::cast<llvm::Function>(ir::xModule->getOrInsertFunction(utils::to_word(ctx->word()), ty));

                fn->setCallingConv(llvm::CallingConv::C); // Set call conv.
                llvm::BasicBlock* block = llvm::BasicBlock::Create(ir::xContext, "entry", fn); // Create new block
                llvm::IRBuilder<> fnBuilder = llvm::IRBuilder<>(block); // Make new block

                ir::NameMap __fn_locals{

                };

                size_t j = 0;
                std::string name;
                for(auto i = fn->arg_begin(); i < fn->arg_end(); i++) {
                    name = utils::to_word(ctx->params()->variable_phrase(j));
                    i->setName(name);
                    __fn_locals[name] = ir::Name{
                        name,
                        params[j],
                        i
                    };
                    j++;
                }

                for(auto i : ctx->phrase()) {
                    ir::gen::parsePhrase(i, fnBuilder, __fn_locals, globals); // Parse phrases with new builder
                }

                ir::xFPM->run(*fn);

                return fn;
            }
        }
    }
}
