// #include "../include/KaleidoscopeJIT.h"
#include "ir.h"

namespace xanathar {
    namespace ir {

//===----------------------------------------------------------------------===//
// Code Generation
//===----------------------------------------------------------------------===//

        llvm::LLVMContext xContext;
        llvm::IRBuilder<> xBuilder(xContext);
        std::unique_ptr<llvm::Module> xModule;
        std::unique_ptr<llvm::legacy::FunctionPassManager> xFPM;

//===----------------------------------------------------------------------===//
// Top-Level parsing and JIT Driver
//===----------------------------------------------------------------------===//

        static void InitializeModuleAndPassManager() {
            // Open a new module.
            xModule = llvm::make_unique<llvm::Module>("xanathar", xContext);

            // Create a new pass manager attached to it.
            xFPM = llvm::make_unique<llvm::legacy::FunctionPassManager>(xModule.get());

            // Do simple "peephole" optimizations and bit-twiddling optzns.
            xFPM->add(llvm::createInstructionCombiningPass());
            // Reassociate expressions.
            xFPM->add(llvm::createReassociatePass());
            // Eliminate Common SubExpressions.
            xFPM->add(llvm::createGVNPass());
            // Simplify the control flow graph (deleting unreachable blocks, etc).
            xFPM->add(llvm::createCFGSimplificationPass());

            xFPM->doInitialization();
        }

//llvm::Function* CreateFunction(const std::string& name, llvm::FunctionType* ty) {
//    auto fn = llvm::cast<llvm::Function>(xModule->getOrInsertFunction(name, ty)); // Insert function
//
//    fn->setCallingConv(llvm::CallingConv::C); // Set call conv.
//
//    return fn;
//}

/*
 * To make block:
 * llvm::BasicBlock* block = llvm::BasicBlock::Create(xContext, "entry", fn);
 *
 * Set insert point:
 * xBuilder.SetInsertPoint(block);
 */

        bool OptimizeFunction(llvm::Function f) {
            return xFPM->run(f);
        }

//===----------------------------------------------------------------------===//
// Main driver code.
//===----------------------------------------------------------------------===//

        int init_irgen() {
            llvm::InitializeNativeTarget();
            llvm::InitializeNativeTargetAsmPrinter();
            llvm::InitializeNativeTargetAsmParser();

            InitializeModuleAndPassManager();

//    InitializeMainBlock();

//    xBuilder.CreateRet(llvm::Constant::getIntegerValue(llvm::IntegerType::get(xContext, 32), llvm::APInt(32, 32)));

            return 0;
        }

        void dump(llvm::raw_ostream &out) {
            xModule->print(out, nullptr);
        }
    }
}