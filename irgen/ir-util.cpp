//
// Created by proc-daemon on 1/16/19.
//

#include <llvm/IR/DerivedTypes.h>
#include "ir-util.h"
#include "ir.h"

namespace xanathar{
    namespace ir{
        namespace utils {
            std::map<std::string, llvm::Type*> base_types {
                    {"char", llvm::IntegerType::get(xContext, 8)},
                    {"int1", llvm::IntegerType::get(xContext, 1)},
                    {"int2", llvm::IntegerType::get(xContext, 2)},
                    {"int4", llvm::IntegerType::get(xContext, 4)},
                    {"int8", llvm::IntegerType::get(xContext, 8)},
                    {"int16", llvm::IntegerType::get(xContext, 16)},
                    {"int32", llvm::IntegerType::get(xContext, 32)},
                    {"int64", llvm::IntegerType::get(xContext, 64)},
                    {"int128", llvm::IntegerType::get(xContext, 128)},
                    {"int256", llvm::IntegerType::get(xContext, 256)},
                    {"int512", llvm::IntegerType::get(xContext, 512)},
                    {"int1024", llvm::IntegerType::get(xContext, 1024)},
            };

            std::string to_word(antlr4::ParserRuleContext *ctx) {
                std::string buf;
                std::vector<antlr4::tree::ParseTree *> p(ctx->children);
                for (auto i : p) {
                    buf += i->getText();
                    for (auto j : i->children) {
                        p.push_back(j);
                    }
                }

                return buf;
            }

            llvm::Type* get_base_type(const std::string& ty) {
                return base_types[ty];
            }

            llvm::Type* to_type(antlr4::ParserRuleContext *ctx) {
                std::string stringified = to_word(ctx).substr(2);
                std::string base;
                int ptr_lvl = 0;

                for(char c : stringified) {
                    if (c == '*') {
                        ptr_lvl++;
                    } else {
                        base += c;
                    }
                }

                llvm::Type* base_type = get_base_type(base);
                for(int i = 0; i < ptr_lvl; i++){
                    base_type = llvm::PointerType::get(base_type, 0);
                }

                return base_type;
            }
        }
    }
}
