//
// Created by proc-daemon on 1/15/19.
//

#ifndef XANATHAR_2_0_IR_H
#define XANATHAR_2_0_IR_H
#include "llvm/ADT/APFloat.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"

#include <string>


namespace xanathar {
    namespace ir {
        extern llvm::IRBuilder<> xBuilder;
        extern std::unique_ptr<llvm::Module> xModule;
        extern llvm::LLVMContext xContext;
        extern std::unique_ptr<llvm::legacy::FunctionPassManager> xFPM;

        int init_irgen();
        void dump(llvm::raw_ostream&);
    }
}

#endif //XANATHAR_2_0_IR_H
