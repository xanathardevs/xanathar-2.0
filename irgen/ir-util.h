//
// Created by proc-daemon on 1/16/19.
//

#ifndef XANATHAR_2_0_IR_UTIL_H
#define XANATHAR_2_0_IR_UTIL_H

#include <llvm/IR/Type.h>
#include "../lex/XanatharParser.h"

namespace xanathar{
    namespace ir{
        namespace utils{
            std::string to_word(antlr4::ParserRuleContext*);
            llvm::Type* to_type(antlr4::ParserRuleContext*);
        }
    }
}

#endif //XANATHAR_2_0_IR_UTIL_H
