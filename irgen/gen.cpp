//
// Created by proc-daemon on 1/15/19.
//

#include "gen.h"

namespace xanathar {
    namespace ir {
        namespace gen {
            llvm::Value* parsePhrase(XanatharParser::PhraseContext *ctx, const llvm::IRBuilder<>& builder,
                                     NameMap& locals, NameMap& globals) {
                if (ctx->asm_()){
                    return builders::asm_(ctx->asm_(), builder, locals, globals);
                } else if (ctx->break_()){
                    return builders::break_(ctx->break_(), builder, locals, globals);
                } else if (ctx->class_()){
                    return builders::class_(ctx->class_(), builder, locals, globals);
                } else if (ctx->dec()){
                    return builders::dec(ctx->dec(), builder, locals, globals);
                } else if (ctx->declare_statement()){
                    return builders::declare_statement(ctx->declare_statement(), builder, locals, globals);
                } else if (ctx->extends_()){
                    return builders::extends_(ctx->extends_(), builder, locals, globals);
                } else if (ctx->fn()){
                    return builders::fn(ctx->fn(), builder, locals, globals);
                } else if (ctx->for_()){
                    return builders::for_(ctx->for_(), builder, locals, globals);
                } else if (ctx->hook()){
                    return builders::hook(ctx->hook(), builder, locals, globals);
                } else if (ctx->if_()){
                    return builders::if_(ctx->if_(), builder, locals, globals);
                } else if (ctx->inc()){
                    return builders::inc(ctx->inc(), builder, locals, globals);
                } else if (ctx->interface_()){
                    return builders::interface_(ctx->interface_(), builder, locals, globals);
                } else if (ctx->lambda()){
                    return builders::lambda(ctx->lambda(), builder, locals, globals);
                } else if (ctx->let()){
                    return builders::let(ctx->let(), builder, locals, globals);
                } else if (ctx->lock()){
                    return builders::lock(ctx->lock(), builder, locals, globals);
                } else if (ctx->match_()){
                    return builders::match_(ctx->match_(), builder, locals, globals);
                } else if (ctx->new_()){
                    return builders::new_(ctx->new_(), builder, locals, globals);
                } else if (ctx->override()){
                    return builders::override(ctx->override(), builder, locals, globals);
                } else if (ctx->prep()){
                    return builders::prep(ctx->prep(), builder, locals, globals);
                } else if (ctx->reg_set()){
                    return builders::reg_set(ctx->reg_set(), builder, locals, globals);
                } else if (ctx->ret()){
                    return builders::ret(ctx->ret(), builder, locals, globals);
                } else if (ctx->skip()){
                    return builders::skip(ctx->skip(), builder, locals, globals);
                } else if (ctx->template_()){
                    return builders::template_(ctx->template_(), builder, locals, globals);
                } else if (ctx->unlock()){
                    return builders::unlock(ctx->unlock(), builder, locals, globals);
                } else if (ctx->use_statement()){
                    return builders::use_statement(ctx->use_statement(), builder, locals, globals);
                } else if (ctx->variable_setting()){
                    return builders::variable_setting(ctx->variable_setting(), builder, locals, globals);
                } else if (ctx->virtual_fn()){
                    return builders::virtual_fn(ctx->virtual_fn(), builder, locals, globals);
                } else if (ctx->while_()){
                    return builders::while_(ctx->while_(), builder, locals, globals);
                } else if (ctx->expression()){
                    return builders::expression(ctx->expression(), builder, locals, globals);
                } else {
                    throw invalid_op();
                }
            }

            class ExprWalker : public XanatharBaseVisitor {
            private:
                llvm::IRBuilder<>* b;
                ir::NameMap locals;
                ir::NameMap globals;

                template<class T>
                llvm::Value* binop(llvm::Value* (llvm::IRBuilder<>::* f)(llvm::Value*, llvm::Value*, const llvm::Twine&), T* ctx) {
                    return ((this->b)->*f)(this->visit(ctx->expression()[0]), this->visit(ctx->expression()[1]), "");
                    // turn this->visit into (this->visit(...).to_rvalue().get())
                } // This is the most horrific function I have ever written in my life

                ir::Name get_var(std::string name) {
                    auto it = this->locals.find(name);
                    if(it != this->locals.end()) {
                        return it->second;
                    }

                    auto git = this->globals.find(name);
                    if(git != this->globals.end()) {
                        return it->second;
                    }

                    std::cerr << "Could not find $" << name << std::endl;
                    exit(0);
                    // return nullptr;
                    // TODO implement
                }
            public:
                explicit ExprWalker(llvm::IRBuilder<>* b, ir::NameMap locals, ir::NameMap globals) : b(b), locals(locals), globals(globals) {}

                virtual antlrcpp::Any visitAddition(XanatharParser::AdditionContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateNSWAdd, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitSubtraction(XanatharParser::SubtractionContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateNSWSub, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitMultiplication(XanatharParser::MultiplicationContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateNSWMul, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitDivision(XanatharParser::DivisionContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateExactSDiv, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitModulo(XanatharParser::ModuloContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateSRem, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitBinaryOr(XanatharParser::BinaryOrContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateOr, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitBinaryXor(XanatharParser::BinaryXorContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateXor, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitBinaryAnd(XanatharParser::BinaryAndContext* ctx) override {
                    llvm::Value* v = this->binop(&llvm::IRBuilder<>::CreateAnd, ctx); // SIGNED INTEGER
                    return v;
                }

                virtual antlrcpp::Any visitParentheses(XanatharParser::ParenthesesContext* ctx) override {
                    return this->visit(ctx->expression());
                }

                virtual antlrcpp::Any visitNumberConst(XanatharParser::NumberConstContext *ctx) override {
                    llvm::Value* v = llvm::ConstantInt::getSigned(llvm::IntegerType::get(ir::xContext, 32),
                                                                  std::stoi(utils::to_word(ctx)));
                    return v;
                }

                virtual antlrcpp::Any visitVariablePhrase(XanatharParser::VariablePhraseContext *ctx) override {
                    ir::Name named_var = get_var(utils::to_word(ctx->variable_phrase()));
                    llvm::Value* var = named_var.as_rvalue(*this->b)->get(); // TODO: figure out how to pass names down the parse chain
                                                                            //  in order to do (e.g.) BinOp: this->visit(left).as_rvalue().get() + this->visit(right)...
                    return var;
                    // return this->b->CreateLoad(), "");
                    // TO|DO: lvalue/rvalue
                    /*
                     * example:
                     * return get_var(...).get();
                     *
                     *
                     * in name:
                     * virtual get() = 0;
                     * to_rvalue() = 0;
                     *
                     * rvalue : name
                     * get -> data
                     * to_rvalue() -> self
                     *
                     * lvalue : name
                     * get -> data (ptr to val)
                     * to_rvalue() -> *data
                     */
                    // return nullptr;
                }
            };

            llvm::Value* parseExpression(XanatharParser::ExpressionContext* ctx, llvm::IRBuilder<> builder,
                                         NameMap& globals, NameMap& locals) {
                /*
                 * to parse strings:
                 * return new llvm::GlobalVariable(llvm::PointerType::get(llvm::IntegerType::get(ir::xContext, 8), 0), true,
                        llvm::GlobalValue::LinkageTypes::InternalLinkage,
                        llvm::ConstantDataArray::getString(ir::xContext, this->data));
                 */

                ExprWalker visitor(&builder, locals, globals);
                return visitor.visit(ctx);
            }
        }
    }
}